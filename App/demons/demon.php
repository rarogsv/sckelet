<?php

use App\Library\Scelet\Query\SwooleHtppRequest;
use App\Model\GlobalData;
use Swoole\Http\Request;
use Swoole\Http\Response;
use Swoole\Http\Server;


$server = new Server(IP, $port);

/**
 * https://www.swoole.co.uk/docs/modules/swoole-server/configuration
 */
$server->set($sett);

GlobalData::Create();
$GLOBALS['ip'] = [];
$GLOBALS['ts_cleans_ip'] = time();

if (!isset($GLOBALS['max_conn_min'])){
    $GLOBALS['max_conn_min'] = 60;
}
//logs(__FILE__, __LINE__, $GLOBALS['max_conn_min']);

$server->on("start", function (Swoole\HTTP\Server $server)  {
    //logs(__FILE__, __LINE__, 'start pid-'.GlobalData::$pid);
    /*
    App\Library\Scelet\HttpRequest\Request::Build(IP.':'.SOCKET_SC.'/'.SERVER_HANDLER_KEY.'/setPid/')
        ->setGET([
            'p'=> GlobalData::$pid,
            'd'=> DEMON_NAME
        ])->requestGET(3);
    */

    $server->tick(60000, function() use ($server) {
        $server = \App\Library\Scelet\ModelBase\Servers::Load(ID_SERVER);
        $server->online(DEMON_NAME);
        //logs(__FILE__, __LINE__, GlobalData::getInstance()->getLoggerSpeed());
        //logs(__FILE__, __LINE__, 'ping');
        //ping
        GlobalData::getInstance()->ping();
    });

    start($server);
});

function _lightDdosProtect(Request $request){
    if (!isset($request->header['x-real-ip'])){
        return false;
    }

    $ip = $request->header['x-real-ip'];
    $ipReq = [];
    $tn = time();
    $countIPReq = 0;
    //logs(__FILE__, __LINE__, $ip);
    //logs(__FILE__, __LINE__, $GLOBALS['ip']);
    _cleanIP($tn);

    if (isset($GLOBALS['ip'][$ip])){
        $ipReq = $GLOBALS['ip'][$ip];
        $countIPReq = count($ipReq);
    }

    if ($countIPReq > 0){
        if (($ipReq[0] + 60) > $tn){
            if ($countIPReq >= $GLOBALS['max_conn_min']){
                return false;
            }
        } else {
            $ipReq = [];
        }
    }

    $ipReq[]=$tn;
    $GLOBALS['ip'][$ip] = $ipReq;

    return true;
}

function _cleanIP($tn){
    if (($tn-$GLOBALS['ts_cleans_ip'])>60){
        foreach ($GLOBALS['ip'] as &$ipReq){
            if (($ipReq[0]+60) < $tn){
                if (count($ipReq) < $GLOBALS['max_conn_min']){
                    $ipReq=[];
                }
            }
        }
        $GLOBALS['ts_cleans_ip'] = $tn;
    }
}

$server->on('request', function (Request $request, Response $response) use ($server)  {

    // {"fd":3,"streamId":0,"header":{"host":"login.itneris.com","x-real-ip":"178.151.228.25","x-forwarded-for":"178.151.228.25","connection":"close","sec-ch-ua":"\"Opera\";v=\"77\", \"Chromium\";v=\"91\", \";Not A Brand\";v=\"99\"","sec-ch-ua-mobile":"?0","dnt":"1","upgrade-insecure-requests":"1","user-agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/91.0.4472.101 Safari\/537.36 OPR\/77.0.4054.90","accept":"text\/html,application\/xhtml+xml,application\/xml;q=0.9,image\/avif,image\/webp,image\/apng,*\/*;q=0.8,application\/signed-exchange;v=b3;q=0.9","sec-fetch-site":"cross-site","sec-fetch-mode":"navigate","sec-fetch-dest":"document","referer":"https:\/\/taxikef.net\/","accept-encoding":"gzip, deflate, br","accept-language":"ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"},"server":{"request_method":"GET","request_uri":"\/_i\/l\/","path_info":"\/_i\/l\/","request_time":1624304922,"request_time_float":1624304922.382365,"server_protocol":"HTTP\/1.0","server_port":6201,"remote_port":58610,"remote_addr":"176.103.63.105","master_time":1624304922},"cookie":{"G_AUTHUSER_H":"0","G_ENABLED_IDPS":"google","fbm_664174890660130":"base_domain=.login.itneris.com","fbsr_664174890660130":"W1bpDvaUc5m8S3OyNfMmKS1ja2EEVm7jBVn--8b6kfQ.eyJ1c2VyX2lkIjoiMjA4NjY1MzI2ODEwNzM3OCIsImNvZGUiOiJBUUJJRTY0WElQd2JESE5hcGVqMi1UZldtS3dYTXZ2dTRDRkJ3NDhZSllLbFVuTlFBNUF5Rjl1M2czTm5uMFJibWJzcnljVXFhU0t6OWFRUWJwU2tFZmxieHZHRDVFV2ZGX1g0TWZnaVZ6MkdBV0FVQWZaTE1CaHRKWlRYZFg4cXZxWU5TQ2E5dGFRa1dMOXlaZFhUQW1wVkFQNHpvWTJabHV5V2RORFNyTmdKQzVCVnZ5QXRkSDJNZTNPb0hXbDNRVjBjWkdneThpSDBGd3JPSVI3TmhiaV80WnN1ZEpaZFhrazNKSXJSbVotcTE2a0EySWN0VVlfbHAtb3hvTXJoXzA1d2pQRHhfbzY3Y1JxV1ZndEtMZGl1bkJDc2tQZkQ4MVhMdk9iVDBEb0J2dVZ5MkVNWmtkV0FMY29xRE1BZGVZT1RDOGpzRFB5S085YlZZZTlDa0NjZSIsImFsZ29yaXRobSI6IkhNQUMtU0hBMjU2IiwiaXNzdWVkX2F0IjoxNjI0MzA0MTI5fQ"},"get":null,"files":null,"post":null,"tmpfiles":null}
    //logs(__FILE__, __LINE__, $request->server['request_uri']);
    //logs(__FILE__, __LINE__, $request->cookie);
    //logs(__FILE__, __LINE__, $request);
    $res = _lightDdosProtect($request);
    if (!$res){
        //logs(__FILE__, __LINE__, $GLOBALS['ip']);
        //logs(__FILE__, __LINE__, 'return');
        //$response->end('Ban IP.');
        return;
    }


    $timeStart =  microtime(true)*1000;
    $swRequest = new SwooleHtppRequest($request);
    $swResponse = new \App\Library\Scelet\Query\SwooleHttpResponse($response);
    try {
        $uri = $swRequest->getUri();
        if (count($uri) < 2){
            //проверка контроллера
            $response->end('');
            return;
        }

        //logs(__FILE__, __LINE__, $request->server['remote_addr']);
        if ($uri[0] == SERVER_HANDLER_KEY){
            //logs(__FILE__, __LINE__, $request);
            \App\Library\Scelet\Util\ServerHandler::handle($server, $swRequest, $swResponse, GlobalData::getInstance());
            return;
        } else {
            //logs(__FILE__, __LINE__, $uri);
            $injection = new \App\Library\Scelet\ControllersBase\Injection(PUBLIC_KEY, $uri, $swRequest, $swResponse, $server);
            request($injection);
            //\App\Controllers\Client\v1\ClientController::Run($injection)->getResponse()->send();
        }

        $timeStop =  microtime(true)*1000;
        GlobalData::getInstance()->setLoggerSpeed($uri[1], $timeStart, $timeStop);
        //logs(__FILE__, __LINE__, GlobalData::getInstance()->getLoggerSpeed());
    } catch (Throwable $e) {

        logs(__FILE__, __LINE__, $swRequest->getRequestData());
        logs(__FILE__, __LINE__, $e->getFile());
        logs(__FILE__, __LINE__, $e->getLine());
        logs(__FILE__, __LINE__, $e->getMessage());

        \App\Library\Scelet\Util\ServerHandler::exception($e, $uri[1], [
            'cookie' => $swRequest->getRequest()->cookie,
            'get' => $swRequest->getRequest()->get,
            'post' => $swRequest->getRequest()->get,
            'json' => $swRequest->getRequest()->rawContent(),
        ], $swResponse);
    }
});

if (function_exists('addServer')) {
    addServer($server);
}

$server->start();