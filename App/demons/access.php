<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 08.10.2023
 * Time: 11:14
 */
use Swoole\Http\Request;
use Swoole\Http\Response;

$GLOBALS['blocked'] = [];
$GLOBALS['count_req'] = 0;
$GLOBALS['last_send'] = time();
//сохр заголовки запросов
$GLOBALS['request_data'] = [];

//сохр заголовки заблокированные
$GLOBALS['data_block'] = [];



function checkHasAccept(Request $request, Response &$response){
    //logs(__FILE__, __LINE__, $GLOBALS['count_req']);
    //logs(__FILE__, __LINE__, count($GLOBALS['request_data']));


    $GLOBALS['count_req']++;
    $h = $request->header;
    $coockie = [];
    $idUser = null;
    if (!is_null($request->cookie)){
        $coockie = $request->cookie;
    }

    //IP юзера и языки  должны быть
    if (!isset($h['x-real-ip']) || !isset($h['accept-language'])){
        _addBlockedDdos(-1);
        return false;
    }

    if ($request->server['request_method'] == 'GET'){
        //logs(__FILE__, __LINE__, $coockie);
        if (isset($coockie['imp'])){
            //проверка куки
            $idUser = _getCookieDdos($coockie['imp']);
            if (is_null($idUser)){
                _addBlockedDdos(-3);
                return false;
            }
        }
    } else {
        //все ПОСТ запросы должны иметь куки
        if (count($coockie) == 0 || !isset($coockie['imp'])){
            _addBlockedDdos(-4);
            return false;
        } else {
            $idUser = _getCookieDdos($coockie['imp']);
            if (is_null($idUser)){
                _addBlockedDdos(-3);
                return false;
            }
        }
    }

    if (is_null($idUser)){
        $idUser = getRandomString(10);
        _setCookieDdos($idUser, $response);
    }

    $user = _saveHistoryDdos($request, $idUser);
    if (count($user['time'])>120){
        _addBlockedDdos(-5);
        return false;
    } else if (count($user['time'])>20){
        _addBlockedDdos(-6);
        return false;
    } else if (count($user['time'])>90){
        _addBlockedDdos(-7);
        return false;
    }



    $uri        = $request->server['request_uri'];
    $hasGet     = !is_null($request->get);
    $hasPost    = !is_null($request->post);
    $hasFiles   = !is_null($request->files);
    $ds = [];
    foreach ($GLOBALS['data_block'] as $block) {
        eval($block);
    }

    foreach ($ds as $id=>$deprecated){
        if ($deprecated){
            _addBlocked($id);
            return false;
        }
    }

    //logs(__FILE__, __LINE__, $GLOBALS['count_req']);
    //logs(__FILE__, __LINE__, $GLOBALS['request_data']);


    return true;
}



function _addBlockedDdos($id){
    if (isset($GLOBALS['blocked'][$id])){
        $GLOBALS['blocked'][$id]++;
    } else {
        $GLOBALS['blocked'][$id] = 1;
    }
}

function _saveHistoryDdos(Request $request, string $idUser){
    $header = $request->header;
    $del = ['host', 'x-forwarded-proto', 'x-forwarded-for', 'connection', 'content-length', 'pragma', 'dnt', 'sec-ch-ua',
        'sec-ch-ua-mobile', 'user-agent', 'content-type', 'cache-control', 'accept', 'sec-fetch-site', 'sec-fetch-mode',
        'sec-fetch-dest', 'accept-encoding', 'upgrade-insecure-requests', 'sec-fetch-user'];

    foreach ($del as $item){
        if (isset($header[$item])){
            unset($header[$item]);
        }
    }
    if (isset($header['sec-ch-ua-platform'])){
        $header['sec-ch-ua-platform'] = json_decode($header['sec-ch-ua-platform']);
    }

    $c = [];
    if (!is_null($request->cookie)){
        $c = array_keys($request->cookie);
    }
    $uri = $request->server['request_uri'];
    if (isset($GLOBALS['request_data'][$idUser])){
        $GLOBALS['request_data'][$idUser]['time'][] = time();
        $GLOBALS['request_data'][$idUser]['cookie'] = $c;
        if (isset($GLOBALS['request_data'][$idUser]['server']['uri'][$uri])){
            $GLOBALS['request_data'][$idUser]['server']['uri'][$uri]++;
        } else {
            $GLOBALS['request_data'][$idUser]['server']['uri'][$uri]= 1;
        }

        if (isset($GLOBALS['request_data'][$idUser]['server']['method'][$request->server['request_method']])){
            $GLOBALS['request_data'][$idUser]['server']['method'][$request->server['request_method']]++;
        } else {
            $GLOBALS['request_data'][$idUser]['server']['method'][$request->server['request_method']]= 1;
        }
        $GLOBALS['request_data'][$idUser]['cr']++;
    } else {
        ksort($header);
        $GLOBALS['request_data'][$idUser] = [
            'cookie' => $c,
            'server' => [
                'uri'       => [$uri     => 1],
                'method'    => [$request->server['request_method']  => 1],
            ],
            'cr'        => 1,
            'header'    => $header,
            'time'      => [time()]
        ];
        unset($header['x-real-ip']);
        $GLOBALS['request_data'][$idUser]['header_imprint'] = base64_encode(json_encode($header));
    }


    return $GLOBALS['request_data'][$idUser];
}

function _clearDdos(){
    $GLOBALS['blocked']     = [];
    $GLOBALS['count_req']   = 0;

    //сохр заголовки запросов
    $GLOBALS['request_data']= [];

    //сохр заголовки заблокированные
    $GLOBALS['data_block']  = [];
    $GLOBALS['last_send']   = time();
}

function _setCookieDdos(string $id, Response &$response){
    //logs(__FILE__, __LINE__, '_setCookieDdos');
    //logs(__FILE__, __LINE__, $id);
    $cookie = encrypt(
        ['id'=>$id],
        KEY_ITNERIS_SENDER
        );
    $response->cookie('imp', $cookie, time()+91104000, '/', DOMAIN);
}

function _getCookieDdos(string $imp){
    try {
        $data = decrypt($imp, KEY_ITNERIS_SENDER);
    } catch (Exception $e) {
        return null;
    }

    if ($data === false || !isset($data['id'])){
        return null;
    }
    //logs(__FILE__, __LINE__, '_getCookieDdos');
    //logs(__FILE__, __LINE__, $data['id']);
    return $data['id'];
}

function getHistoryReq(){
    logs(__FILE__, __LINE__, 'getHistoryReq');
    //logs(__FILE__, __LINE__, $GLOBALS['last_send']+30);
    //logs(__FILE__, __LINE__, $GLOBALS['last_send']+30>time());

    if ($GLOBALS['last_send']+30>time()){
        return [];
    }

    //logs(__FILE__, __LINE__, $GLOBALS['count_req']);
    //logs(__FILE__, __LINE__, count($GLOBALS['request_data']));

    //top 10-ка схожести по IP
    $topSimilarIPAll        = [];
    $topSimilarCountReqAll  = [];
    $topSimilarHeaderAll    = [];
    $topSimilarUriAll       = [];
    $not_cookie             = [];

    foreach ($GLOBALS['request_data'] as $id=>$data){
        //similar IP
        $ip = $data['header']['x-real-ip'];
        //logs(__FILE__, __LINE__, $id);
        //logs(__FILE__, __LINE__, $topSimilarIPAll);
        _addItemSimilar($topSimilarIPAll, $ip, $id, $data, true);
        //logs(__FILE__, __LINE__, $topSimilarIPAll);

        //similar req
        _addItemSimilar($topSimilarCountReqAll, $data['cr'], $id, $data, true);
        //similar header
        _addItemSimilar($topSimilarHeaderAll, $data['header_imprint'], $id, $data, false);

        //similar not cookie
        if (count($data['cookie']) == 0){
            _addItemSimilar($not_cookie, 0, $id, $data, false);
        }
    }

    logs(__FILE__, __LINE__, $topSimilarIPAll);
    $top_ip = _getTop($topSimilarIPAll, 10);//many id
    if (isset($not_cookie[0]['id']) && count($not_cookie[0]['id']) > 100){
        $not_cookie[0]['id'] = array_slice($not_cookie[0]['id'], 0, 100); //many id
    }
    //$top_req            = _getTop20($GLOBALS['request_data']);//top 10-ка кто самое больше делал запросов
    //logs(__FILE__, __LINE__, count($top_ip));

    //logs(__FILE__, __LINE__, count($topSimilarCountReqAll));
    $top_similar_req    = _getTop($topSimilarCountReqAll, 20);//по частоте запроса
    //logs(__FILE__, __LINE__, count($top_similar_req));

    //logs(__FILE__, __LINE__, count($topSimilarHeaderAll));
    $top_similar_header = _getTop($topSimilarHeaderAll, 20);//по заголовку
    //logs(__FILE__, __LINE__, count($top_similar_header));


    $allIds             = array_unique(array_merge(_getIds($top_similar_req, 100), _getIds($top_similar_header, 100),
        _getIds($top_ip, 100), _getIds($not_cookie, 100)));
    $usersData          = array_sub($GLOBALS['request_data'], $allIds);

    foreach ($usersData as $id=>&$datum){
        unset($datum['header_imprint']);
        foreach ($datum['time'] as &$item){
            $item = $item -  $GLOBALS['last_send'];
        }
    }
    //logs(__FILE__, __LINE__, $GLOBALS['count_req']);
    //logs(__FILE__, __LINE__, count($GLOBALS['request_data']));

    $timeReport = $GLOBALS['last_send'];
    _clearDdos();
    return [
        'ts_report'            => $timeReport,
        'top_ip'            => $top_ip,
        'not_cookie'        => $not_cookie,
        'top_similar_req'   => $top_similar_req,
        'top_similar_header'=> $top_similar_header,
        'blocked'           => $GLOBALS['blocked'],
        'users'             => $usersData,
    ];
}



function _getIds(array $arr, int $c) : array
{
    $res = [];
    foreach ($arr as $item){
        $res = array_merge($res, $item['id']);
    }
    if (count($res) > $c){
        return array_slice($res, 0, $c);
    } else {
        return $res;
    }
}

function _addItemSimilar(&$arr, $key,  $id, $data, $addKey){
    //logs(__FILE__, __LINE__, count($arr));
    if (!isset($arr[$key])) {
        $arr[$key] = ['cr'=>0, 'id'=>[], 'key'=>''];
        if ($addKey){
            $arr[$key]['key'] = $key;
        }
    }

    $arr[$key]['cr'] += $data['cr'];
    $arr[$key]['id'][] = $id;
    //logs(__FILE__, __LINE__, count($arr));
}

function _getTop($arr, $c){
    usort($arr, "_sortDdosSimilarCountReq");
    if (count($arr) > $c){
        $res = array_slice($arr, 0, $c);
    } else {
        $res = $arr;
    }
    return $res;
}
//https://www.php.net/manual/ru/function.usort.php
//сортируем по количеству запросов
function _sortDdosSimilarCountReq($req1, $req2){
    $a = $req1['cr'];
    $b = $req1['cr'];
    if ($a == $b) {
        return 0;
    }
    return ($a > $b) ? -1 : 1;
}
