<?php

use App\Library\Scelet\Query\SwooleHtppRequest;
use App\Model\GlobalData;

use Swoole\Http\Request;
use Swoole\WebSocket\Frame;
use Swoole\WebSocket\Server;


$server = new Server(IP, $port, SWOOLE_BASE);

/**
 * https://www.swoole.co.uk/docs/modules/swoole-server/configuration
 */
$server->set($sett);

GlobalData::Create();

$server->on("start", function (Swoole\HTTP\Server $server)  {
    //logs(__FILE__, __LINE__, 'start pid-'.GlobalData::$pid);
    /*
    App\Library\Scelet\HttpRequest\Request::Build(IP.':'.SOCKET_SC.'/'.SERVER_HANDLER_KEY.'/setPid/')
        ->setGET([
            'p'=> GlobalData::$pid,
            'd'=> DEMON_NAME
        ])->requestGET(3);
    */

    $server->tick(60000, function() use ($server) {
        $server = \App\Library\Scelet\ModelBase\Servers::Load(ID_SERVER);
        $server->online(DEMON_NAME);
        //logs(__FILE__, __LINE__, GlobalData::getInstance()->getLoggerSpeed());
        //logs(__FILE__, __LINE__, 'ping');
        //ping
        GlobalData::getInstance()->ping();
    });

    start($server);
});

$server->on('open', function(Server $server, Swoole\Http\Request $request) {

    //print_log(__FILE__, __LINE__, 'open');
    //print_log(__FILE__, __LINE__, $request);
    //return;
    //{"fd":1,"streamId":0,"header":{"upgrade":"websocket","connection":"Upgrade","host":"qrdo.net","pragma":"no-cache","cache-control":"no-cache","user-agent":"Mozilla\/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit\/537.36 (KHTML, like Gecko) Chrome\/84.0.4147.135 Safari\/537.36 OPR\/70.0.3728.154","origin":"https:\/\/qrdo.net","sec-websocket-version":"13","accept-encoding":"gzip, deflate, br","accept-language":"ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7","sec-websocket-key":"AJDo3QU4F813k\/zrXuDuhA==","sec-websocket-extensions":"permessage-deflate; client_max_window_bits"},"server":{"request_method":"GET","request_uri":"\/c\/","path_info":"\/c\/","request_time":1599494726,"request_time_float":1599494726.136403,"server_protocol":"HTTP\/1.1","server_port":10009,"remote_port":51592,"remote_addr":"193.203.50.113","master_time":1599494725},"cookie":{"data":"eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjgsImlhdCI6MTU5OTE0NzYyNCwiZXhwIjoxNjA2NDA1MjI0LCJkYmlkIjoiZGVmYXVsdCJ9.l1yIcSOgds-5C4QtGvB4kLSYymf1AEu-BnA0tRvVxmE"},"get":null,"files":null,"post":null,"tmpfiles":null}

    if (!isset($request->header['origin']) || $request->header['origin']!= FULL_URL_MAIN){
        $server->disconnect($request->fd, 1, 'bad uri');
        return;
    }


    $timeStart =  microtime(true)*1000;
    $swRequest = new SwooleHtppRequest($request);
    //$swResponse = new \App\Library\Scelet\Query\SwooleHttpResponse($response);
    try {
        $uri = $swRequest->getUri();
        if (count($uri) < 2){
            //проверка контроллера
            $response->end('');
            return;
        }

        logs(__FILE__, __LINE__, $request->server['remote_addr']);
        if ($uri[0] == SERVER_HANDLER_KEY){
            logs(__FILE__, __LINE__, $request);
            \App\Library\Scelet\Util\ServerHandler::handle($server, $swRequest, null, GlobalData::getInstance());
            return;
        } else {
            //logs(__FILE__, __LINE__, $uri);
            $injection = new \App\Library\Scelet\ControllersBase\Injection(PUBLIC_KEY, $uri, $swRequest, null, $server);
            open($injection);
            //\App\Controllers\Client\v1\ClientController::Run($injection)->getResponse()->send();
        }

        $timeStop =  microtime(true)*1000;
        GlobalData::getInstance()->setLoggerSpeed($uri[1], $timeStart, $timeStop);
        //logs(__FILE__, __LINE__, GlobalData::getInstance()->getLoggerSpeed());
    } catch (Throwable $e) {

        logs(__FILE__, __LINE__, $swRequest->getRequestData());
        logs(__FILE__, __LINE__, $e->getFile());
        logs(__FILE__, __LINE__, $e->getLine());
        logs(__FILE__, __LINE__, $e->getMessage());

        \App\Library\Scelet\Util\ServerHandler::exception($e, $uri[1], [
            'cookie' => $swRequest->getRequest()->cookie,
            'get' => $swRequest->getRequest()->get,
            'post' => [],
            'json' => [],
        ], $swResponse);
    }
});


$server->on('message', function(Server $server, Frame $frame) {
    //print_log(__FILE__, __LINE__, 'message');
    //print_log(__FILE__, __LINE__, $frame);
    //print_log(__FILE__, __LINE__, "connection message: {$frame->data}");
    //$server->push($frame->fd, json_encode(["hello", time()]));
    $requestData = json_decode($frame->data, true);
    //print_log(__FILE__, __LINE__, $requestData);
    if (!isset($requestData['uri']) || !isset($requestData['data'])){
        //print_log(__FILE__, __LINE__, 'close');
        $server->close($frame->fd);
        return;
    }

    if ($requestData['uri'][0]=='z'){
        //print_log(__FILE__, __LINE__, 'z');
        $server->push($frame->fd, '[]');
        return;
    }


    $timeStart =  microtime(true)*1000;
    $swRequest = new \App\Library\Scelet\Query\SwooleSocketRequest($request);
    try {
        $uri = $swRequest->getUri();
        if (count($uri) < 2){
            //проверка контроллера
            $response->end('');
            return;
        }

        logs(__FILE__, __LINE__, $request->server['remote_addr']);
        $injection = new \App\Library\Scelet\ControllersBase\Injection(PUBLIC_KEY, $uri, $swRequest, null, $server);
        message($injection);

        $timeStop =  microtime(true)*1000;
        GlobalData::getInstance()->setLoggerSpeed($uri[0], $timeStart, $timeStop);
        //logs(__FILE__, __LINE__, GlobalData::getInstance()->getLoggerSpeed());
    } catch (Throwable $e) {

        logs(__FILE__, __LINE__, $swRequest->getRequestData());
        logs(__FILE__, __LINE__, $e->getFile());
        logs(__FILE__, __LINE__, $e->getLine());
        logs(__FILE__, __LINE__, $e->getMessage());

        \App\Library\Scelet\Util\ServerHandler::exception($e, $uri[0], [
            'cookie' => [],
            'get' => $swRequest->getRequest()->get,
            'post' => [],
            'json' => [],
        ], $swResponse);
    }
});

$server->on('close', function(Server $server, int $fd) {
    GlobalData::getInstance()->disconnect($fd);
    //print_log(__FILE__, __LINE__, GlobalData::getInstance()->connects);
});



$server->start();