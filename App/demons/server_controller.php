<?php
/**
 * Server controller
 */

use App\Library\Scelet\Query\SwooleHtppRequest;
use App\Model\GlobalData;
use Swoole\Http\Request;
use Swoole\Http\Response;


$server = new Swoole\HTTP\Server(IP, $port);


/**
 * https://www.swoole.co.uk/docs/modules/swoole-server/configuration
 */
$server->set($sett);

GlobalData::Create();

$server->on("start", function (Swoole\HTTP\Server $server)  {
    logs(__FILE__, __LINE__, 'start');
    $server->tick(60000, function() use ($server) {
        $server = \App\Library\Scelet\ModelBase\Servers::Load(ID_SERVER);
        $server->online('sc');
        //ping
        GlobalData::getInstance()->ping();
    });
});


$server->on('request', function (Request $request, Response $response) use ($server) {

    $swRequest = new SwooleHtppRequest($request);
    $swResponse = new \App\Library\Scelet\Query\SwooleHttpResponse($response);
    $uri = $swRequest->getUri();

    //logs(__FILE__, __LINE__, $uri);
    if ($uri[0] == SERVER_HANDLER_KEY){
        logs(__FILE__, __LINE__, $request->get);
        $sc = getProjectServerHandler();
        $sc->serverConroller($swRequest, $swResponse, GlobalData::getInstance(), $server);
        //\App\Library\Scelet\Util\ServerHandler::serverConroller($swRequest, $swResponse, GlobalData::getInstance());
        //return;
    }

    $response->end('');

});

$server->start();