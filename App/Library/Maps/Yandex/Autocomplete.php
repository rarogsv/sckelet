<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.03.2020
 * Time: 13:15
 */
namespace App\Library\Maps\Yandex;

use App\Library\Scelet\HttpRequest\Request;
use Exception;

/**
 * Class Autocomplete
 * @package App\Library\Maps\Yandex
 * url - https://yandex.ru/dev/maps/geocoder/doc/desc/concepts/input_params.html
 */
class Autocomplete
{
    const LANGUAGE_RU = 'ru_RU';
    const LANGUAGE_UA = 'uk_UA';
    const LANGUAGE_BY = 'be_BY';
    const LANGUAGE_EN = 'en_US';

    const LOCALITY_LOCALITY = 'locality';
    const LOCALITY_ADR = 'house';


    protected $url = "https://geocode-maps.yandex.ru/1.x";
    protected $key;
    protected $input;
    protected $language=Autocomplete::LANGUAGE_RU;

    protected $location=null;
    protected $radius=null;

    protected $locality = null;
    protected $country = null;


    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param mixed $input
     * @return Autocomplete
     */
    public function setInput(string $input) : self
    {
        $this->input = $input;
        return $this;
    }


    /**
     * @param null $locality
     * @return Autocomplete
     */
    public function setLocality($locality) : self
    {
        $this->locality = $locality;
        return $this;
    }


    /**
     * @param null $language
     * @return Autocomplete
     */
    public function setLanguage($language) : self
    {
        $this->language = strtolower($language);
        return $this;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return Autocomplete
     */
    public function setLocation(float $lat, float $lng) : self
    {
        $this->location = $lat.','.$lng;
        return $this;
    }


    /**
     * @param null $country
     * @return Autocomplete
     */
    public function setCountry($country) : self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @param int $radius
     * @return Autocomplete
     */
    public function setRadius(int $radius) : self
    {
        $this->radius = $radius;
        return $this;
    }


    /**
     * @return array
     * @throws Exception
     */
    public function send() : array
    {
        $requestData = $this->getRequestData();
        $request = Request::Build($this->url)->setGET($requestData)->requestGET();
        $res = $request->getResponse();
        //logs(__FILE__, __LINE__, $res);
        //logs(__FILE__, __LINE__, isset($res['response']));
        //logs(__FILE__, __LINE__, isset($res['response']['GeoObjectCollection']));
        //logs(__FILE__, __LINE__, isset($res['response']['GeoObjectCollection']['featureMember']));
        if (isset($res['response']) && isset($res['response']['GeoObjectCollection']) && isset($res['response']['GeoObjectCollection']['featureMember'])){
            return $this->parseResponse($res);
        } else {
            throw new Exception();
        }
    }

    /**
     * @param array $response
     * @return array
     */
    protected function parseResponse(array $response) : array
    {
        $places = $response['response']['GeoObjectCollection']['featureMember'];


        $placesConverted = [];
        logs(__FILE__, __LINE__, $places);

        foreach ($places as $place){
            //logs(__FILE__, __LINE__, isset($place['place_id']));
            if (!is_null($this->country)){
                if (!isset($place['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['country_code'])){continue;}
                if ($this->country != $place['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['country_code']){
                    continue;
                }
            }

            if (!is_null($this->locality)){
                if (!isset($place['GeoObject']['metaDataProperty']['GeocoderMetaData']['kind'])){continue;}
                if ($this->locality != $place['GeoObject']['metaDataProperty']['GeocoderMetaData']['kind']){
                    continue;
                }
            }



            list($lng, $lat) = explode(' ', $place['GeoObject']['Point']['pos']);
            $location = ['lat'=>$lat, 'lng'=>$lng];
            logs(__FILE__, __LINE__, $location);
            $pc = [
                'address'       => $place['GeoObject']['metaDataProperty']['GeocoderMetaData']['text'],
                'name'          => $place['GeoObject']['name'],
                'address_arr'   => $place['GeoObject']['metaDataProperty']['GeocoderMetaData']['Address']['Components'],
                'location'      => $location,
            ];
            $placesConverted[] = $pc;
        }
        return $placesConverted;
    }

    /**
     * @return array
     */
    protected function getRequestData() : array
    {
        $data = [
            'apikey' => $this->key,
            'geocode' => $this->input,
            'lang' => $this->language,
            'format' => 'json',
        ];

        if (!is_null($this->location) && !is_null($this->radius)){
            $data['ll'] = $this->location;
            $data['rspn'] = $this->radius;
        }

        return $data;
    }

}