<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.03.2020
 * Time: 13:15
 */
namespace App\Library\Maps\Here;

use App\Library\Scelet\HttpRequest\Request;
use Exception;

/**
 * Class Geocoder
 * @package App\Library\Maps\Yandex
 */
class Geocoder
{


    protected $url = "https://geocoder.ls.hereapi.com/6.2/geocode.json";
    protected $key;
    protected $locationId;


    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param mixed $locationId
     * @return Geocoder
     */
    public function setLocationId(string $locationId) : self
    {
        $this->locationId = $locationId;
        return $this;
    }



    /**
     * @return array
     * @throws Exception
     */
    public function send() : array
    {
        $requestData = $this->getRequestData();
        $request = Request::Build($this->url)->setGET($requestData)->requestGET();
        $res = $request->getResponse();
        //logs(__FILE__, __LINE__, $res);
        //{"suggestions":[{"label":"Україна, Київ, Вишняківська вулиця, 6А","language":"uk","countryCode":"UKR","locationId":"NT_DRb.Be.7.0uyM1NY.mLg4C_2ANk","address":{"country":"Україна","county":"Київ","city":"Київ","district":"Дарницький район","street":"Вишняківська вулиця","houseNumber":"6А","postalCode":"02140"},"matchLevel":"houseNumber"}]}
        if (isset($res['Response'])){
            return $this->parseResponse($res);
        } else {
            throw new Exception();
        }
    }

    /**
     * @param array $response
     * @return array {
                     "label": "Україна, Київ, Вишняківська вулиця, 6А",
                     "language": "uk",
                     "countryCode": "UKR",
                     "locationId": "NT_DRb.Be.7.0uyM1NY.mLg4C_2ANk",
                     "address": {
                     "country": "Україна",
                     "county": "Київ",
                     "city": "Київ",
                     "district": "Дарницький район",
                     "street": "Вишняківська вулиця",
                     "houseNumber": "6А",
                     "postalCode": "02140"
                     },
                     "matchLevel": "houseNumber"
                     }
     */
    protected function parseResponse(array $response) : array
    {
        return $response['Response']['View'][0]['Result'][0]['Location'];
    }

    /**
     * @return array
     */
    protected function getRequestData() : array
    {
        $data = [
            'apiKey' => $this->key,
            'locationid' => $this->locationId
        ];

        return $data;
    }

}