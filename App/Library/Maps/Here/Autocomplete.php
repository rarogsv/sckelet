<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.03.2020
 * Time: 13:15
 */
namespace App\Library\Maps\Here;

use App\Library\Scelet\HttpRequest\Request;
use Exception;

/**
 * Class Autocomplete
 * @package App\Library\Maps\Yandex
 */
class Autocomplete
{


    protected $url = "https://autocomplete.geocoder.ls.hereapi.com/6.2/suggest.json";
    protected $key;
    protected $input;


    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param mixed $input
     * @return Autocomplete
     */
    public function setInput(string $input) : self
    {
        $this->input = $input;
        return $this;
    }



    /**
     * @return array
     * @throws Exception
     */
    public function send() : array
    {
        $requestData = $this->getRequestData();
        $request = Request::Build($this->url)->setGET($requestData)->requestGET();
        $res = $request->getResponse();
        //logs(__FILE__, __LINE__, $res);
        //{"suggestions":[{"label":"Україна, Київ, Вишняківська вулиця, 6А","language":"uk","countryCode":"UKR","locationId":"NT_DRb.Be.7.0uyM1NY.mLg4C_2ANk","address":{"country":"Україна","county":"Київ","city":"Київ","district":"Дарницький район","street":"Вишняківська вулиця","houseNumber":"6А","postalCode":"02140"},"matchLevel":"houseNumber"}]}
        if (isset($res['suggestions'])){
            return $this->parseResponse($res);
        } else {
            throw new Exception();
        }
    }

    /**
     * @param array $response
     * @return array {
                     "label": "Україна, Київ, Вишняківська вулиця, 6А",
                     "language": "uk",
                     "countryCode": "UKR",
                     "locationId": "NT_DRb.Be.7.0uyM1NY.mLg4C_2ANk",
                     "address": {
                     "country": "Україна",
                     "county": "Київ",
                     "city": "Київ",
                     "district": "Дарницький район",
                     "street": "Вишняківська вулиця",
                     "houseNumber": "6А",
                     "postalCode": "02140"
                     },
                     "matchLevel": "houseNumber"
                     }
     */
    protected function parseResponse(array $response) : array
    {
        //logs(__FILE__, __LINE__, 'parseResponse');
        $places = $response['suggestions'];
        $placesConverted = [];
        //logs(__FILE__, __LINE__, $places);

        foreach ($places as $place){
            $pc = [
                'address'       => $place['label'],
                'address_arr'   => $place['address'],
                'locationId'    => $place['locationId'],
            ];
            $placesConverted[] = $pc;
        }
        //logs(__FILE__, __LINE__, $placesConverted);
        return $placesConverted;
    }

    /**
     * @return array
     */
    protected function getRequestData() : array
    {
        $data = [
            'apiKey' => $this->key,
            'query' => $this->input
        ];

        return $data;
    }

}