<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.03.2020
 * Time: 13:56
 */

namespace App\Library\Maps\Google;


use App\Library\Scelet\HttpRequest\Request;
use Exception;

class Geodecoding
{
    protected $url = "https://maps.googleapis.com/maps/api/geocode/json";
    protected $key;
    protected $place_id = null;
    protected $location = null;
    protected $language=null;

    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param null $language
     * @return Geodecoding
     * @throws Exception
     */
    public function setLanguage($language) : self
    {
        if (strlen($language) != 2){
            throw new Exception();
        }
        $this->language = strtolower($language);
        return $this;
    }

    /**
     * @param null $place_id
     * @return Geodecoding
     */
    public function setPlaceId($place_id) : self
    {
        $this->place_id = $place_id;
        return $this;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return Geodecoding
     */
    public function setLocation(float $lat, float $lng) : self
    {
        $this->location = $lat.','.$lng;
        return $this;
    }


    public function send()
    {
        $request = Request::Build($this->url)->setGET($this->getRequestData())->requestGET();
        $res = $request->getResponse();
        if ($res['status'] != 'OK'){
            throw new Exception($res['error_message']);
        }

        return $this->parseResponse($res);
    }

    protected function parseResponse(array $response) : array
    {
        if (!isset($response['results']) && count($response['results']) > 0){return [];}

        $place = $response['results'][0];
        $pc = [];

        if (isset($place['formatted_address']) && isset($place['geometry']) && $place['geometry']['location']){
            $pc = [
                'location' => [
                    'lat' => $place['geometry']['location']['lat'],
                    'lng' => $place['geometry']['location']['lng'],
                ],

                'address' => $place['formatted_address'],
                'address_arr' => []
            ];

            if (isset($place['address_components']) && count($place['address_components']) > 0){
                $pc['address_arr'] = array_column($place['address_components'], 'short_name');
            }
        }


        return $pc;
    }

    protected function getRequestData() : array
    {
        $keys = ['key','place_id','language','location'];
        $data = [];
        foreach ($keys as $key){
            if (is_null($this->{$key})){continue;}
            $data[$key] = $this->{$key};
        }


        return $data;
    }


}