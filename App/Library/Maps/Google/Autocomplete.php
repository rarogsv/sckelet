<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.03.2020
 * Time: 13:15
 */
namespace App\Library\Maps\Google;

use App\Library\Scelet\HttpRequest\Request;
use Exception;

class Autocomplete
{
    protected $url = "https://maps.googleapis.com/maps/api/place/autocomplete/json";
    protected $key;
    protected $input;
    protected $language=null;
    protected $location=null;
    protected $radius=null;
    protected $strictbounds=false;
    protected $components=null;
    protected $types=null;



    public function __construct(string $key)
    {
        $this->key = $key;
    }

    /**
     * @param mixed $input
     * @return Autocomplete
     */
    public function setInput(string $input) : self
    {
        $this->input = $input;
        return $this;
    }

    /**
     * @param null $language
     * @return Autocomplete
     * @throws Exception
     */
    public function setLanguage($language) : self
    {
        if (strlen($language) != 2){
            throw new Exception();
        }
        $this->language = strtolower($language);
        return $this;
    }

    /**
     * @param float $lat
     * @param float $lng
     * @return Autocomplete
     */
    public function setLocation(float $lat, float $lng) : self
    {
        $this->location = $lat.','.$lng;
        return $this;
    }

    /**
     * @param int $radius
     * @return Autocomplete
     */
    public function setRadius(int $radius)
    {
        $this->radius = $radius;
        return $this;
    }

    /**
     * @param bool $strictbounds
     * @return Autocomplete
     */
    public function setStrictbounds(bool $strictbounds) : self
    {
        $this->strictbounds = $strictbounds;
        return $this;
    }

    /**
     * @param string $countryISO
     * @return Autocomplete
     * @throws Exception
     */
    public function setComponents(string $countryISO) : self
    {
        if (strlen($countryISO) != 2){
            throw new Exception();
        }
        $this->components = 'country:'.$countryISO;

        return $this;
    }

    /**
     * @param null $types
     * @return Autocomplete
     * @throws \Exception
     */
    public function setTypes($types) : self
    {
        if (!array_search($types, ['geocode', 'address', 'establishment'])){
            throw new Exception();
        }
        $this->types = $types;
        return $this;
    }



    /**
     * @return array
     * @throws Exception
     */
    public function send() : array
    {
        $request = Request::Build($this->url)->setGET($this->getRequestData())->requestGET();
        $res = $request->getResponse();
        logs(__FILE__, __LINE__, $res);
        if ($res['status'] != 'OK'){
            if ($res['status'] == 'ZERO_RESULTS'){
                return [];
            } else {
                throw new Exception($res['error_message']);
            }
        } else {
            return $this->parseResponse($res);
        }
    }

    /**
     * @param array $response
     * @return array
     */
    protected function parseResponse(array $response) : array
    {

        if (!isset($response['predictions'])){return [];}

        $places = $response['predictions'];
        $placesConverted = [];
        logs(__FILE__, __LINE__, count($places));

        foreach ($places as $place){
            //logs(__FILE__, __LINE__, isset($place['place_id']));
            //logs(__FILE__, __LINE__, 1);
            if (!isset($place['place_id']) || !isset($place['description'])){continue;}
            logs(__FILE__, __LINE__, 2);

            $pc = [
                'place_id' => $place['place_id'],
                'address' => $place['description'],
                'address_arr' => [],
                'types' => $place['types']
            ];
            if (isset($place['terms']) && count($place['terms']) > 0){
                $pc['address_arr'] = array_column($place['terms'], 'value');
            }

            $placesConverted[] = $pc;
        }
        return $placesConverted;
    }

    /**
     * @return array
     */
    protected function getRequestData() : array
    {
        $keys = ['key','input','language','location','radius','components','types'];
        $data = [];
        foreach ($keys as $key){
            if (is_null($this->{$key})){continue;}
            $data[$key] = $this->{$key};
        }

        if ($this->strictbounds){
            $data['strictbounds'] = '';
        }

        return $data;
    }

}