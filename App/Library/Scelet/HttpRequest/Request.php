<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 31.01.2020
 * Time: 23:47
 */
namespace App\Library\Scelet\HttpRequest;

class Request
{

    protected $url;
    protected $dataGET=[];
    protected $dataPOST=[];

    protected $header=[];
    protected $response;
    protected $httpCode;

    public $isJSON = true;

    public function __construct(string $url)
    {
        $this->url = $url;
    }

    public static function Build(string $url) : self
    {
        return new Request($url);
    }

    public function setHeader(array $header) : self
    {
        $this->header = $header;
        return $this;
    }

    public function setPOST(array $data) : self
    {
        $this->dataPOST = $data;
        return $this;
    }

    public function setGET(array $data) : self
    {
        $this->dataGET = $data;
        return $this;
    }


    public function requestPOST($timeOut=0){
        return $this->send(true, $timeOut);
    }

    public function requestGET($timeOut=0){
        return $this->send(false, $timeOut);
    }



    protected function send(bool $isPost, $timeOut=0) : self
    {
        $url = $this->url;
        $aHeaders = [];
        foreach ($this->header as $key => $value) {
            $aHeaders[] = $key . ": " . $value;
        }

        $redirect = null;
        $ch = curl_init();
        if (count($this->dataGET) > 0){
            $url .= '?'.http_build_query($this->dataGET);
        }
        logs(__FILE__, __LINE__, $url);


        if ($isPost){
            curl_setopt($ch, CURLOPT_POST, true);
            //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->dataPOST));
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($this->dataPOST));

            logs(__FILE__, __LINE__, json_encode($this->dataPOST));
        }

        curl_setopt($ch, CURLOPT_URL, $url);

        if (count($this->header) > 0){
            curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeaders);
        }

        curl_setopt($ch, CURLOPT_TIMEOUT, $timeOut);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeOut*2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $this->response = curl_exec($ch);
        //$this->response = str_replace(chr(0xEFBBBF), '', $this->response);
        // Находим позицию первого символа '{'
        $pos = strpos($this->response, '{');
        // Вырезаем подстроку, начиная с найденной позиции
        if ($pos === 1){
            $this->response = substr($this->response, $pos);
        }

        //logs(__FILE__, __LINE__, $this->response);
        //logs(__FILE__, __LINE__, $this->isJSON);

        if ($this->isJSON){
            $this->response = json_decode($this->response, true);
        }

        $this->httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        return $this;
    }

    /**
     * @param bool $isJSON
     * @return Request
     */
    public function setIsJSON(bool $isJSON)
    {
        $this->isJSON = $isJSON;
        return $this;
    }

    function isJSON($string){

        return is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    public function parse() : array
    {
        return json_decode($this->response, true);
    }

    /**
     * @return mixed
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return mixed
     */
    public function getHttpCode()
    {
        return $this->httpCode;
    }


}