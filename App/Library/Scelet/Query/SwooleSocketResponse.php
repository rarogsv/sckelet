<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 29.07.2020
 * Time: 18:19
 */

namespace App\Library\Scelet\Query;


use Swoole\Http\Response;

class SwooleSocketResponse extends BaseResponse
{
    public $server;
    public $swooleResponse;
    protected $hasRedirect = false;

    public function __construct(Ser)
    {
        $this->swooleResponse = $response;
    }

    function setHeader(string $key, string $value)
    {
        $this->swooleResponse->header($key, $value);
        return $this;
    }

    function redirect(string $url)
    {
        $this->swooleResponse->redirect($url);
        $this->hasRedirect = true;
        return $this;
    }



    function setCookie(string $key, string $cookie, int $lifeTime, string $domain)
    {
        return $this;
    }

    function sendFile($path)
    {
        $this->swooleResponse->header('Content-Type', 'application/octet-stream');
        $this->swooleResponse->header('Content-Disposition', 'attachment; filename="'.basename($path).'"');
        $this->swooleResponse->header('Cache-Control', 'must-revalidate');
        $this->swooleResponse->sendfile($path);
        return $this;
    }

    public function onBackGet(){
        $this->html('<!DOCTYPE html><html><body><script>history.back();</script></body></html>');
    }

    public function onBackPost(){
        $this->setFrontCallback(self::FRONT_CALL_BACK)->json();
    }

    function send()
    {
        //logs(__FILE__, __LINE__, 'send');
        ///logs(__FILE__, __LINE__, $this->hasRedirect);
        if ($this->hasRedirect){return;}


        if(is_string($this->response)){
            //logs(__FILE__, __LINE__, 1);
            //logs(__FILE__, __LINE__, is_string($responseController->response));
            $this->swooleResponse->end($this->response);
        } else {
            //logs(__FILE__, __LINE__, 2);
            $this->swooleResponse->end(json_encode($this->response));
        }
    }
}