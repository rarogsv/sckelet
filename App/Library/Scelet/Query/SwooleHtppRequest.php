<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 29.07.2020
 * Time: 17:11
 */

namespace App\Library\Scelet\Query;


use Swoole\Http\Request;

class SwooleHtppRequest extends BaseRequest
{
    public function __construct(Request $request)
    {
        //requset example
        //{"fd":1,"streamId":0,
        //"header":{"host":"qrdo.net","x-real-ip":"46.211.18.142","x-forwarded-for":"46.211.18.142","connection":"close","content-length":"80233","user-agent":"PostmanRuntime\/7.26.2","accept":"*\/*","cache-control":"no-cache","postman-token":"e8ee76de-73ca-49fa-977a-88c9a51bade7","accept-encoding":"gzip, deflate, br","content-type":"multipart\/form-data; boundary=--------------------------651645894666833229460892"},
        //"server":{"query_string":"ertert=edtgdfg","request_method":"POST","request_uri":"\/rter","path_info":"\/rter","request_time":1595603246,
        //"request_time_float":1595603246.881803,"server_protocol":"HTTP\/1.0","server_port":10010,"remote_port":33114,"remote_addr":"193.203.50.113","master_time":1595603246},
        //"cookie":null,"get":{"ertert":"edtgdfg"},
        //"files":{"key2":{"name":"photo5246886955835305682.jpg","type":"image\/jpeg","tmp_name":"\/tmp\/swoole.upfile.rcWxFA","error":0,"size":79902}},
        //"post":{"key1":"v1"},"tmpfiles":["\/tmp\/swoole.upfile.rcWxFA"]}
        //$request->json = json_decode($request->rawContent(), true);
        parent::__construct($request);





    }

    /**
     * @param string $key
     * @return null|string
     */
    function getCookie(string $key)
    {
        if (isset($this->request->cookie) && isset($this->request->cookie[$key])){
            return $this->request->cookie[$key];
        }
        return null;
    }

    //GET POST
    function requestType()
    {
        return $this->request->server['request_method'];
    }


    /**
     * @return array
     */
    function getUri()
    {
        if (strlen($this->request->server['request_uri']) == 0){
            return [];
        } else {
            return $this->parseURI($this->request->server['request_uri']);
        }
    }


    function requestGET(){
        $this->request->get;
    }

    function isGet(): bool
    {
        return $this->requestType() == 'GET';
    }

    function isPost(): bool
    {
        return $this->requestType() == 'POST';
    }

    /**
     * @return mixed
     */
    public function getRequest() : Request
    {
        return $this->request;
    }


    /**
     * @param string $key
     * @return string
     */
    function getToken(string $key)
    {
        if (isset($this->getRequest()->header[$key])){
            return $this->getRequest()->header[$key];
        } else {
            return null;
        }
    }


    function getRequestData(){
        return [
            'addr'      => $this->getRequest()->server['remote_addr'],
            'cookie'    => $this->getRequest()->cookie,
            'get'       => $this->getRequest()->get,
            'post'      => $this->getRequest()->post,
            'files'     => $this->getRequest()->files,
        ];
    }

    function getCastData(array $list, $type): array
    {
        $res = [];
        switch ($type){
            case 'GET':
                $res = $this->castData($list, $this->getRequest()->get);
                break;
            case 'POST':
                $res = $this->castData($list, $this->getRequest()->post);
                break;
            case 'JSON':
                $data = [];
                if ($this->getRequest()->rawContent() != null){
                    $data = json_decode($this->getRequest()->rawContent(), true);
                }
                $res = $this->castData($list, $data);
                break;
        }
        return $res;
    }

    function getCastField($field, $type, $default=null, $typeData)
    {
        $res = null;
        switch ($typeData){
            case 'GET':
                $res = $this->castField($this->getRequest()->get, $field, $type, $default);
                break;
            case 'POST':
                $res = $this->castField($this->getRequest()->post, $field, $type, $default);
                break;
            case 'JSON':
                $data = [];
                if ($this->getRequest()->rawContent() != null){
                    $data = json_decode($this->getRequest()->rawContent(), true);
                }
                $res = $this->castField($data, $field, $type, $default);
                break;
        }
        return $res;
    }


    function getJSON(){
        $data = [];
        if ($this->getRequest()->rawContent() != null){
            $data = json_decode($this->getRequest()->rawContent(), true);
        }
        return $data;
    }
}