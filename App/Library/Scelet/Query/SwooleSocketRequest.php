<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 29.07.2020
 * Time: 17:11
 */

namespace App\Library\Scelet\Query;


use Swoole\Http\Request;

class SwooleSocketRequest extends BaseRequest
{
    public function __construct(Request $request)
    {
        //requset example
        //{"fd":1,"streamId":0,
        //"header":{"host":"qrdo.net","x-real-ip":"46.211.18.142","x-forwarded-for":"46.211.18.142","connection":"close","content-length":"80233","user-agent":"PostmanRuntime\/7.26.2","accept":"*\/*","cache-control":"no-cache","postman-token":"e8ee76de-73ca-49fa-977a-88c9a51bade7","accept-encoding":"gzip, deflate, br","content-type":"multipart\/form-data; boundary=--------------------------651645894666833229460892"},
        //"server":{"query_string":"ertert=edtgdfg","request_method":"POST","request_uri":"\/rter","path_info":"\/rter","request_time":1595603246,
        //"request_time_float":1595603246.881803,"server_protocol":"HTTP\/1.0","server_port":10010,"remote_port":33114,"remote_addr":"193.203.50.113","master_time":1595603246},
        //"cookie":null,"get":{"ertert":"edtgdfg"},
        //"files":{"key2":{"name":"photo5246886955835305682.jpg","type":"image\/jpeg","tmp_name":"\/tmp\/swoole.upfile.rcWxFA","error":0,"size":79902}},
        //"post":{"key1":"v1"},"tmpfiles":["\/tmp\/swoole.upfile.rcWxFA"]}

        parent::__construct($request);
    }

    /**
     * @param string $key
     * @return null|string
     */
    function getCookie(string $key)
    {
        return null;
    }

    //GET POST
    function requestType()
    {
        return 'SOCKET';
    }


    /**
     * @return array
     */
    function getUri()
    {
        if (strlen($this->request['uri']) == 0){
            return [];
        } else {
            return $this->parseURI($this->request['uri']);
        }
    }


    function requestGET(){
        $this->request->get;
    }

    function isGet(): bool
    {
        return false;
    }

    function isPost(): bool
    {
        return false;
    }

    /**
     * @return mixed
     */
    public function getRequest() : array
    {
        return $this->request;
    }

    /**
     * @param string $key
     * @return string
     */
    function getToken(string $key)
    {
        if (isset($this->request['header'][$key])){
            return $this->request['header'][$key];
        } else {
            return null;
        }
    }

    function getRequestData(){
        return $this->request['data'];
    }

    function getCastData(array $list, $type): array
    {
        $res = [];
        if (isset($this->request['data'])){
            $res = $this->castData($list, $this->request['data']);
        }

        return $res;
    }

    function getCastField($field, $type, $default=null, $typeData)
    {
        $res = null;
        if (isset($this->request['data'])){
            $res = $this->castField($this->request['data'], $field, $type, $default);
        }
        return $res;
    }

}