<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 12.06.2020
 * Time: 11:38
 */

namespace App\Library\Scelet\Query;


abstract class BaseResponse
{
    const STATUS_OK             = 200;
    const STATUS_BAD_REQUEST    = 400;
    const STATUS_UNAUTHORIZED   = 401;
    const STATUS_OLD_TOKEN      = 445;
    const STATUS_PAYMENT        = 402;
    const STATUS_NOT_FOUND      = 404;
    const STATUS_DUPLICATE      = 406;
    const STATUS_URI            = 407;
    const STATUS_BAD_LOGIN_PASS = 408;
    const STATUS_LOCKED         = 423;
    const STATUS_ERROR          = 500;

    const FRONT_CALL_REDIRECT   = 'redir';
    const FRONT_CALL_BACK       = 'back';
    const FRONT_CALL_RELOAD     = 'reload';

    public $response = [];

    abstract function setHeader(string $key, string $value);

    abstract function redirect(string $url);

    abstract function setCookie(string $key, string $cookie, int $lifeTime, string $domain);

    abstract function sendFile($path);

    abstract function send();

    public function json(array $data = []){
        $data['status'] = self::STATUS_OK;
        $this->response = array_merge($data, $this->response);
        return $this;
    }

    public function html(string $html=''){
        $this->response = $html;
        return $this;
    }

    public function setFrontCallback($function, $data=''){
        $this->response['callback'] = ['func'=>$function, 'data'=>$data];
        return $this;
    }

    public function oldToken(){
        return $this->badResponse(self::STATUS_OLD_TOKEN);
    }

    public function badURI(){
        return $this->badResponse(self::STATUS_BAD_REQUEST);
    }


    public function badLoginPass($comment, $addon=null){
        return $this->badResponse(self::STATUS_BAD_LOGIN_PASS, $comment, $addon);
    }

    public function badData($comment='Bad data', $addon=null){
        return $this->badResponse(self::STATUS_BAD_REQUEST, $comment, $addon);
    }

    public function unauth($comment, $addon=null){
        return $this->badResponse(self::STATUS_UNAUTHORIZED, $comment, $addon);
    }

    public function payment($comment, $addon=null){
        return $this->badResponse(self::STATUS_PAYMENT, $comment, $addon);
    }

    public function notFound($comment, $addon=null){
        return $this->badResponse(self::STATUS_NOT_FOUND, $comment, $addon);
    }

    public function duplicate($comment, $addon=null){
        return $this->badResponse(self::STATUS_DUPLICATE, $comment, $addon);
    }

    public function locked($comment, $addon=null){
        return $this->badResponse(self::STATUS_LOCKED, $comment, $addon);
    }

    public function error($comment, $addon=null){
        return $this->badResponse(self::STATUS_ERROR, $comment, $addon);
    }

    protected function badResponse($status, $comment=null, $addon=null){
        $data = [
            'status' => $status,
        ];
        if (!is_null($comment)){
            $data['comment']  = $comment;
        }

        if (isset($addon)){
            $data['addon'] = $addon;
        }
        $this->response = $data;
        return $this;
    }

}