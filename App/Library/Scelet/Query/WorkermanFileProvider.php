<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 29.07.2020
 * Time: 16:11
 */

namespace App\Library\Scelet\Query;


class WorkermanFileProvider extends BaseFileProvider
{

    public static function Create() : self
    {
        return new WorkermanFileProvider();
    }


    /*
     * в файл записуються по порядку индекса, нумерация с 0.
     *
     * logs(__FILE__, __LINE__, $_FILES[0]['name']); //название поля
     * logs(__FILE__, __LINE__, $_FILES[0]['file_name']); // название имени файла cmr1.jpп
     * logs(__FILE__, __LINE__, $_FILES[0]['file_size']); //размер в байтах
     * logs(__FILE__, __LINE__, $_FILES[0]['file_type']);  //тип документа   text/plain, image/jpeg....
     * logs(__FILE__, __LINE__, $_FILES[0]['file_data']) // сам файл
     */
    function upload(string $path, string $keyFile, string $nameFile)
    {
        //logs(__FILE__, __LINE__, $path.$newNamePhoto);
        $this->createDir($path);
        $file = $this->getFile($keyFile);
        file_put_contents($path.$nameFile, $file['file_data']);
    }


    public function existFileUpload($key) : bool
    {
        return isset($this->fileUploading[$key]);
    }


    public function getExtension(string $keyFile) : string
    {
        $file = $this->getFile($keyFile);
        return $this->extension($file['file_name']);
    }

    public function getFile(string $keyFile)
    {
        foreach ($_FILES as $index=>$file){
            if ($file['name'] == $keyFile){
                return $file;
            }
        }
        return null;
    }
    /*
     types {
        text/html                             html htm shtml;
        text/css                              css;
        text/xml                              xml;
        image/gif                             gif;
        image/jpeg                            jpeg jpg;
        application/javascript                js;
        application/atom+xml                  atom;
        application/rss+xml                   rss;

        text/mathml                           mml;
        text/plain                            txt;
        text/vnd.sun.j2me.app-descriptor      jad;
        text/vnd.wap.wml                      wml;
        text/x-component                      htc;

        image/png                             png;
        image/tiff                            tif tiff;
        image/vnd.wap.wbmp                    wbmp;
        image/x-icon                          ico;
        image/x-jng                           jng;
        image/x-ms-bmp                        bmp;
        image/svg+xml                         svg svgz;
        image/webp                            webp;

        application/font-woff                 woff;
        application/java-archive              jar war ear;
        application/json                      json;
        application/mac-binhex40              hqx;
        application/msword                    doc;
        application/pdf                       pdf;
        application/postscript                ps eps ai;
        application/rtf                       rtf;
        application/vnd.apple.mpegurl         m3u8;
        application/vnd.ms-excel              xls;
        application/vnd.ms-fontobject         eot;
        application/vnd.ms-powerpoint         ppt;
        application/vnd.wap.wmlc              wmlc;
        application/vnd.google-earth.kml+xml  kml;
        application/vnd.google-earth.kmz      kmz;
        application/x-7z-compressed           7z;
        application/x-cocoa                   cco;
        application/x-java-archive-diff       jardiff;
        application/x-java-jnlp-file          jnlp;
        application/x-makeself                run;
        application/x-perl                    pl pm;
        application/x-pilot                   prc pdb;
        application/x-rar-compressed          rar;
        application/x-redhat-package-manager  rpm;
        application/x-sea                     sea;
        application/x-shockwave-flash         swf;
        application/x-stuffit                 sit;
        application/x-tcl                     tcl tk;
        application/x-x509-ca-cert            der pem crt;
        application/x-xpinstall               xpi;
        application/xhtml+xml                 xhtml;
        application/xspf+xml                  xspf;
        application/zip                       zip;

        application/octet-stream              bin exe dll;
        application/octet-stream              deb;
        application/octet-stream              dmg;
        application/octet-stream              iso img;
        application/octet-stream              msi msp msm;

        application/vnd.openxmlformats-officedocument.wordprocessingml.document    docx;
        application/vnd.openxmlformats-officedocument.spreadsheetml.sheet          xlsx;
        application/vnd.openxmlformats-officedocument.presentationml.presentation  pptx;

        audio/midi                            mid midi kar;
        audio/mpeg                            mp3;
        audio/ogg                             ogg;
        audio/x-m4a                           m4a;
        audio/x-realaudio                     ra;

        video/3gpp                            3gpp 3gp;
        video/mp2t                            ts;
        video/mp4                             mp4;
        video/mpeg                            mpeg mpg;
        video/quicktime                       mov;
        video/webm                            webm;
        video/x-flv                           flv;
        video/x-m4v                           m4v;
        video/x-mng                           mng;
        video/x-ms-asf                        asx asf;
        video/x-ms-wmv                        wmv;
        video/x-msvideo                       avi;
        font/ttf                              ttf;
    }
     * */

    function getFileSize($keyFile) : int
    {
        $file = $this->getFile($keyFile);
        return $file['file_size'];
    }

    function existFiles($key): bool
    {
        return count($_FILES) > 0;
    }
}