<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 29.07.2020
 * Time: 17:42
 */

namespace App\Library\Scelet\Query;

use Swoole\Http\Request;

class SwooleFileProvider extends BaseFileProvider
{
    protected $files;

    public function __construct($files=null)
    {
        // "files": {
        //    "key2": {
        //      "name": "photo5246886955835305682.jpg",
        //      "type": "image/jpeg",
        //      "tmp_name": "/tmp/swoole.upfile.rcWxFA",
        //      "error": 0,
        //      "size": 79902
        //    }
        //}
        $this->files = $files;
    }

    public function isImage($key): bool
    {
        $type = $this->files[$key]['type'];
        switch ($type){
            case 'image/jpeg':
                return true;
            case 'image/png':
                return true;
            default:
                return false;
        }

    }

    public static function Create(Request $request) : self
    {
        return new SwooleFileProvider($request->files);
    }

    function existFileUpload($key): bool
    {
        if (is_null($this->files)){
            return false;
        }
        return isset($this->files[$key]);
    }

    function upload(string $path, string $keyFile, string $nameFile, int $ind=-1)
    {
        if ($ind == -1){
            $tmp_name =$this->files[$keyFile]['tmp_name'];
        } else {
            $tmp_name =$this->files[$keyFile][$ind]['tmp_name'];
        }

        move_uploaded_file($tmp_name,  $path.$nameFile);
    }

    /**
     * @param string $keyFile
     * @return string
     * @throws \Exception
     */
    function getExtension(string $keyFile, $ind=-1): string
    {
        if ($ind == -1){
            $file = $this->files[$keyFile];
        } else {
            $file = $this->files[$keyFile][$ind];
        }

        return $this->extension($file['name']);
    }

    function getFileSize($keyFile): int
    {
        $file = $this->files[$keyFile];
        return $file['size'];
    }

    function existFiles($key): bool
    {
        return isset($this->files);
    }
}