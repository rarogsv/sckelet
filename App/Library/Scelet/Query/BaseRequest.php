<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 18.06.2020
 * Time: 13:36
 */

namespace App\Library\Scelet\Query;


abstract class BaseRequest
{
    public $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    /**
     * @param string $uri
     * @return array
     */
    protected function parseURI(string $uri)
    {
        $requestUri = explode('/', strtok(trim($uri, '/'), '?'));
        if (strlen(end($requestUri)) == 0){
            unset($requestUri[count($requestUri)-1]);
        }
        //$this->uri = array_filter($this->uri, function($value) { return $value !== ''; });
        return $requestUri;
    }

    /**
     * @return array
     */
    abstract function getUri();

    /**
     * @param string $key
     * @return string
     */
    abstract function getToken(string $key);

    /**
     * @param string $key
     * @return string
     */
    abstract function getCookie(string $key);

    /**
     * @return string
     */
    abstract function requestType();

    /**
     * @return bool
     */
    abstract function isGet();

    /**
     * @return bool
     */
    abstract function isPost();


    abstract function getCastData(array $list, $type): array;



    function castData(array $list, $data): array
    {
        $res = [];

        foreach ($list as $item){
            //logs(__FILE__, __LINE__, $item);
            if (isset($data[$item[0]])){//есть с данным ключом
                if (count($item) == 3){
                    $default = $item[2];
                } else {
                    $default = null;
                }
                $res[$item[0]] = $this->castField($data, $item[0], $item[1], $default, $item);
            } else {
                /*switch ($item[1]){
                    case BOOL:
                        $res[$item[0]] = 0;
                        break;
                    default:

                }*/
                //logs(__FILE__, __LINE__, $item);
                //logs(__FILE__, __LINE__, $item[0]);

                if (isset($item[2])){ //есть дэфолт значение
                    $res[$item[0]] = $item[2];
                } else if (isset($item[1]) == BOOL){
                    $res[$item[0]] = 0;
                }
            }
        }
        return $res;
    }

    function castField($data, $field, $type, $default=null, $all=[]){
        $res = $default;
        if (isset($data[$field])){
            switch ($type){
                case STRING:
                    $res = trim(addslashes($data[$field]));
                    break;
                case INT:
                    $res = intval($data[$field]);
                    break;
                case TIME_STAMP:
                    $res = (int)strtotime($data[$field]);
                    break;
                case FLOAT:
                    $res = floatval($data[$field]);
                    break;
                case MONEY:
                    $val = intval($data[$field]*100);
                    /*if ($val < 0){
                        $val = -1 * $val;
                    }*/
                    $res = $val;
                    break;
                case BOOL:
                    $res = intval($data[$field]);
                    break;
                case DECODE:
                    if (is_array($data[$field])){
                        $res = $data[$field];
                    } else {
                        $res = json_decode($data[$field], true);
                    }
                    break;
                case EMAIL:
                    if (filter_var($data[$field], FILTER_VALIDATE_EMAIL) !== false){
                        $res = $data[$field];
                    } else {
                        $res = '';
                    }
                    break;
                case INNER:
                    //all - 0-field, 1-type, 2-arr, 3-default
                    if (array_search($data[$field], $all[2])===false){
                        $res = $all[3];
                    } else {
                        $res = $data[$field];
                    }
                    break;

            }
        } else {
            switch ($type){
                case BOOL:
                    $res = 0;
                    break;
                default:
                    $res = $default;
            }
        }
        return $res;
    }


}