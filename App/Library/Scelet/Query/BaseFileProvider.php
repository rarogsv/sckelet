<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 07.03.2019
 * Time: 18:40
 */

namespace App\Library\Scelet\Query;



use Exception;

abstract class BaseFileProvider{

    const IMAGE_TYPE = 'image';
    const TEXT_TYPE = 'text';
    const VIDEO_TYPE = 'video';
    const AUDIO_TYPE = 'audio';

    protected $MAX_SIZE = 1; //MAX size



    /**
     * @param string $fullPath
     * @param int $rotate
     */
    public function rotate(string $fullPath, int $rotate){
        $source = imagecreatefromjpeg($fullPath);
        $rotate = imagerotate($source, $rotate, 0);
        imagejpeg($rotate, $fullPath);
        list($width, $height, $type, $attr) = getimagesize( $fullPath);
    }

    //full path -/home/public/avatar.jpg

    public function createDir(string  $path, $mode = 0777){
        if (!file_exists($path)) {
            mkdir($path, $mode, true);
        }
    }

    public static function createFolder(string  $path, $mode = 0777){
        if (!file_exists($path)) {
            mkdir($path, $mode, true);
        }
    }

    /**
     * @param string $filename
     * @return string
     * @throws Exception
     */
    protected function extension(string $filename) : string
    {
        $data = explode('.', $filename);
        if (count($data) > 1){
            return end($data);
            //list($fileNameUser, $extension) = $data;
            //return $extension;
        } else {
            throw new Exception('Bad extension');
        }
    }



    public static function delFile(string $path, string $name)
    {
        //logs(__FILE__, __LINE__, $path);
        //logs(__FILE__, __LINE__, $name);
        if (file_exists($path.$name)) {
            //logs(__FILE__, __LINE__, 'del');
            unlink($path.$name);
            return true;
        } else {
            return false;
        }
    }

    public static function delFile2(string $pathFull)
    {
        logs(__FILE__, __LINE__, $pathFull);
        if (file_exists($pathFull)) {
            logs(__FILE__, __LINE__, 'del');
            unlink($pathFull);
            return true;
        } else {
            return false;
        }
    }


    //exif_imagetype($this->fileUploading[$type]["tmp_name"]); — Определение типа изображения
    /* public function checkExistsDir()
     {
         $res =  move_uploaded_file($this->dir.$this->name.'.'.$this->extension);
         logs(__FILE__, __LINE__,$this->dir.$this->name.'.'.$this->extension.'---'.json_encode($res));
         return $res;
     }*/



    //var1
    public static function removeFiles(string $dir)
    {
        $files = glob($dir.'*'); // get all file names
        foreach($files as $file){ // iterate files
            if(is_file($file))
                unlink($file); // delete file
        }
    }
    public static function removeDirectory(string $dir) {
        foreach (glob($dir) as $file) {
            if (is_dir($file)) {
                self::removeDirectory("$file/*");
                rmdir($file);
            } else {
                unlink($file);
            }
        }
    }

    abstract function existFiles($key) : bool;

    abstract function existFileUpload($key) : bool;

    abstract function getFileSize($keyFile) : int ;

    abstract function getExtension(string $keyFile) : string;

    abstract function upload(string $path, string $keyFile, string $nameFile);
}
