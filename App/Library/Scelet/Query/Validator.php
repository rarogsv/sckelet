<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 21.02.2020
 * Time: 17:07
 */

namespace App\Library\Scelet\Query;

use App\Library\Scelet\ControllersBase\Exeptions\BadData;
use DateTime;
use Swoole\Http\Request;

class Validator
{

    private $get = [];
    private $post = [];
    private $json = [];
    private $key = '';

    public function __construct($get, $post, $json=[])
    {
        $this->get = $get;
        $this->post = $post;
        $this->json = $json;
    }

    public static function Cast($get, $post, $json){
        return new Validator($get, $post, $json);
    }






    /**
     * @param string $key
     * @return Validator
     */
    public function verifyField(string $key)
    {
        $this->key = $key;
        return $this;
    }

    protected function _getValue(int $who){
        switch ($who){
            case 0:
                if (!isset($this->get[$this->key])){return null;}
                return $this->get[$this->key];
            case 1:
                if (!isset($this->post[$this->key])){return null;}
                return $this->post[$this->key];
            case 2:
                //logs(__FILE__, __LINE__, $this->json);
                //logs(__FILE__, __LINE__, $this->key);
                //logs(__FILE__, __LINE__, isset($this->json[$this->key]));
                if (!isset($this->json[$this->key])){return null;}
                return $this->json[$this->key];
        }
        return null;
    }

    /**
     * @param int $max
     * @return $this
     * @throws BadData
     */
    public function maximum(int $max, int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || $v > $max){
            throw new BadData('Lower '.$max.', '.$this->key);
        }
        return $this;
    }

    /**
     * @param int $min
     * @return $this
     * @throws BadData
     */
    public function minimum(int $min, int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || $v < $min){
            throw new BadData('Larger '.$min.', '.$this->key);
        }

        return $this;
    }



    /**
     * @param int $length
     * @return Validator
     * @throws BadData
     */
    public function length(int $length, int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || strlen($v) !== $length){
            throw new BadData('Length != '.$length.', '.$this->key);
        }
        return $this;
    }


    /**
     * @param int $length
     * @return Validator
     * @throws BadData
     */
    public function arrLength(int $length, int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || count($v) !== $length){
            throw new BadData('Length array != '.$length.', '.$this->key);
        }

        return $this;
    }


    /**
     * @param int $length
     * @return Validator
     * @throws BadData
     */
    public function minLength(int $length, int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || strlen($v) < $length){
            throw new BadData('Length < '.$length.', '.$this->key);
        }

        return $this;
    }


    /**
     * @param int $length
     * @return Validator
     * @throws BadData
     */
    public function maxLength(int $length, int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || strlen($v) > $length){
            throw new BadData('Length > '.$length.' - '.$this->key);
        }

        return $this;
    }


    /**
     * @param int $who
     * @return Validator
     * @throws BadData
     */
    public function isId(int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || !is_numeric($this->post[$this->key]) || $this->post[$this->key] < -1){
            throw new BadData('Is not id, '.$this->key);
        }

        return $this;
    }

    /**
     * @return $this
     * @throws BadData
     */
    public function verifyLanguage(int $who=1){
        $this->key = 'lang';

        $this->length(2, $who);

        return $this;
    }


    /**
     * @return $this
     * @throws BadData
     */
    public function isLogin(int $who=1){
        $this->minLength(4, $who);
        $this->maxLength(20, $who);

        return $this;
    }

    /**
     * @return $this
     * @throws BadData
     */
    public function isPass(int $who=1){
        $this->minLength(4, $who);
        $this->maxLength(20, $who);

        return $this;
    }

    /**
     * @return $this
     * @throws BadData
     */
    public function isName(int $who=1) : self
    {
        $this->minLength(2, $who);
        $this->maxLength(25, $who);

        return $this;
    }

    /**
     * @return $this
     * @throws BadData
     */
    public function isPhone(int $who=1) : self
    {
        $this->minLength(12, $who);
        $this->maxLength(16, $who);

        return $this;
    }



    /**
     * @return Validator
     * @throws BadData
     */
    public function isBool(int $who=1) : self
    {
        $this->minimum(0, $who);
        $this->maximum(1, $who);

        return $this;
    }


    /**
     * @return Validator
     * @throws BadData
     */
    public function isNumeric(int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v) || !is_numeric($this->post[$this->key])){
            throw new BadData('Is not numeric, '.$this->key);
        }

        return $this;
    }





    /**
     * @param $from
     * @param $to
     * @return Validator
     * @throws BadData
     */
    public function range($from, $to, int $who=1):self {
        $v = $this->_getValue($who);

        if (is_null($v) || $v>$to || $v < $from){
            throw new BadData('Range');
        }
        return $this;
    }


    /**
     * @param int $who
     * @return Validator
     * @throws BadData
     */
    public function isEmail(int $who=1) : self
    {
        $v = $this->_getValue($who);

        if (is_null($v ) || !filter_var($v, FILTER_VALIDATE_EMAIL)){
            throw new BadData('Is not email, '.$this->key);
        }

        return $this;
    }

    /**
     * @param array $keys
     * @param int $who
     * @return Validator
     * @throws BadData
     */
    public function exist(array $keys, int $who=1) : self
    {

        //logs(__FILE__, __LINE__, $this->json);
        foreach ($keys as $key){

            //logs(__FILE__, __LINE__, $this->post);
            //logs(__FILE__, __LINE__, !isset($this->post[$key]));
            $this->key = $key;
            $v = $this->_getValue($who);
            //logs(__FILE__, __LINE__, $key);
            //logs(__FILE__, __LINE__, $v);
            if (is_null($v)){
                throw new BadData('Not found, '.$key);
            }
        }

        return $this;
    }

    /**
     * @param string $key
     * @param int $who
     * @return bool
     */
    public function issetField(int $who=1){
        $v = $this->_getValue($who);
        return !is_null($v );

    }




    /**
     * @param string $format
     * @return $this
     * @throws BadData
     */
    public function isDate($format = 'Y-m-d', int $who=1){
        if (!$this->_validateDate($format, $who)){
            throw new BadData('Is not date format='.$format.', '.$this->key);
        }
        return $this;
    }

    /**
     * @param string $format
     * @return $this
     * @throws BadData
     */
    public function isTime($format = 'H:i:s', int $who=1){
        if (!$this->_validateDate($format, $who)){
            throw new BadData('Is not time format='.$format.', '.$this->key);
        }
        return $this;
    }

    /**
     * @param string $format
     * @return $this
     * @throws BadData
     */
    public function isDateTime($format = 'Y-m-d H:i:s', int $who=1){

        if (!$this->_validateDate($format, $who)){
            throw new BadData('Is not date-time format='.$format.', '.$this->key);
        }
        return $this;
    }

    /**
     * @param $format
     * @return bool
     */
    protected function _validateDate($format, int $who) : bool
    {
        $v = $this->_getValue($who);
        $d = DateTime::createFromFormat($format, $v);
        return $d && $d->format($format) == $v;
    }


}