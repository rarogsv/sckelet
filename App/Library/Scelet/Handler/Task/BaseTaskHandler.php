<?php

/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 31.01.2019
 * Time: 1:12
 */
namespace App\Library\Scelet\Handler\Task;


/**
 * Class MyTask
 * @package App\Model\Handler
 */
abstract class BaseTaskHandler
{
    protected $timers = [];
    protected $tasks=[];
    protected $DAY_SEC = 86400;

    abstract public function run();


    /**
     * @param BaseTask $task
     */
    public function setTasks(BaseTask $task)
    {
        $this->tasks[] = $task;
    }

    /**
     * @return array
     */
    public function getTasks()
    {
        return $this->tasks;
    }


}