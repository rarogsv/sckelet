<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 11.03.2019
 * Time: 10:43
 */

namespace App\Library\Scelet\Handler\Task;



/**
 * Class Task
 * @package App\Model\Handler
 */
abstract class BaseTask
{
    protected $_action;
    protected $_model;
    protected $_dataUpdate;
    /**
     * @var array
     */
    private $dataUpdate;

    /**
     * Task constructor.
     * @param string $action
     * @param $model
     */
    public function __construct(string $action, array $model, array $dataUpdate)
    {
        $this->_action = $action;
        $this->_model = $model;
        $this->_dataUpdate = $dataUpdate;
    }




    /**

     * @return bool
     */
    abstract function run() : bool ;


}