<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 02.01.2022
 * Time: 22:10
 */

namespace App\Library\Scelet\DataBase;


use App\Library\Scelet\ModelBase\BaseGlobalData;
use App\Model\GlobalData;

class PDORequest
{

    protected $dbName = DB_NAME_DEFAULT;
    protected $requestWhere = null;
    protected $requestOrder = null;
    protected $numbReq = 0;
    protected $requestData = [];
    protected $multitable = [];
    protected $badInnerData = false;

    /**
     * @param array $columns - item column
     * @param string $table
     * @param array $as - [[column=>def value]]
     * @return $this
     */
    public function setMultitable(array $columns, string $table, array $missingColumns=[])
    {
        logs(__FILE__, __LINE__, 'setMultitable');
        $arrayDiff = array_diff($columns, array_keys($missingColumns));
        $subQuery = "SELECT ".implode(',', $arrayDiff).",";
        if (count($missingColumns) > 0){
            foreach ($missingColumns as $key=>$val){
                $subQuery .="'".$val."' AS ".$key.",";
            }
        }
        $subQuery .= "'".$table."' AS tbl FROM ".$table;
        $this->multitable[] = $subQuery;
        //logs(__FILE__, __LINE__, $this->multitable);
        return $this;
    }

    protected function clear(){
        $this->requestWhere = null;
        $this->requestOrder = null;
        $this->numbReq = 0;
        $this->badInnerData = false;
        $this->requestData = [];
        $this->multitable = [];
    }

    public static function Create(){
        return new PDORequest();
    }


    public function connection($dbName){
        $this->dbName = $dbName;
        return $this;
    }

    /**
     * @param string|array $column
     * @param string $columnValue
     * @param string $operator
     * @return PDORequest
     */
    public function where($column, $columnValue = null, $operator = '=')
    {

        if (is_array($column)){
            foreach ($column as $key => $value){
                $this->generateWhere($key, $value, $operator, 'AND');
            }
            return $this;
        } else {
            //logs(__FILE__, __LINE__, !is_null($columnValue));
            if (!is_null($columnValue)){
                $this->generateWhere($column, $columnValue, $operator, 'AND');
            }
        }
        //logs(__FILE__, __LINE__, $column.'-'.$columnValue);
        //logs(__FILE__, __LINE__, $this->requestWhere);
        return $this;
    }

    //"SELECT * FROM registry WHERE firstname = :firstname OR lastname = :lastname"
    //$COL_TBL_NUM.' = :tblNum_INT AND '.$COL_MAT_STATE.' = '.$MAT_STATE_ACT.' AND '.$COL_LIN_1_UCO.' < :tblUcoDataLine1UcoMax_INT '.
    //                'ORDER BY '.$COL_LINED.' ASC, '.$COL_LINSOR.' ASC';

    protected function generateWhere($column, $columnValue = null, $operator = '=', $cond){
        if (isset($this->requestData[$column])){
            return;
        }

        $this->numbReq++;
        //logs(__FILE__, __LINE__, $column.'-'.$columnValue);
        if (is_null($this->requestWhere)){
            //logs(__FILE__, __LINE__, 1);
            $this->requestWhere = 'WHERE';
        } else {
            if ($cond == 'AND' || $cond == 'OR'){
               // logs(__FILE__, __LINE__, 2);
                $this->requestWhere .= ' '.$cond;
            }
        }
        //logs(__FILE__, __LINE__, $operator);
        switch ($operator){
            case 'LIKE':
                //SELECT book_id, title FROM books WHERE title LIKE :pattern
                $key = 'pattern'.$this->numbReq;
                $this->requestWhere .= ' '.$column.' LIKE :'.$key;
                $this->requestData[$key] = $columnValue;
                break;
            case 'BETWEEN':
                //SELECT SOMETHING FROM grading WHERE SOMETHING BETWEEN max AND min
                if (is_array($columnValue)){
                    $this->requestWhere .= ' '.$column.' BETWEEN :start_'.$column.' AND :end_'.$column;
                    $this->requestData['start_'.$column] = $columnValue[0];
                    $this->requestData['end_'.$column] = $columnValue[1];
                }
                break;
            case 'IN':
                //logs(__FILE__, __LINE__, $columnValue);
                //logs(__FILE__, __LINE__, is_array($columnValue));
                if (is_array($columnValue) && count($columnValue) > 0){
                    $preparedInValues = [];
                    foreach ($columnValue as $ind=>$value){
                        $newCol = 'val'.$ind;
                        $this->requestData[$newCol] = $value;
                        $preparedInValues[]=':'.$newCol;
                        //$preparedInValues[] = $value;
                    }


                    $this->requestWhere .= ' '.$column.' IN ('.implode(',', $preparedInValues).')';
                    //logs(__FILE__, __LINE__, str_repeat('?,', count($columnValue) - 1) . '?');
                } else {
                    $this->badInnerData = true;
                }
                break;

            default:
                $this->requestWhere .= ' '.$column.' '.$operator.' :'.$column;
                $this->requestData[$column] = $columnValue;
        }

        /*
        // other parameters that are going into query
        $params = ["foo" => "foo", "bar" => "bar"];
        $ids = [1,2,3];
        $in = "";
        $i = 0; // we are using an external counter
        //because the actual array keys could be dangerous
        foreach ($ids as $item) {
            $key = ":id".$i++;
            $in .= ($in ? "," : "") . $key; // :id0,:id1,:id2
            $in_params[$key] = $item; // collecting values into a key-value array
        }
        $sql = "SELECT * FROM table WHERE foo=:foo AND id IN ($in) AND bar=:bar";
        $stm = $db->prepare($sql);
        $stm->execute(array_merge($params,$in_params)); // just merge two arrays
        $data = $stm->fetchAll();

        */
    }



    public function orWhere($whereProp, $whereValue = null, $operator = '=')
    {
        if (is_array($whereProp)){
            foreach ($whereProp as $key => $value){
                $this->generateWhere($whereProp, $whereValue, $operator, 'OR');
            }
        } else {
            if (!is_null($whereValue)){
                $this->generateWhere($whereProp, $whereValue, $operator, 'OR');
            }
        }
        return $this;
    }

    //SELECT pole FROM table WHERE text2 <= :txt2 ORDER BY text2 DESC LIMIT 1
    public function orderBy($column, $orderbyDirection = "DESC")
    {
        $this->requestOrder = 'ORDER BY '.$column.' '.$orderbyDirection;
        return $this;
    }

    public function getIDList($tableName, $key='id') : array
    {
        $res = $this->get($tableName, [$key]);
        if (count($res) > 0){
            return array_unique(array_column($res, $key));
        } else {
            return [];
        }
    }

    /*
     *
     SELECT
  users.first_name,
  users.last_name,
  orders.order_id,
  orders.order_date
FROM
  users
JOIN
  orders
ON
  users.id = orders.user_id;
     */


    /**
     * @param string $tableName
     * @param null $numRows
     * @param string $columns
     * @return
     * @throws \Exception
     */
    public function get($tableName, $columns = '*', $numRows = null)
    {
        //SELECT name, color, calories FROM fruit ORDER BY name
        if ($this->badInnerData){
            $this->clear();
            return [];
        }

        $req = 'SELECT ';
        if (is_array($columns)){
            $req .= implode(',', $columns);
        } else {
            $req .= $columns;
        }

        $req .= ' FROM ';

        if (count($this->multitable) > 0){
            $req .= '('.implode(' UNION ALL ', $this->multitable). ') ';
        }

        $req .= $tableName.$this->contactReq();

        if (!is_null($numRows)){
            //logs(__FILE__, __LINE__, $numRows);
            /**
            Чтобы вернуть результаты с 6 по 15, нужно использовать такой синтаксис:
            SELECT * FROM users ORDER BY id DESC LIMIT 5(пропустить), 10(количество строк)
             */
            if (is_array($numRows)) {
                $req .= 'LIMIT '.$numRows[0].','.$numRows[1];
            } else {
                $req .= 'LIMIT '.$numRows;
            }
        }
        //logs(__FILE__, __LINE__, $req);

        $statement  = $this->execute($req);
        $res = $statement->fetchAll();
        if (!is_null($res) && count($res) > 0){
            foreach ($res as $i => $item){
                foreach ($item as $key => $value){
                    if (is_string($value)){
                        $res[$i][$key] = stripslashes($value);
                    }
                }
            }
        }

        return $res;

    }

    public function inc($tableName, $column, $count)
    {
        //SELECT name, color, calories FROM fruit ORDER BY name
        if ($this->badInnerData){
            $this->clear();
            return [];
        }
        return $this->incDec($tableName, $column, $count, '+');
    }

    public function dec($tableName, $column, $count)
    {
        //SELECT name, color, calories FROM fruit ORDER BY name
        if ($this->badInnerData){
            $this->clear();
            return [];
        }
        return $this->incDec($tableName, $column, $count, '-');
    }

    protected function incDec($tableName, $column, $count, $char){
        $req = 'UPDATE '.$tableName.' SET '.$column.'='.$column.$char.$count.' '.$this->contactReq();

        logs(__FILE__, __LINE__, $req);

        $statement  = $this->execute($req);
        $res = $statement->fetchAll();
        if (!is_null($res) && count($res) > 0){
            foreach ($res as $i => $item){
                foreach ($item as $key => $value){
                    if (is_string($value)){
                        $res[$i][$key] = stripslashes($value);
                    }
                }
            }
        }

        return $res;
    }

    public function getOne($tableName, $columns = '*')
    {
        if ($this->badInnerData){
            $this->clear();
            return null;
        }

        $res = $this->get($tableName, $columns, 1);
        //logs(__FILE__, __LINE__, $res);
        if (count($res) > 0){
            return $res[0];
        } else {
            return null;
        }
    }

    protected function contactReq(){
        $res = ' ';
        if (!is_null($this->requestWhere)){
            $res .= $this->requestWhere.' ';
        }
        if (!is_null($this->requestOrder)){
            $res .= $this->requestOrder.' ';
        }

        return $res;
    }


    public function delete($tableName)
    {
        $req = 'DELETE FROM '.$tableName.$this->contactReq();
        $statement = $this->execute($req);
        return $statement->rowCount();
    }

    public function execute($req){

        BaseGlobalData::$lastRequest = json_encode(['request' => $req, 'data'=>$this->requestData]);
        logs(__FILE__, __LINE__,$req);
        logs(__FILE__, __LINE__,$this->requestData);

        $statement = BaseGlobalData::$connects[$this->dbName]->prepare($req);
        $statement->execute($this->requestData);

        $this->clear();
        return $statement;
    }

    /**
     * @param string $tableName
     * @param array $tableData
     * @param array $inc = [[col, '+'/'-', val],...]
     * @return mixed
     */
    public function update( string $tableName, array $tableData=[], array $inc=[])
    {
        if ($this->badInnerData){
            $this->clear();
            return 0;
        }

        $preparedInValues = [];
        foreach ($tableData as $column=>$value){
            /*if (is_string($value)){
                $this->requestData[$column] = addslashes($value);
            } else {
                $this->requestData[$column] = $value;
            }*/
            $this->requestData[$column] = $value;
            $preparedInValues[]= $column.'=:'.$column;
        }
        foreach ($inc as $d){
            $preparedInValues[]= $d[0].'='.$d[0].$d[1].abs($d[2]);
        }
        $req = 'UPDATE '.$tableName.' SET '.implode(',', $preparedInValues).' '.$this->contactReq();
        //"UPDATE users SET name=:name, surname=:surname, sex=:sex WHERE id=:id"
        //logs(__FILE__, __LINE__, 'update');
        $statement = $this->execute($req);
        return $statement->rowCount();
    }

    public function insert($tableName, $tableData)
    {
        if ($this->badInnerData){
            $this->clear();
            return null;
        }

        $preparedInValues = [];
        foreach ($tableData as $column=>$value){
            $this->requestData[$column] = $value;
            $preparedInValues[]= $column.'=:'.$column;
        }
        $req = 'INSERT INTO '.$tableName.' SET '.implode(',', $preparedInValues).' '.$this->contactReq();
        //logs(__FILE__, __LINE__, $req);
        //logs(__FILE__, __LINE__, $this->requestData);

        $statement = $this->execute($req);
        return GlobalData::$connects[$this->dbName]->lastInsertId();
    }




    /**
     * @param $lat
     * @param $lng
     * @param $radiusKM
     * @return mixed
     * @throws \Exception
     */
    public function searchByLocation($table, $lat, $lng, $radiusKM){
        $ids = $this->rawQueryValue("SELECT id, 
                        111.045 * DEGREES(ACOS(COS(RADIANS($lat))* COS(RADIANS(lat))* COS(RADIANS(lng) - RADIANS($lng))+ SIN(RADIANS($lat))* SIN(RADIANS(lat)))) 
                      AS distance FROM ".$table." HAVING distance < " . $radiusKM . " ORDER BY distance");

        if (count($ids) > 0){
            return $this->where('id', $ids, 'IN')->get($table);
        } else {
            return [];
        }
    }

    public function ping(){
        $this->execute('SELECT 1');
    }

}