<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 11.06.2020
 * Time: 15:10
 */

namespace App\Library\Scelet\Util;


use Exception;
use Firebase\JWT\BeforeValidException;
use Firebase\JWT\ExpiredException;
use Firebase\JWT\JWT;
use Firebase\JWT\Key;
use Firebase\JWT\SignatureInvalidException;
use InvalidArgumentException;
use UnexpectedValueException;

class AccessHandler
{
    protected $typeEncript;
    protected $key='';
    public function __construct(string $key, string $typeEncript = 'HS256')
    {
        $this->key = $key;
        $this->typeEncript = $typeEncript;
    }

    /**
     * @param array $data
     * @param null $pass
     * @return string
     */
    function encode(array $data, $pass=null){
        logs(__FILE__, __LINE__, $data);


        if (is_null($pass)){
            logs(__FILE__, __LINE__, $this->key);
            return JWT::encode($data, $this->key, $this->typeEncript);
        } else {
            logs(__FILE__, __LINE__, 1);
            return JWT::encode($data, $pass, $this->typeEncript);
        }
    }

    /**
     * @param string $token
     * @param null $pass
     * @return array
     * @throws InvalidArgumentException     Provided JWT was empty
     * @throws UnexpectedValueException     Provided JWT was invalid
     * @throws SignatureInvalidException    Provided JWT was invalid because the signature verification failed
     * @throws BeforeValidException         Provided JWT is trying to be used before it's eligible as defined by 'nbf'
     * @throws ExpiredException             Provided JWT has since expired, as defined by the 'exp' claim
     */
    function decode(string $token, $pass=null)
    {

        if (is_null($pass) || strlen($pass) == 0){
            //logs(__FILE__, __LINE__, 1);
            //return (array)JWT::decode($token, $this->key, [$this->typeEncript]);
            return (array)JWT::decode($token, new Key($this->key, $this->typeEncript));
        } else {
            //logs(__FILE__, __LINE__, 1);
            //return (array)JWT::decode($token, $pass, [$this->typeEncript]);
            return (array)JWT::decode($token, new Key($pass, $this->typeEncript));
        }
    }

    function myEncode(string $data, $key=null, $algos='sha256'){
        if (is_null($key)){
            $key = $this->key;
        }
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);
        $ciphertext_raw = openssl_encrypt($data, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac($algos, $ciphertext_raw, $key, $as_binary=true);
        return base64_encode( $iv.$hmac.$ciphertext_raw );
    }

    /**
     * @param string $token
     * @param null $key
     * @return string
     * @throws \Exception
     * //https://www.php.net/manual/ru/function.hash-hmac-algos.php
     */

    function myDecode(string $token, $key=null, $algos='sha256'){
        if (is_null($key)){
            $key = $this->key;
        }
        $c = base64_decode($token);
        $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
        $iv = substr($c, 0, $ivlen);
        $hmac = substr($c, $ivlen, $sha2len=32);
        $ciphertext_raw = substr($c, $ivlen+$sha2len);
        $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
        $calcmac = hash_hmac($algos, $ciphertext_raw, $key, $as_binary=true);
        if (hash_equals($hmac, $calcmac))// с PHP 5.6+ сравнение, не подверженное атаке по времени
        {
            logs(__FILE__, __LINE__, $original_plaintext);
            return $original_plaintext;
        } else {
            throw new Exception('Bad token');
        }
    }

    public static function randomPassword() {
        $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
        $pass = array(); //remember to declare $pass as an array
        $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass); //turn the array into a string
    }

}