<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 11.03.2020
 * Time: 13:11
 */
namespace App\Library\Scelet\Util;

class DateHelper
{

    /**
     * https://www.php.net/manual/ru/function.date.php
     * @param string $format
     * @return int
     */
    public function getVariableOfDate(string $format) : int
    {
        if (!array_search($format, [self::NAME, 'j', 'z', 'W', self::NAME, self::CONTENT_TYPE, 'G', 'H', 'i', self::SORT, 'v'])){
            return -1;
        }
        return date($format, strtotime(time()));
    }

}