<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 02.01.2022
 * Time: 13:16
 */

namespace App\Library\Scelet\Util;


use App\Library\Scelet\ModelBase\BaseGlobalData;
use App\Library\Scelet\ModelBase\Servers;
use App\Library\Scelet\Query\BaseRequest;
use App\Library\Scelet\Query\SwooleHtppRequest;
use App\Library\Scelet\Query\SwooleHttpResponse;
use App\Model\GlobalData;
use Swoole\Http\Server;
use Throwable;

class ServerHandler
{
    const ON          = 'on';
    const OFF         = 'off';
    const SERVER_RESET       = 'reset';

    const UPDATE             = 'update';
    const UPDATE_BACK        = 'update_back';

    const UPDATE_SCRIPT             = 'update_script';
    const UPDATE_BACK_SCRIPT        = 'update_back_script';

    public $server = null;

    public function __construct()
    {
        $this->server = Servers::Load(ID_SERVER);
    }

    public function serverConroller(SwooleHtppRequest $request, SwooleHttpResponse $response, BaseGlobalData $globalData, Server $server){
        $uri = $request->getUri();
        logs(__FILE__, __LINE__, 'serverConroller');
        $demons= json_decode($request->getRequest()->get['demons']);
        switch ($uri[1]){

            case self::UPDATE_BACK:
                logs(__FILE__, __LINE__, 'UPDATE_BACK');
                if (!IS_TEST) {
                    $this->killDemons($demons);
                    $this->backGit();

                    //logs(__FILE__, __LINE__, $res1);
                    //logs(__FILE__, __LINE__, $res2);
                    //logs(__FILE__, __LINE__, [IP == MAIN_SERVER_IP]);
                    //$this->runDemons($demons);
                    $server->reload();
                }
                break;

            case self::ON:
                $this->runDemons($demons);
                break;

            case self::OFF:
                $this->killDemons($demons);
                break;

            case self::UPDATE:
                logs(__FILE__, __LINE__, 'UPDATE');
                logs(__FILE__, __LINE__, $demons);
                //return;
                if (!IS_TEST){
                    $this->killDemons($demons);
                    $res1 = $this->updateGit();

                    //logs(__FILE__, __LINE__, [IP == MAIN_SERVER_IP]);
                    //$this->runDemons($demons);
                    $server->reload();
                }
                break;

            case self::UPDATE_SCRIPT:
                logs(__FILE__, __LINE__, 'UPDATE_SCRIPT');
                $this->upScript();
                break;
            case self::UPDATE_BACK_SCRIPT:
                logs(__FILE__, __LINE__, 'UPDATE_SCRIPT');
                $this->backScript();
                break;

            case self::SERVER_RESET:
                logs(__FILE__, __LINE__, 'SERVER_RESET');
                if (!IS_TEST) {
                    foreach ($demons as $demon) {
                        self::killDemon($demon);
                        self::runDemon($demon);
                        $server->reload();
                    }
                }
                break;
        }
        //self::restart($demons);
    }

    public function runDemons($demons){
        foreach ($demons as $demon){
            self::runDemon($demon);
        }
    }

    public function killDemons($demons){
        foreach ($demons as $demon){
            self::killDemon($demon);
        }
    }

    public function backGit(){
        //logs(__FILE__, __LINE__, 'chack');
        //logs(__FILE__, __LINE__, $hash);
        if (strlen($obj->git_hash_beafore) == 0){return null;}

        exec('git reset --hard '.$obj->git_hash_beafore, $res);
        //logs(__FILE__, __LINE__, $res);

        $this->server->update([
            'git_hash_beafore'  => '',
            'git_hash'          => $this->server->git_hash_beafore
        ]);

        $this->updateComposer();
    }

    public function updateGit()
    {
        logs(__FILE__, __LINE__, 'updateGit');
        $res_git = '';
        $res_composer = '';
        exec('cd '.DIR.'; git pull '.URL_GIT.' master', $res_git);
        logs(__FILE__, __LINE__, $res_git);
        if (is_array($res_git)){
            logs(__FILE__, __LINE__, 1);
            if (count($res_git) > 0){
                //logs(__FILE__, __LINE__, $res);
                $d1 = explode(' ', $res_git[0]);
                if (count($d1) == 2){
                    //["76b8cc1","8478937"] 0-old commit, 1-new commit
                    $d2 = explode('..', $d1[1]);
                    foreach ($d2 as &$d){
                        $d = trim($d);
                    }

                    logs(__FILE__, __LINE__, $d2);
                    if (is_array($d2) && count($d2) > 1){
                        logs(__FILE__, __LINE__, 'update server');
                        $this->server->update([
                            'git_hash_beafore'  => $this->server->git_hash,
                            'git_hash'          => $d2[1]
                        ]);
                        $res_composer = $this->updateComposer();

                        logs(__FILE__, __LINE__, $res_composer);
                        if (is_array($res_composer)){
                            $res_composer = json_encode($res_composer, JSON_UNESCAPED_UNICODE );
                        }
                    }
                }
            }

            $res_git = json_encode($res_git, JSON_UNESCAPED_UNICODE );
        }

        logs(__FILE__, __LINE__, $res_git);
        $this->server->update([
            'comment_git'  => $res_git,
            'comment_composer'  => $res_composer
        ]);
    }


    public function updateComposer(){
        if (IS_TEST){
            return [];
        }
        exec('cd '.DIR.'; php composer.phar update;', $res);
        return $res;
    }


    public function restart($demons){
        foreach ($demons as $demon){
            self::killDemon($demon);
            self::runDemon($demon);
        }
    }


    public static function runDemon($name)
    {
        logs(__FILE__, __LINE__, 'runDemon-'.$name);
        exec('cd '.DIR_DEMON.'; php '.$name, $res);
        //logs(__FILE__, __LINE__, $res);
    }

    //{"admin":{"web.php":[1657,1658,1660],"stream.php":[1754,1755,1757],"runer.php":[74728]}}
    static function getRunDemons(){
        $res = preg_replace('/\s+/', ' ', shell_exec('ps aux | grep php'));
        $res = explode(' ', $res);
        //logs(__FILE__, __LINE__, $res);
        $demons = [];
        foreach ($res as $ind=>$val){
            if ( $val =='php' && strpos($res[$ind+1], '.php') !== false){
                $userName = $res[$ind-10];
                $pid =  intval($res[$ind-9]);
                $demon = $res[$ind+1];
                if (!isset($demons[$userName])){
                    $demons[$userName] = [
                        $demon => []
                    ];
                }
                $demons[$userName][$demon][]=$pid;
            }
        }
        //logs(__FILE__, __LINE__, $demons);
        return $demons;
    }

    public static function killDemon($name, $pin=0){
        //new
        $runDemonsAll = self::getRunDemons();
        logs(__FILE__, __LINE__, 'killDemon-'.$name);
        logs(__FILE__, __LINE__, $runDemonsAll);
        //logs(__FILE__, __LINE__, 1);
        //const USER_SERVER_NAME_SHORT = 'TaxiBli';
        foreach ($runDemonsAll as $key => $runDemonsUser){
            if (isset($runDemonsUser[$name])){
                foreach ($runDemonsUser[$name] as $pid){
                    if ($pin != $pid){
                        exec("kill -9 $pid");
                    }
                }
            }
        }
    }


    public static function handle(Server $server, BaseRequest $request, SwooleHttpResponse $response, BaseGlobalData $globalData){
        $uri = $request->getUri();
        logs(__FILE__, __LINE__, $uri);
        switch ($uri[1]){
            case 'getSpeed':
                //logs(__FILE__, __LINE__, $globalData->getLoggerSpeed());
                $response->json([
                    'speed' => $globalData->getLoggerSpeed()
                ]);
                break;
        }
        $response->send();
    }

    public static function exception(Throwable $exception, $action='', $requestData=[], SwooleHttpResponse $response=null)
    {
        GlobalData::getInstance()->connectDB();
       // logs(__FILE__, __LINE__, 1);
        $lb = new \App\Library\Scelet\ModelBase\Logger\LoggerBug(DEMON_NAME, $action, $requestData, $exception);
        //logs(__FILE__, __LINE__, 3);
        $lb->setDBData(GlobalData::$lastRequest);
        //logs(__FILE__, __LINE__, 2);
        //logs(__FILE__, __LINE__, $lb);
        $lb->save();

        if (!is_null($response)){
            $response->error('');
            $response->send();
        }
    }


    protected function upScript(){
        logs(__FILE__, __LINE__, 'upScript1');
    }


    protected function backScript(){}

}