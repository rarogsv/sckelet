<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 30.09.2020
 * Time: 9:19
 */

namespace App\Library\Scelet\Util;


use App\Library\Firebase\FCMPushNotification;
use App\Library\Firebase\SettingNotification;
use PHPMailer\PHPMailer\PHPMailer;

class UtilBase
{


    /**
     * @param $email
     * @param $caption
     * @param $body
     * @throws \PHPMailer\PHPMailer\Exception
     */
    function sendEmail($email, $caption, $body, $senderEmail, $passEmail){
        //api.peacock.ee:5001/common/payment/
        $mail = new PHPMailer();
        $mail->SMTPDebug  = true;
        /*
        $mail->Host       = '91.217.90.53';
        $mail->SMTPSecure = 'tls';
        $mail->Port       = 587;
*/

        //gmail
        //$mail->IsSMTP();
        //$mail->Mailer = "smtp";
        //$mail->SMTPAuth   = true;
        //$mail->SMTPSecure = "tls";
        //$mail->Port       = 587;
        //$mail->Host       = "smtp.gmail.com";

        //ukrnet
        $mail->IsSMTP();
        $mail->Mailer = "smtp";
        $mail->SMTPAuth   = true;
        $mail->SMTPSecure = "ssl";
        $mail->Port       =  465;
        $mail->Host       = "smtp.ukr.net";

        $mail->Username   = $senderEmail;
        $mail->Password   = $passEmail;

        $mail->IsHTML(true);
        $mail ->CharSet = "UTF-8";
        $mail->Subject = $caption;
        $mail->addAddress($email);
        $mail->setFrom($senderEmail);
        $mail->msgHTML($body);
        $res = $mail->send();
        logs(__FILE__, __LINE__, $res);
    }

    function sendPush(string $title, string $body, string $token){
        //logs(__FILE__, __LINE__, $senT);

        /*foreach ($senT as $sen){
            $fbN = new FCMPushNotification(FIRREBASE_API_KEY);
            $aResult = $fbN->sendToDevice($sen, $aPayload, $aOptions);
            logs(__FILE__, __LINE__, $aResult);
        }*/
        //logs(__FILE__, __LINE__, 1);
        $fbN = new FCMPushNotification(FIREBASE_API_KEY);
        try {
            $aResult = $fbN->sendToDevice($token, SettingNotification::Create()->setTimeToLive(60)->setNotification($title, $body)->setNotificationSound());
        } catch (\Exception $e) {
        }
        logs(__FILE__, __LINE__, $aResult);
    }

    function sendWeb(string $title, string $body, string $token, $icon=null, $action=null){
        //$fbN->setTitle($data)
        //    ->setBody($data)/*
        //            ->setUrlIcon('https://deltaunion.itneris.com/src/images/icon_l.png')
        //            ->setUrlAction('https://deltaunion.itneris.com/src/main.php?req=operator')*/
        //    ->send();

        $fbN = new FCMPushNotification(FIREBASE_API_KEY);
        try {
            $setting = SettingNotification::Create()->setTimeToLive(60)->setNotification($title, $body);
            if (!is_null($icon)){
                $setting->setNotificationIcon($icon);
            }
            if (!is_null($action )){
                $setting->setNotificationClickAction($action);
            }
            return $fbN->sendToDevice($token, $setting);
        } catch (\Exception $e) {
            logs(__FILE__, __LINE__, $e->getMessage());
            return null;
        }
    }
/*
    function getRandomString($length = 8) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $string = '';

        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }

        return $string;
    }

    function generatePassword($length = 20){
        $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        $str = '';
        $max = strlen($chars) - 1;

        for ($i=0; $i < $length; $i++){
            $str .= $chars[random_int(0, $max)];
        }

        return $str;
    }
*/

}