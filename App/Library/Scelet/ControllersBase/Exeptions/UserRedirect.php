<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 28.12.2020
 * Time: 17:08
 */
namespace App\Library\Scelet\ControllersBase\Exeptions;

use Exception;


class UserRedirect extends Exception
{
    const REDIRECT_LOGIN = 0;
    const REDIRECT_PAGE = 1;


    public function __construct($code = 0)
    {
        logs(__FILE__, __LINE__, 0);
        parent::__construct($message = "", $code, null);
    }


}