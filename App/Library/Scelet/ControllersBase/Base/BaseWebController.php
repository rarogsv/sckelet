<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 12.05.2020
 * Time: 16:03
 */

namespace App\Library\Scelet\ControllersBase\Base;


use App\Library\Maps\Here\Autocomplete;
use App\Library\Maps\Here\Geocoder;
use App\Library\Scelet\ControllersBase\Injection;
use App\Library\Scelet\DataBase\PDORequest;
use App\Library\Scelet\Query\BaseResponse;
use App\Library\WebAdmin\Constructor\Activity;
use App\Library\WebAdmin\Constructor\Button;
use App\Library\WebAdmin\Constructor\Field\BaseInput;
use App\Library\WebAdmin\Constructor\Field\Field;
use App\Library\WebAdmin\Constructor\Field\Select;
use App\Library\WebAdmin\Constructor\FormCreator;
use App\Library\WebAdmin\Constructor\Icons;
use App\Library\WebAdmin\Constructor\Window\Editor;
use App\Library\WebAdmin\Constructor\Window\Table;
use App\Library\WebAdmin\Constructor\Window\TableContent;
use App\Library\WebAdmin\Constructor\Window\TableHeader;
use App\Model\GlobalData;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;
use Twig\TemplateWrapper;



class BaseWebController extends BaseUserController
{

    const TEMPLATE_ADMIN_BASE = DIR_SKELETON.'App/Library/WebAdmin/Template/v1/';//dir to admin web template
    /**
     * [n=>оглавление, 
     * c=>column table, 
     * s=>-1 0 1(sort> -1=ASC, 1=DESC), 
     * l-search how substring, 
     * t=>тип] add format/base_url/btn_icon/btn_icon_color
     * call_column-params column, call-params row,
     *f-format
     */
    const NAME          = 'name';
    const COLUMN        = 'column';
    const SORT          = 'sort';
    const SEARCH        = 'search';
    const CONTENT_TYPE  = 'cont_type';
    const DATE_FORMAT   = 'date_format';//формат даты
    const TIME_ZONE     = 'time_zone';//дата с учетом зоны
    const PRELOAD       = 'preload'; // ставится первой в масиве. вызывается один раз
    const CALL_COLUMN   = 'call_column';
    const CALL_COLUMNS  = 'call_columns';
    const ACTION_BTN    = 'action_btn';
    const COLSPAN       = 'colspan';
    const VERTICAL      = 'vertical';

    const COLOR_ROW     = 'color_row';
    const COLOR_CELL    = 'color_cell';

    const ACTIONS       = 'actions';    // C create U udate D delete T table Q quick E edittable
    const SUBTABLES     = 'subtables';  // массив таблиц с масивом не совпадающих [DBMap::PAY_CLIENT => [],DBMap::CASH_FLOW => [], DBMap::PAYMENT_CLIENT => ['from_id'=>0, 'from_table'=>DBMap::CLIENT],//колонки к-х нету в одной из таблиц надо их заполнять пустыми]
    const ACT_SUBTABLES = 'act_subtables';  //[icon(str), color(str/null), table, single(0/1), sortBy(column), orderBy(ASC/DESC)]
    const QUICK         = 'quick';      //[icon, color, server_action]
    const SUBEDIT       = 'subedit';    //[icon, color, table]


    public $urlLogin='';
    public $urlPage='';

    public $controller = '';
    public $callback = [];
    public $timeZone = 0;
    public $dataTable = [];
    public $addData = [];

    /**
     * @return array
     */
    public function getAddData($key): array
    {
        if (isset($this->addData[$key])){
            return $this->addData[$key];
        } else {
            return [];
        }
    }

    public function __construct(Injection $injection, BaseUserProtection $userProtection, string $table, string $urlPage, array $dataTable, array $callFnc=[])
    {
        //logs(__FILE__, __LINE__, 3);
        parent::__construct($injection, $userProtection, $table, $callFnc);
        $this->urlLogin = $this->controller.'login/';
        $this->dataTable = $dataTable;
        $this->urlPage = $urlPage;
        $this->timeZone = $this->getCastFieldGet('tz', INT, 0);
    }

    protected function decodeUserData($token)
    {
        try {
            return $this->accessGenerator->decode($token, KEY_ITNERIS_SENDER);
        } catch (\Exception $e) {
            return null;
        }
    }

    protected function logined(){
        $this->setCookie();
        $this->redirToMainPage();
        $this->getResponse()->json();
    }

    function redirToLogin(){
        parent::redirToLogin();
        logs(__FILE__, __LINE__, 'redirToLogin-'.$this->urlLogin);
        logs(__FILE__, __LINE__, $this->getRequest()->isGet());
        logs(__FILE__, __LINE__, $this->getRequestData()->server['request_method']);
        if ($this->getRequest()->isGet()){
            //logs(__FILE__, __LINE__, 1);
            $this->getResponse()->redirect($this->urlLogin);
        } else {
            //logs(__FILE__, __LINE__, 2);
            $this->getResponse()->setFrontCallback(BaseResponse::FRONT_CALL_REDIRECT,$this->urlLogin);
        }
    }



    function redirToMainPage(){
        //logs(__FILE__, __LINE__, $this->urlPage);
        parent::redirToMainPage();
        if ($this->getRequest()->isGet()){
            //logs(__FILE__, __LINE__, 1);
            $this->getResponse()->redirect($this->urlPage);
        } else {
            //logs(__FILE__, __LINE__, 2);
            $this->getResponse()->setFrontCallback( BaseResponse::FRONT_CALL_REDIRECT,$this->urlPage);
        }
    }


    protected function createActivity($pageTitle) : Activity
    {
        $activity = new Activity();
        $activity->user['name'] = $pageTitle;
        $activity->setApi($this->controller);


        //logs(__FILE__, __LINE__, $sh);

        $activity->setWords($this->getTextShortcuts(['wait', 'notification_save', 'bad_type_file', 'small_file', 'big_file',
            'exit', 'ok', 'cancel', 'complete', 'select_service', 'bad_size_file', 'yes', 'no', 'exit_text', 'nothing_selected']));
        //logs(__FILE__, __LINE__, $activity->words_string);
        return $activity;
    }

    function logout()
    {
        logs(__FILE__, __LINE__, 'logout');
        $this->clearCookie();
        $this->redirToLogin();
    }

    function getShort($string, $length=20){
        if (strlen($string) >= $length) {
            $s = mb_substr($string, 0, $length,'UTF-8'). '...';
            //str_replace('�', '', $s);
            //logs(__FILE__, __LINE__, $s);
            return $s;
        }
        return $string;
    }

    protected function createTable($tableReq){
        //$selectMenu = $tableReq;
        $tableName = $this->getTextShortcut($tableReq);
        if(!is_null($this->getRequestGet('tp')))
        {
            //logs(__FILE__, __LINE__, $this->getRequestData()->get['tp']);
            $selectMenu = $this->getRequestGet('tp');
            $tableName =  $this->getTextShortcut($selectMenu);
            if (!is_null($this->getRequestGet('id'))){
                $objParent = $this->db->where('id', $this->getCastFieldGet('id', INT, 0))->getOne($selectMenu);
                if (!is_null($objParent)){
                    if (isset($objParent['name'])){
                        $tableName .= ' (' . $objParent['name'] .')';
                    } else {
                        $tableName .= ' (' . $objParent['id'] .')';
                    }
                }
            }

            $tableName .=  ' > '.$this->getTextShortcut($tableReq);
        }


        return $this->_createTable(
            $tableName,
            $tableReq
        );
    }



    protected function createSelect(array $data) : Select
    {
        $select = Select::Create();
        foreach ($data as $d){
            $select->addOption($d[1], $d[0]);
        }
        return $select;
    }



    protected function _createTable(string $titleTable, string $tableReq) : Table
    {
        if (isset($this->user->lang)){
            $lang =  $this->user->lang;
        } else {
            $lang = DEFAULT_LANGUAGE;
        }

        $fields = $this->dataTable[$tableReq];
        $words = GlobalData::getInstance()->getTextShortcuts(array_merge(array_column($fields, self::NAME), ['display', 'search']), $lang);
        //logs(__FILE__, __LINE__, array_column($fields, self::NAME));
        //logs(__FILE__, __LINE__, $tableReq);

        $table = Table::Create()
            ->setWords($words)
            ->setTitle($titleTable);

        $tableReq     = $this->getCastFieldGet('table', STRING, '');
        if (strlen($tableReq) == 0 || !in_array($tableReq, array_keys($this->dataTable))){
            //logs(__FILE__, __LINE__, 'return');
            return $table;
        }

        //['tpColArr'=>'payments']
        //p=orders&offset=1&count=25&search=&sortBy=start_time&orderBy=DESC&tp=worker&id=12
        //p=payment_client&offset=1&count=25&search=&sortBy=created&orderBy=DESC&tp=orders&tpColArr=payments&id=16
        $table->base_url = $this->getUriTableBase($tableReq);

        foreach ($fields as $ind => $field) {
            if (isset($field[self::NAME])){
                //logs(__FILE__, __LINE__, $field[self::NAME].'-'.$this->getTextShortcut($field[self::NAME]));
                //logs(__FILE__, __LINE__, $field);
                $th = TableHeader::Create()->setShortcut($field[self::NAME]);
                if (isset($field[self::SORT])){
                    $th->setSort($field[self::SORT]);
                }

                if (isset($field[self::COLUMN])){
                    $th->setColumn($field[self::COLUMN]);
                }

                if (isset($field[self::COLSPAN])){
                    $th->setColspan($field[self::COLSPAN]);
                }

                if (isset($field[self::COLSPAN])){
                    $th->setColspan($field[self::COLSPAN]);
                }

                if (isset($field[self::VERTICAL])){
                    $th->setVertical(1);
                }

                $table->addHeader($th);
            } else if (isset($field[self::ACTION_BTN])){
                $table->setActionTheadName($this->getTextShortcut('action'));
            } else if (isset($field[self::ACTIONS])){
                //колонка Действие
                if (
                    strpos($field[self::ACTIONS], 'U')      !== false //update
                    || strpos($field[self::ACTIONS], 'D')   !== false //delete
                    || strpos($field[self::ACTIONS], 'Q')   !== false //quick
                    || strpos($field[self::ACTIONS], 'T')   !== false //subtable
                ) {
                    $table->setActionTheadName($this->getTextShortcut('action'));
                }

                if (strpos($field[self::ACTIONS], 'C') !== false ){ //create
                    //Кнопка Создать
                    $table->addBtnHeader(
                        Button::Create(Button::ACTION_EDIT, $this->getTextShortcut('create'), Icons::CREATE)->isHeader()->setTable($tableReq)
                    );
                }

            }
        }

        //logs(__FILE__, __LINE__, $table);
        return $table;
    }

    /*protected function createEditor($table) : Editor
    {
        $editor = new Editor();
        $editor->setTitle($this->getTextShortcut('edit'))
            ->addBtnAction(Button::Create()->setText($this->getTextShortcut('save'))->setAction(Button::ACTION_SAVE))
            ->setId($this->getCastFieldPost('id', INT, 0))
            ->setTable($table);
        return $editor;
    }*/

    protected function renderPage(array $data, string $templ, string $dir){
        $template = $this->loadTemplate($templ, $dir);
        $render = $template->render($data);
        $this->getResponse()->html($render);
    }

    protected function renderContentEditor(Editor $editor){
        $this->renderContent('editor', $editor->toArray());
    }

    protected function renderContent(string $template, array $content, $templateDIR = self::TEMPLATE_ADMIN_BASE){
        $template = $this->loadTemplate($template, $templateDIR);
        $render = $template->render($content);
        //logs(__FILE__, __LINE__, $render);
        $this->getResponse()->html($render);
    }
    


    protected function loadTemplate(string $templateFile, $templateDIR = null) : TemplateWrapper
    {
        if (is_null($templateDIR)){
            $twig = new Environment(new FilesystemLoader(self::TEMPLATE_ADMIN_BASE));
        } else {
            $twig = new Environment(new FilesystemLoader($templateDIR));
        }
        return $twig->load($templateFile.'.html');
    }

    //  int count, int offset, arr columns, string $table
    //  db, range, sort[key, val], parent[table, id, single], int $idParent
    protected function _getContentTable(array $sett)
    {
        //['offset'=>1, 'count'=>25, 'sortBy'=>'', 'orderBy'=>'', 'search'=>'']

        /**
         * выборка с несколь
         * SELECT * FROM
        (
        SELECT user_id, credit_value, NULL AS debit_value, created_date FROM muvi_credit_points
        UNION ALL
        SELECT user_id, NULL AS credit_value, debit_value, created_date FROM muvi_debit_points
        ) t
        WHERE user_id = 111
        ORDER BY created_date DESC
        LIMIT 0, 20
         */
        //$this->response = Logs::getInstance()->getLog($this->request['app_id']);
         logs(__FILE__, __LINE__, $this->getRequestData()->post);
         logs(__FILE__, __LINE__, $sett);

         //logs(__FILE__, __LINE__, 1);
        $dbName=DB_NAME_DEFAULT;
        if (isset($sett['db'])){
            $dbName=$sett['db'];
        }

        if (!isset($this->dataTable[$sett['header']])){
            return [];
        }

        $dataTable  = $this->dataTable[$sett['header']]; //ключ масива настр

        $columnsLoad = array_column($dataTable, self::COLUMN);
        if (!in_array('id', $columnsLoad)){
            $columnsLoad[]='id';
        }

        //logs(__FILE__, __LINE__, $columnsLoad);
        //просмотреть строку зависимую записанной в ячейке другой строки

        $tableParent = addslashes($this->getCastFieldPost('tp', STRING, ''));

        if (isset($this->getRequestData()->post['tp_id'])){
            $parentID  = $this->getCastFieldPost('tp_id', INT, 0);
        } else {
            $parentID  = $this->getCastFieldPost('id', INT, 0);
        }

        //parent[table, id]
        //logs(__FILE__, __LINE__, $tableParent);
        //logs(__FILE__, __LINE__, $sett['parent']);
        if (isset($sett['parent'])){
            $parentID = $sett['parent']['id'];
            $tableParent = $sett['parent']['table'];
        }
        //logs(__FILE__, __LINE__, $tableParent);

        $isSingle  = $this->getCastFieldPost('single', INT, 0);
        //p=payment_client&offset=1&count=25&search=&sortBy=created&orderBy=DESC&tp=orders&id=16
        if (strlen($tableParent) > 0 && $parentID > 0){
            //tpColArr - название поля масива ид
            if (isset($this->getRequestData()->post['tpColArr'])){
                //специально создается новый запрос к базе
                $objParent = PDORequest::Create()->connection($dbName)->where('id', $parentID)->getOne($tableParent);
                //logs(__FILE__, __LINE__, $objParent);
                if (is_null($objParent)) {
                    logs(__FILE__, __LINE__, 'return');
                    return [];
                }
                $columnArr = $this->getCastFieldPost('tpColArr', STRING, '');

                //logs(__FILE__, __LINE__, $columnArr);
                //logs(__FILE__, __LINE__, $objParent[$columnArr]);
                if (strlen($columnArr) > 0 && isset($objParent[$columnArr])){
                    $arr = json_decode($objParent[$columnArr]); //масив ид
                    if (count($arr) == 0){
                        logs(__FILE__, __LINE__, 'return');
                        return [];
                    }
                    $this->db->where('id', $arr, 'IN');
                } else {
                    logs(__FILE__, __LINE__, 'return');
                    return [];
                }
            } else {

                //logs(__FILE__, __LINE__, 2);
                if ($isSingle){
                    $objParent = PDORequest::Create()->connection($dbName)->where('id', $parentID)->getOne($tableParent);
                    //logs(__FILE__, __LINE__, $objParent);
                    if (is_null($objParent)) {
                        logs(__FILE__, __LINE__, 'return');
                        return [];
                    }

                    $keySingleObj = $sett['table'].'_id';
                    //logs(__FILE__, __LINE__, $objParent);
                    //logs(__FILE__, __LINE__, $keySingleObj);
                    if (isset($objParent[$keySingleObj]) && $objParent[$keySingleObj] > 0){
                        return $this->db->connection($dbName)->where('id', $objParent[$keySingleObj] )->get($sett['table'], $columnsLoad);
                    } else {
                        logs(__FILE__, __LINE__, 'return');
                        return [];
                    }
                } else {
                    $isSingle  = $this->getCastFieldPost('single', INT, 0);
                    $divided = $this->getCastFieldPost('divided', STRING, null);
                    if (is_null($divided)){
                        $this->db->where($tableParent.'_id', $parentID);
                    } else {
                        $this->db->where($divided.'_id', $parentID)->where($divided.'_table', $tableParent);
                    }
                }
            }
        } else {
            logs(__FILE__, __LINE__, $isSingle);
            if ($isSingle){
                $id = $this->getCastFieldPost('id', INT, 0);
                logs(__FILE__, __LINE__, $id);
                if($id > 0){
                    $this->db->where('id', $id);
                }
            }
        }

        //logs(__FILE__, __LINE__, 3);
        //logs(__FILE__, __LINE__, $tableParent);
        //logs(__FILE__, __LINE__, $idParent);
        $search = addslashes($this->getCastFieldPost('search', STRING, ''));

        //logs(__FILE__, __LINE__, $dataTable);
        if (strlen($search) > 2) {
            //logs(__FILE__, __LINE__, $columns);
            $columnSearch = [];
            foreach ($dataTable as $column){
                if (isset($column[self::COLUMN]) && $column[self::SEARCH]==1){
                    $columnSearch[] = $column;
                    //$column = $column[self::COLUMN];
                }
            }
            if (count($columnSearch) > 0){
                $this->db->where($columnSearch[0][self::COLUMN], '%'.$search.'%', 'LIKE');
                unset($columnSearch[0]);
                foreach ($columnSearch as $ind => $column){
                    //logs(__FILE__, __LINE__, $column);
                    $this->db->orWhere($column[self::COLUMN], '%'.$search.'%', 'LIKE');
                }
            }
        }

        //logs(__FILE__, __LINE__, $columnsLoad);
        //logs(__FILE__, __LINE__, '$sortBy-'.$sortBy);
        //logs(__FILE__, __LINE__, array_search($sortBy, $columnsLoad));
        //logs(__FILE__, __LINE__, '$sort-'.$sort);
        $sortBy = addslashes($this->getCastFieldPost('sortBy', STRING, ''));//"sortBy":"start_time",
        $orderBy = addslashes($this->getCastFieldPost('orderBy', STRING, ''));//orderBy":"DESC", ASC

        if (strlen($sortBy) > 0 && ($orderBy == 'DESC' || $orderBy == 'ASC')){
            $this->db->orderBy($sortBy, $orderBy);
        }

        //rangeFrom rangeTo
        $rangeFrom   = $this->getCastFieldPost('rangeFrom', INT, 0);
        $rangeTo   = $this->getCastFieldPost('rangeTo', INT, 0);
        //logs(__FILE__, __LINE__, $this->dataTable[$sett['header']]);
        if ($rangeFrom > 0 && $rangeTo>0){
            $range   = [$this->getTSWhithTZ($rangeFrom), $this->getTSWhithTZ($rangeTo)];
            //logs(__FILE__, __LINE__, $range);
            $fieldSort = '';
            //logs(__FILE__, __LINE__, $dataTable);
            //self::CONTENT_TYPE=>TableContent::DATE
            $hasSort = false;
            foreach ($dataTable as $item){
                if ($hasSort){continue;}
                if (isset($item[self::COLUMN])){
                    //logs(__FILE__, __LINE__, [$item[self::COLUMN], isset($item[self::CONTENT_TYPE]), $item[self::CONTENT_TYPE] == TableContent::DATE]);
                    if (isset($item[self::CONTENT_TYPE]) && $item[self::CONTENT_TYPE] == TableContent::DATE){
                        $fieldSort = $item[self::COLUMN];
                        if ($fieldSort == $sortBy){
                            $hasSort = true;
                        }
                    }
                }
            }

            //logs(__FILE__, __LINE__, $fieldSort);
            //logs(__FILE__, __LINE__, $range);
            if (strlen($fieldSort) > 0){
                $range[1] += 86400;
                $this->db->where($fieldSort, $range, 'BETWEEN');
            }
        }

        //$rows = $this->db->get($table, [$offset,$count], $columnsLoad);
        /**
        Чтобы вернуть результаты с 6 по 15, нужно использовать такой синтаксис:
        SELECT * FROM users ORDER BY id DESC LIMIT 5(пропустить), 10(количество строк)
         */

        $offset = $this->getCastFieldPost('offset', INT, 25);
        $count = $this->getCastFieldPost('count', INT, 1);

        //logs(__FILE__, __LINE__, [$from, $count]);
        $from = ($offset-1)*$count;
        /*
          SELECT * FROM (SELECT created,money,from_id,from_table,to_id,to_table,who_id,who_table,company_id,id,'payment_client' AS tbl FROM payment_client
        UNION ALL SELECT created,money,from_id,from_table,to_id,to_table,who_id,who_table,company_id,id,'cash_flow' AS tbl FROM cash_back
        UNION ALL SELECT created,money,from_table,to_id,to_table,who_id,who_table,company_id,id,'from_id' AS 0,'cash_move' AS tbl FROM cash_move) money_move ORDER BY created DESC LIMIT 0,25
         * */

        /**
         * $exQuery = "SELECT * FROM(".
        "SELECT id,create$exQuery = "SELECT id,created,company_id,who_id,who_table,status,money,comment,from_id,from_table,to_id FROM(".
        "SELECT id,created,company_id,who_id,who_table,status,money,comment,from_id,from_table,to_id,to_table, 'move' AS tbl FROM ".DBMap::CASH_MOVE." UNION ALL ".
        "SELECT id,created,company_id,who_id,who_table,status,money,comment,from_id,from_table,to_id,to_table, 'span' AS tbl FROM ".DBMap::CASH_BACK.")".
        " money_move WHERE company_id=".$this->user->company_id." ORDER BY created DESC LIMIT 0, 20";
         */
        //если множество таблиц то колонки грузим в них
        $subTablesArr = array_column($dataTable, self::SUBTABLES);
        //logs(__FILE__, __LINE__, $dataTable);
        logs(__FILE__, __LINE__, $subTablesArr);

        if (count($subTablesArr) > 0){
            //logs(__FILE__, __LINE__, '1');
            $subTables = $subTablesArr[0];
            foreach ($subTables as $table=>$missingColumns){
                //logs(__FILE__, __LINE__, [$table, $missingColumns]);
                $this->db->setMultitable($columnsLoad, $table, $missingColumns);
            }
            $rows = $this->db->connection($dbName)->get($sett['table'], '*', [$from, $count]);
        } else {
            //logs(__FILE__, __LINE__, '2');
            $rows = $this->db->connection($dbName)->get($sett['table'], $columnsLoad, [$from, $count]);
        }
        //logs(__FILE__, __LINE__, 3);
        //$rows = $this->db->get($table, ($offset*$count), $columnsLoad);
        //logs(__FILE__, __LINE__, GlobalData::$lastRequest);
        if (count($rows) > $count){
            $rows = array_slice($rows, $count*($offset-1));
        }

        return $rows;
    }

    protected function setContent(TableContent &$tableContent, array $rows, string $table){
        //logs(__FILE__, __LINE__, 'setContent');
        //logs(__FILE__, __LINE__, $table);
        if (!isset($this->dataTable[$table])){
            return;
        }
        $columns    = $this->dataTable[$table];
        if (isset($columns[0][self::PRELOAD])){
            $this->{$columns[0][self::PRELOAD]}($rows);
        }

        foreach ($rows as $row){
            $addRow = [];
            $color = '';
            $actionButton = [];

            //перебираем по кругу все настройки
            foreach ($columns as $column){
                if (isset($column[self::NAME])){
                    switch ($column[self::CONTENT_TYPE]){
                        case TableContent::AS_IT_IS:
                            //logs(__FILE__, __LINE__, $column);
                            //logs(__FILE__, __LINE__, $row);
                            $addRow[] = $row[$column[self::COLUMN]];
                            break;
                        case TableContent::AS_IT_SHORT:
                            //logs(__FILE__, __LINE__, $column);
                            //logs(__FILE__, __LINE__, $row);
                            $addRow[] = $this->getTextShortcut($row[$column[self::COLUMN]]);
                            break;
                        case TableContent::BOOLEAN:
                            $addRow[] = $row[$column[self::COLUMN]]==0?$this->getTextShortcut('no'):$this->getTextShortcut('yes');
                            break;
                        case TableContent::DATE:
                            //logs(__FILE__, __LINE__, $column[self::DATE_FORMAT]);
                            if ($row[$column[self::COLUMN]] > 0){
                                if (isset($column[self::TIME_ZONE])){
                                    if ($column[self::TIME_ZONE] == 'user'){
                                        $tz = $this->timeZone;
                                    } else {
                                        $tz = $row[$column[self::TIME_ZONE]];
                                    }
                                    $addRow[] = date($column[self::DATE_FORMAT], $this->getTSWhithTZ($row[$column[self::COLUMN]], $tz));
                                } else {
                                    $addRow[] = date($column[self::DATE_FORMAT], $row[$column[self::COLUMN]]);
                                }
                            } else {
                                $addRow[] = '-';
                            }
                            break;
                        case TableContent::MONEY:
                            //$addRow[] = number_format($row[$column[self::COLUMN]]/100, 2, '.', '');
                            $addRow[] = getMoneyFormat($row[$column[self::COLUMN]]);
                            break;
                        case TableContent::EMAIL:
                            $addRow[] = $this->getEmailCell($row[$column[self::COLUMN]]);
                            break;
                        case TableContent::TEL:
                            $addRow[] = $this->getPhoneCell($row[$column[self::COLUMN]]);
                            break;
                        case TableContent::RATING:
                            //logs(__FILE__, __LINE__, $row);
                            //logs(__FILE__, __LINE__, $column[self::COLUMN]);
                            //logs(__FILE__, __LINE__, $row[$column[self::COLUMN]]);
                            //$addRow[] = number_format($row[$column[self::COLUMN]]/100, 2, '.', '');
                            $addRow[] = getRateFormat($row[$column[self::COLUMN]]);
                            break;
                        case TableContent::LINK:
                            //base_url, btn_icon, btn_icon_color
                            //$src = $this->{$column[self::CALL_COLUMN]}($row[$column[self::COLUMN]]);
                            //$addRow[] = $this->getLinkCell($column, $src);
                            break;

                        case TableContent::IMG:
                            //base_url, btn_icon, btn_icon_color
                            //$src = $column['base_url'].$row[$column[self::COLUMN]];

                            $src = $this->{$column[self::CALL_COLUMN]}($row[$column[self::COLUMN]]);
                            $addRow[] = '<a href="'.$src.'" target="_blank"><img src="'.$src.'" style="width: 70px;"></a>';
                            break;

                        case TableContent::COUNT:
                            $addRow[] = count(json_decode($row[$column[self::COLUMN]], true));
                            break;

                        case TableContent::CALLBACK:
                            //logs(__FILE__, __LINE__, $this->{$column[self::CALL_COLUMNS]});

                            if (isset($column[self::CALL_COLUMNS])){
                                $addRow[] = $this->{$column[self::CALL_COLUMNS]}($row);
                            } else if (isset($column[self::CALL_COLUMN])) {
                                $addRow[] = $this->{$column[self::CALL_COLUMN]}($row[$column[self::COLUMN]]);
                            }
                            break;

                    }
                } else if (isset($column[self::COLOR_ROW])){
                    $color = $this->{$column[self::COLOR_ROW]}($row);
                } else if (isset($column[self::ACTION_BTN])){
                    $actionButton = $this->{$column[self::ACTION_BTN]}($row);
                }
            }
            //logs(__FILE__, __LINE__, $row);
            //logs(__FILE__, __LINE__, isset($columns[self::ACTION_BTN]));

            $tableContent->addRow($addRow, $row['id'], $color, $actionButton);
        }

        $lastColumn = end($columns);
        //logs(__FILE__, __LINE__, $columns);
        //logs(__FILE__, __LINE__, $lastColumn[self::ACTIONS]);

        if (isset($lastColumn[self::ACTIONS])){
            $actionBtn  =  $lastColumn[self::ACTIONS];
            if (strpos($actionBtn, 'U') !== false){
                //logs(__FILE__, __LINE__, 'update btn');
                $tableContent->addBtnAction(
                    Button::Create(Button::ACTION_EDIT, $this->getTextShortcut('edit'), Icons::EDIT)->setTable($table)
                );
            }

            if (strpos($actionBtn, 'D') !== false){
                $tableContent->addBtnAction(
                    Button::Create(Button::ACTION_DELETE, $this->getTextShortcut('delete'), Icons::DELETE)->setMsg($this->getTextShortcut('msg_del'))
                );
            }

            if (isset($lastColumn[self::ACT_SUBTABLES])){
                $subTables = $lastColumn[self::ACT_SUBTABLES];
                //logs(__FILE__, __LINE__, $table);
                //logs(__FILE__, __LINE__, $subTables);
                //$this->getUriSubTable(DBMap::WORKER, $table)
                //[icon(str), color(str/null), table, single(0/1), sortBy(column), orderBy(ASC/DESC)]
                foreach ($subTables as $subTable){
                    //$count = count($subTable);
                    $subTable['urlParams']['tp'] = $table;//Добавление родительскую таблицу
                    $btn = Button::Create(Button::ACTION_TABLE, $this->getTextShortcut($subTable['btn']['name']), $subTable['btn']['icon'], $subTable['btn']['color'])
                        ->setHref(
                        $this->getUrlTable($subTable['toTable'], $subTable['urlParams'])
                    );
                    //p=orders&offset=1&count=25&sortBy=start_time&orderBy=DESC&search=&tp=client&single=0&rangeFrom=0&rangeTo=0&tp_id=162
                   /* if ($count == 3){
                        $btn->setHref(
                            $this->getUrlTable($subTable[2], ['tp'=>$table])
                        );
                        //logs(__FILE__, __LINE__, $subTable);
                    } else if ($count == 4){
                        $btn->setHref(
                                    $this->getUrlTable($subTable[2], ['tp'=>$table, 'single'=>$subTable[3]])
                                    //$this->getUriSubTable($subTable[2], $table,$subTable[3])
                                );
                        //logs(__FILE__, __LINE__, $subTable);
                    } else {
                        $btn->setHref(
                                    $this->getUrlTable($subTable[2], ['tp'=>$table, 'single'=>$subTable[3], 'sortBy'=>$subTable[4], 'orderBy'=>$subTable[5]])
                                    //$this->getUriSubTable($subTable[2], $table, $subTable[3], $subTable[4], $subTable[5])
                                );
                        //$this->getUriSubTable(DBMap::PAYMENT_CLIENT, DBMap::ORDER, 0, 'created', 'DESC', ['tpColArr'=>'payments'])
                        //$this->getUriSubTable(1, 2, 3, 4, 5)
                        //logs(__FILE__, __LINE__, $subTable);
                    }*/

                    $tableContent->addBtnAction($btn);
                }
            }

            if (isset($lastColumn[self::SUBEDIT])){
                $subEdits = $lastColumn[self::SUBEDIT];
                //[icon, color, table]
                foreach ($subEdits as $subEdit){
                    $tableContent->addBtnAction(
                        Button::Create(Button::ACTION_EDIT,
                            $this->getTextShortcut($subEdit['btn']['name']),
                            $subEdit['btn']['icon'], //icon
                            $subEdit['btn']['color'])  //color
                            ->setTable($subEdit['btn']['table'])
                    );
                }
            }

            if (isset($lastColumn[self::QUICK])){
                $quick = $lastColumn[self::QUICK];
                foreach ($subTables as $subEdit){
                    //[icon, color, server_action]
                    $tableContent->addBtnAction(
                        Button::Create(
                            Button::ACTION_QUICK,
                            $this->getTextShortcut($subEdit['btn']['name']),
                            $subEdit['btn']['icon'], //icon
                            $subEdit['btn']['color']  //color
                        )->setServerAction($subEdit['btn']['action'])
                    );
                }
            }
        }
    }

    protected function getTSWhithTZ($ts, $tz=null){
        if ($tz == null){
            $tz = $this->timeZone;
        }
        //logs(__FILE__, __LINE__, 'getTSWhithTZ');
        //logs(__FILE__, __LINE__, date('H:i', $ts));
        //logs(__FILE__, __LINE__, date('H:i', $ts - $tz));
        //logs(__FILE__, __LINE__, $tz);
        return $ts - $tz;
    }


    protected function getLinkCell($name, $url){
        return '<a href="'.$url.'"  target="_blank" style="font-size: 13px;"><i class="fa fa-external-link">&nbsp;'.$name.'</i></a>';
    }

    //??????
    protected function getBtnCell($column, $id){
        return '<a href="'.$column['base_url'].base64_encode($id).'" target="_blank" style="font-size: 13px;"><i class="fa " style="color:#000000;"></i></a>';
    }

    protected function getPhoneCell($cell){
        if (strlen($cell) > 5){
            if (strpos($cell, '+') !== 0){
                $cell = '+'.$cell;
            }
            return $cell
                .'<br>'
                .'<a href="tel:'.$cell.'" style="font-size: 13px;" class="btn btn-success btn-xs"><i class="fa fa-phone"></i></a>'//phone
                .'<a href="https://api.whatsapp.com/send?phone='.$cell.'" style="font-size: 13px;" class="btn btn-success btn-xs"><i class="fa fa-whatsapp"></i></a>';//whatsap
                //.'<a href="viber://chat?number='.$cell.'" style="font-size: 13px;" class="btn btn-success btn-xs"><i class="fab fa-viber"></i></a>'//viber
                //.'<a href="https://t.me/'.$cell.'" style="font-size: 13px;" class="btn btn-success btn-xs"><i class="fa-telegram"></i></a>';//telegram
        } else{
            return '';
        }
    }

    protected function getEmailCell($cell){
        if (strlen($cell) > 0){
            return '<a href="mailto:'.$cell.'" style="font-size: 13px;"><i class="fa fa-envelope-o">&nbsp;'.$cell.'</i></a>';
        } else{
            return '';
        }
    }

    
    

    /**
     * @param $table
     * @param $tableParent
     * @param int $single будет грузиться один обьект зависимый от пэрента
     * @return string
     */
    protected function getUriSubTable($table, $tableParent, $single=0, $sortBy='', $orderBy='', $add=[]) : string
    {
        //logs(__FILE__, __LINE__, 'subtable');
        $url = $this->getUriTable($table, 1, 25, $sortBy, $orderBy, '', $tableParent, 0, $add);

        if ($single){
            $url .= '&single=1';
        }

        return  $url;
    }

    protected function getUriTable($table, $numbPage=1, $count=25, $sortBy='', $orderBy='', $search='', $tp='', $tp_id=0, $add=[]) : string
    {
        //logs(__FILE__, __LINE__, [$table, $numbPage, $count, $sortBy, $orderBy, $search, $tp, $tp_id, $add]);
        $url =  $this->getUriTableBase($table).'&offset='.$numbPage.'&count='.$count.'&search='.$search.'&sortBy='.$sortBy.'&orderBy='.$orderBy;
        if (strlen($tp) > 0){
            $url .= '&tp='.$tp;
        }

        if ($tp_id > 0){
            $url .= '&tp_id='.$tp_id;
        }

        foreach ($add as $key => $val){
            $url .= '&'.$key.'='.$val;
        }

        return $url;
    }


    protected function getUrlTable($table, $params=[]) : string
    {
        //$table, $numbPage=1, $count=25, $sortBy='', $orderBy='', $search='', $tp='', $tp_id=0, $add=[]
        //logs(__FILE__, __LINE__, [$table, $numbPage, $count, $sortBy, $orderBy, $search, $tp, $tp_id, $add]);
        $url =  $this->getUriTableBase($table);
        $deff = ['offset'=>1, 'count'=>25, 'sortBy'=>'', 'orderBy'=>'', 'search'=>''];
        foreach ($deff as $key => $val){
            if (!isset($params[$key])){
                $params[$key] = $val;
            }
        }
        //$add = ['tp', 'tp_id', 'tpColArr', 'single', 'divided'];
        foreach ($params as $key=>$val){
            $url .= '&'.$key.'='.$val;
        }


        if (isset($params['rangeFrom']) && isset($params['rangeTo'])){
            //$url .= '&rangeFrom='.$params['rangeFrom'].'&rangeTo='.$params['rangeTo'];
        } else {
            if (isset($this->dataTable[$table])){
                $dataTable = $this->dataTable[$table];
                foreach ($dataTable as $item){
                    if (isset($item[self::COLUMN]) && isset($item[self::CONTENT_TYPE]) && $item[self::CONTENT_TYPE] == TableContent::DATE){
                        //logs(__FILE__, __LINE__, [$item[self::COLUMN], isset($item[self::CONTENT_TYPE]), $item[self::CONTENT_TYPE] == TableContent::DATE]);
                        $url .= '&rangeFrom=0&rangeTo=0';
                        break;
                    }
                }
            }
        }



        return $url;
    }

    protected function getUriTableBase($table){
        return $this->controller.'page/?table='.$table;
    }

    protected function getUriEditor($table = '', $id = 0) : string
    {
        $url = $this->controller.'page/?e='.$table;
        if ($id > 0){
            $url .='id='.$id;
        }
        return $url;
    }



    function getField(BaseInput $input, string $shortcut, $show=true): Field
    {
        return Field::Create($this->getTextShortcut($shortcut), $input, intval($show));
    }

    protected function getContentCopy($column){
        //logs(__FILE__, __LINE__, 'getContentCopy'.$column);
        return '<a data-action="copy" data-copy="'.base64_encode($column).'" class="btn btn-success btn-xs"><i class="'.Icons::COPY.'"></i>Copy</a>';
    }

    function _autocomplete($table, $value, $field, $fields)
    {
        //logs(__FILE__, __LINE__, $this->getRequestData()->post);
        $this->db->where($field, '%'.addslashes($value).'%', 'LIKE');
        $res = $this->db->get($table, $fields,  [0, 6]);
        $this->getResponse()->json(['arr' => $res]);
        //logs(__FILE__, __LINE__, count($rows));
        //logs(__FILE__, __LINE__, $res);
    }

    function renderEditor(FormCreator $form){
        //logs(__FILE__, __LINE__, $this->getRequestData()->post['type']);
        if ($this->getCastFieldPost('type', STRING, 'page') == 'page'){
            $this->renderContent('editor_table', $form->getEditor()->toArray());
        } else {
            $this->renderContent('editor', $form->getEditor()->toArray());
        }
    }

    protected function _autocompleteHereHandler($query){
        $action = $this->getCastFieldPost('action', STRING);
        //logs(__FILE__, __LINE__,$action);
        switch ($action){
            case 'address':
                $this->_autocompleteHere($query);
                break;
            case 'geodecode':
                $this->_geoCodeHere($query);
                break;
        }
    }
    

    protected function _autocompleteHere($query){
        //logs(__FILE__, __LINE__,'_autocompleteHere' );
        $ac = new Autocomplete(HERE_MAP_API_KEY);
        $ac->setInput($query);
        $res = $ac->send();
        $this->getResponse()->json(['places' => $res]);
    }

    protected function _geoCodeHere($idLocation){
       // logs(__FILE__, __LINE__,'_geoCodeHere' );
        $ac = new Geocoder(HERE_MAP_API_KEY);
        $ac->setLocationId($idLocation);
        $res = $ac->send();
        $this->getResponse()->json($res);
    }

}