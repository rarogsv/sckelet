<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 28.12.2020
 * Time: 16:16
 */

namespace App\Library\Scelet\ControllersBase\Base;


abstract class BaseProtection
{
    const ACCESS_FREE = 0;
    const ACCESS_COOKIE = 1;
    const ACCESS_TOKEN = 2;
    const ACCESS_LOGIN_PASS = 3;
    const ACCESS_COMBINE=4;



}