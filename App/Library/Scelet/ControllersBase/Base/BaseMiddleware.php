<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 02.08.2019
 * Time: 15:48
 */

namespace App\Library\Scelet\ControllersBase\Base;


use App\Library\Scelet\Query\Validator;
use Swoole\Http\Request;

/**
 * Class BaseMiddleware
 * @package App\Controllers
 */
class BaseMiddleware{

    protected $validator;
    public $isWrong = false;



    /**
     * BaseMiddleware constructor.
     * @param Validator $validator
     */
    public function __construct(Validator $validator)
    {
        $this->validator = $validator;
    }


    /**
     * @return Validator
     */
    public function getValidator(): Validator
    {
        return $this->validator;
    }

}