<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 28.12.2020
 * Time: 16:18
 */

namespace App\Library\Scelet\ControllersBase\Base;


use App\Library\Scelet\ControllersBase\Exeptions\BadCookie;
use App\Library\Scelet\ControllersBase\Exeptions\TokenNotFound;
use App\Library\Scelet\ControllersBase\Exeptions\UserBlock;
use App\Library\Scelet\ControllersBase\Exeptions\UserNotFound;
use App\Library\Scelet\ControllersBase\Exeptions\UserRedirect;
use App\Library\Scelet\ModelBase\BaseModelUser;



abstract class BaseUserProtection extends BaseProtection
{

    protected $user = null;
    public $payload = null;
    protected $controller;

    public function __construct(BaseUserController $userController)
    {
        $this->controller = $userController;
    }

    public function handle()
    {
        //logs(__FILE__, __LINE__, $this->controller->action);

        $this->{$this->controller->action}();
    }

    function clearCookie($d=DOMAIN, $name=null){
        logs(__FILE__, __LINE__, 'clearCookie');
        //$this->controller->getResponse()->setCookie($this->controller->tableUser, '', time()-1, $d);
        if (is_null($name)){
            $name = $this->controller->tableUser;
        }
        $this->controller->clearCookie($name, $d);
    }


    /**
     * @return bool
     * @throws \Exception
     */
    function accessByToken(): bool
    {
        $token = $this->controller->getRequest()->getToken($this->controller->tableUser);
        //logs(__FILE__, __LINE__, $payload);
        return $this->accessBy($token);
    }


    /**
     * @return void
     * @throws UserBlock
     * @throws UserNotFound
     */
    function accessByCookie()
    {
        $token = $this->controller->getRequest()->getCookie($this->controller->tableUser);
        return $this->accessBy($token);
    }

    function accessBy($token){
       logs(__FILE__, __LINE__, $token);
        if (is_null($token)){
            //return;
            throw new TokenNotFound();
        }

        //logs(__FILE__, __LINE__, 1);
        try {
            $this->payload = $this->controller->accessGenerator->decode($token);
        } catch (\Exception $e) {
            throw new TokenNotFound();
        }
        //logs(__FILE__, __LINE__, $this->payload);
        //logs(__FILE__, __LINE__, $this->tableUser);
        //logs(__FILE__, __LINE__, 2);
        if (isset($this->payload['uid']) && is_numeric($this->payload['uid'])){
            $this->user = $this->loadUser($this->payload['uid']);
        }
        //logs(__FILE__, __LINE__, $this->payload);
        //logs(__FILE__, __LINE__, $this->user);

        if ($this->user instanceof BaseModelUser){
            if ($this->user->id == 0){
                //logs(__FILE__, __LINE__, 4);
                throw new UserNotFound();
            } else {
                //logs(__FILE__, __LINE__, 5);
                if ($this->user->block){
                    //logs(__FILE__, __LINE__, 6);
                    throw new UserBlock();
                }
            }
        } else {
            //logs(__FILE__, __LINE__, 7);
            throw new UserNotFound();
        }
    }

    /**
     * @return BaseModelUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    abstract function searchUser(array $search);
    abstract function loadUser(int $id);


    /**
     * @throws \App\Library\Scelet\ControllersBase\Exeptions\UserBlock
     * @throws UserRedirect
     */
    function dontRegUser(){
        logs(__FILE__, __LINE__, 'dontRegUser');
        //logs(__FILE__, __LINE__, $this->request->getRequest()->cookie);
        try {
            $this->accessByCookie();
            logs(__FILE__, __LINE__, 0);
            throw new UserRedirect(UserRedirect::REDIRECT_PAGE);
        } catch (UserNotFound $e) {
            logs(__FILE__, __LINE__, 1);
            $this->clearCookie();
        } catch (TokenNotFound $e){
            logs(__FILE__, __LINE__, 2);
        }
    }

    /**
     * @throws \App\Library\Scelet\ControllersBase\Exeptions\UserBlock
     */
    function maybe(){
        try {
            $this->accessByCookie();
            //logs(__FILE__, __LINE__, 1);
        } catch (UserNotFound $e) {
            logs(__FILE__, __LINE__, 1);
            //logs(__FILE__, __LINE__, $this->hasAccess);
            $this->clearCookie();
        } catch (TokenNotFound $e) {
            logs(__FILE__, __LINE__, 2);
            //$this->clearCookie();
        }
    }



    /**
     * @throws \App\Library\Scelet\ControllersBase\Exeptions\UserBlock
     * @throws UserRedirect
     */
    function hasAccessUser(){
        try {
            //logs(__FILE__, __LINE__, 1);
            $this->accessByCookie();
        } catch (BadCookie $e) {
            logs(__FILE__, __LINE__, 1);
            throw new UserRedirect(UserRedirect::REDIRECT_LOGIN);
        } catch (UserNotFound $e) {
            logs(__FILE__, __LINE__, 2);
            throw new UserRedirect(UserRedirect::REDIRECT_LOGIN);
        } catch (TokenNotFound $e) {
            logs(__FILE__, __LINE__, 3);
            throw new UserRedirect(UserRedirect::REDIRECT_LOGIN);
        }
    }


}