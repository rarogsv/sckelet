<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 18.06.2022
 * Time: 21:25
 */

namespace App\Library\Scelet\ControllersBase\Base;



use App\Library\Scelet\ControllersBase\Injection;
use App\Library\Scelet\ModelBase\BaseDBMap;
use App\Library\Scelet\Util\ServerHandler;
use App\Library\WebAdmin\Constructor\Window\Table;
use App\Library\WebAdmin\Constructor\Window\TableContent;
use App\Library\WebAdmin\Constructor\Button;
use App\Library\WebAdmin\Constructor\Icons;

use App\Library\Scelet\ModelBase\Servers;
use App\Library\Scelet\HttpRequest\Request;


class BaseSAdminController extends BaseWebController
{

    const COLUMNS_BASE = [

        BaseDBMap::SERVER => [
            [self::NAME=>'id',         self::COLUMN=>'id',          self::SORT=>1, self::SEARCH=>1, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'ip',         self::COLUMN=>'ip',          self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'comment',    self::COLUMN=>'comment',     self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'enable',     self::COLUMN=>'enable',      self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::BOOLEAN],
            [self::NAME=>'RAM/ROM/CPU',self::COLUMN=>'ram',         self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMNS=>'getRRC'],
            [self::COLUMN=>'rom',      self::SEARCH=>0],
            [self::COLUMN=>'cpu',      self::SEARCH=>0],
            [self::NAME=>'demon',      self::COLUMN=>'data',        self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMN=>'getDemonData'],
            [self::NAME=>'loading',    self::COLUMN=>'data',        self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMN=>'getLoadingData'],
            [self::NAME=>'speed',      self::COLUMN=>'speed',       self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMN=>'getSpeed'],
            [self::NAME=>'git_hash',   self::COLUMN=>'git_hash',    self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'comment_git',self::COLUMN=>'comment_git', self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'comment_composer',self::COLUMN=>'comment_composer',    self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'error_update',self::COLUMN=>'data',       self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMN=>'getErrorUpdate'],
            [self::ACTION_BTN=>'getBtnServer'],
            [self::ACTIONS => 'CU']
        ],

        BaseDBMap::LOG_BUG => [
            [self::NAME=>'id',         self::COLUMN=>'id',          self::SORT=>1, self::SEARCH=>1, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'demon',      self::COLUMN=>'controller',  self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'action',     self::COLUMN=>'action',      self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'count',      self::COLUMN=>'count',       self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'message',    self::COLUMN=>'message',     self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'trace',      self::COLUMN=>'trace',       self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMN=>'getContentCopy'],
            [self::NAME=>'request',    self::COLUMN=>'request',     self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK, self::CALL_COLUMN=>'getContentCopy'],
            [self::NAME=>'db_last_query',self::COLUMN=>'db_last_query',   self::SORT=>0, self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS]
        ],

        //company history
        BaseDBMap::HISTORY => [
            [self::NAME=>'create',    self::COLUMN=>'created',      self::SORT=>1,  self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::DATE, self::DATE_FORMAT=>'d.m.Y H:i'],
            [self::NAME=>'action',    self::COLUMN=>'action',       self::SORT=>0,   self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK,    self::CALL_COLUMN=>'getHistoryAction'],
            [self::NAME=>'obj_table', self::COLUMN=>'table_obj',    self::SORT=>0,   self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK,    self::CALL_COLUMN=>'getHistoryTable'],
            [self::NAME=>'obj_id',    self::COLUMN=>'id_obj',       self::SORT=>0,   self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::AS_IT_IS],
            [self::NAME=>'data',      self::COLUMN=>'data',         self::SORT=>0,   self::SEARCH=>0, self::CONTENT_TYPE=>TableContent::CALLBACK,    self::CALL_COLUMN=>'getHistoryDataChange'],
        ],
    ];
    protected $nameDemonServerController = '';

    protected function getDemonData($data){
        $dp = json_decode($data, true);
        $res = '';
        foreach ($dp as $key => $d){
            if ($key != 'inf'){
                //logs(__FILE__, __LINE__, $key);
                $res .= $key.'-';
                $res .= intval($d)>$this->timeNow-120?'en':'dis';
                $res .= '<br>';
            }
        }
        return $res;
    }

    protected function getSpeed($column){
        //logs(__FILE__, __LINE__, $column);
        $res = '';
        $arr = json_decode($column, true);
        //{"worker":{"getActualOrders":4},"client":{"commonData":3,"getDataCompany":3,"getActualOrders":2},"company":[]}
        foreach ($arr as $name => $actions){
            $res.= $name.'-';
            if (is_array($actions)){
                arsort($actions, SORT_NUMERIC);
                foreach ($actions as $action => $value){
                    $res .= $action .':'.$value.'; ';
                }
                $res.='<br>';
            }
            //logs(__FILE__, __LINE__, $actions);
        }
        return $res;
    }

    protected function getRRC($columns){
        return $columns['ram'].'/'.$columns['rom'].'/'.$columns['cpu'];
    }


    protected function getErrorUpdate($data){
        $dp = json_decode($data, true);
        $res = '';
        //logs(__FILE__, __LINE__, $key);
        if (isset($dp['err_up_script'])){
            $res = $dp['err_up_script'];
        }
        return $res;
    }

    protected function getLoadingData($data){
        $dp = json_decode($data, true);
        $res = '';
        if (isset($dp['inf'])){
            ksort($dp['inf'], SORT_NUMERIC );
            //logs(__FILE__, __LINE__, $key);
            foreach ($dp['inf'] as $key => $d){
                //logs(__FILE__, __LINE__, $key);
                $res .= 'H-'.$key.':'.$d[0].'/'.$d[1].'/'.$d[2].'; ';
                //[$ram, $rom, $cpu]
                $res .= '<br>';
            }
        }

        return $res;
    }

    protected $DEMON_LIST = [];

    //parent::__construct($injection, new SAdminProtection($this),BaseDBMap::SADMIN,'/_sa/', $this->getUrlTable(BaseDBMap::DICTIONARY), self::COLUMNS);
    public function __construct(Injection $injection, BaseUserProtection $userProtection, string $table, string $urlPage, array $dataTable)
    {
        $dataTable = array_merge($dataTable, self::COLUMNS_BASE);
        parent::__construct($injection, $userProtection, $table, $urlPage, $dataTable);
    }

    protected function restartServers(){
        $servers = $this->db->connection(DB_NAME_SETTING)->get(BaseDBMap::SERVER);
        foreach ($servers as $server){
            $this->restartServer($server);
        }
    }

    protected function restartServer($server){
        //https://176.103.59.97/disp/rd.php
        logs(__FILE__, __LINE__, 'restartServer');
        //$sockets = [SOCKET_WORKER, SOCKET_CLIENT, SOCKET_COMPANY, SOCKET_TICKER, SOCKET_FILE_MANAGER];
        //socket demon close
        Request::Build($server['ip'].':'.SOCKET_SC.'/'.SERVER_HANDLER_KEY.'/'.ServerHandler::SERVER_RESET.'/')->setGET(['demons'=>$this->DEMON_LIST])->requestGET(4);
    }


    protected function serversHandler($action){
        $servers = $this->db->connection(DB_NAME_SETTING)->get(BaseDBMap::SERVER);
        $serverMain = indexOfObj($servers, ['ip' => MAIN_SERVER_IP]);
        //logs(__FILE__, __LINE__, $servers);
        if ($action == ServerHandler::UPDATE || $action == ServerHandler::UPDATE_BACK){

            //logs(__FILE__, __LINE__, $serverMain);

            //откоючает и обновляет
            foreach ($servers as $server){
                $this->serverHandler($server, $action);
            }

            //$this->serverHandler($serverMain, ServerHandler::UPDATE_SCRIPT);
            //обновляет единичное - базу и тд
            switch ($action){
                case ServerHandler::UPDATE:
                    //logs(__FILE__, __LINE__, 'UPDATE_SCRIPT');
                    //ServerHandler::killDemon($this->nameDemonServerController);
                    //ServerHandler::runDemon($this->nameDemonServerController);
                    $this->serverHandler($serverMain, ServerHandler::UPDATE_SCRIPT);
                    break;
                case ServerHandler::UPDATE_BACK:
                    //$this->serverHandler($serverMain, ServerHandler::UPDATE_BACK_SCRIPT);
                    $this->serverHandler($serverMain, ServerHandler::UPDATE_BACK_SCRIPT);
                    break;
            }

            //вкл все демоны
            foreach ($servers as $server){
                $this->serverHandler($server, ServerHandler::ON);
            }

            $this->server->reload();

        } else {
            switch ($action){
                case ServerHandler::UPDATE_SCRIPT:
                    //logs(__FILE__, __LINE__, 'UPDATE_SCRIPT');
                    $this->serverHandler($serverMain, ServerHandler::UPDATE_SCRIPT);
                    $this->server->reload();
                    break;
                case ServerHandler::UPDATE_BACK_SCRIPT:
                    $this->serverHandler($serverMain, ServerHandler::UPDATE_BACK_SCRIPT);
                    $this->server->reload();
                    break;
                default:
                    foreach ($servers as $server){
                        $this->serverHandler($server, $action);
                    }
                    break;
            }
        }
    }

    protected function serverHandler($server, $action){
        //https://176.103.59.97/disp/rd.php
        logs(__FILE__, __LINE__, 'serverHandler-'.$action);
        //$sockets = [SOCKET_WORKER, SOCKET_CLIENT, SOCKET_COMPANY, SOCKET_TICKER, SOCKET_FILE_MANAGER];
        //socket demon close
        logs(__FILE__, __LINE__, DEMON_LIST);
        Request::Build($server['ip'].':'.SOCKET_SC.'/'.SERVER_HANDLER_KEY.'/'.$action.'/')
            ->setGET(['demons'=>json_encode($this->DEMON_LIST)])->requestGET(3);
    }



    /*protected function updateVersion(){
        logs(__FILE__, __LINE__, 'updateVersion');
        $servers = $this->db->connection(DB_NAME_SETTING)->get(BaseDBMap::SERVER);

        foreach ($servers as $server){

            $this->updateVersionSingle($server);
        }
    }

    protected function updateVersionSingle($server){
        logs(__FILE__, __LINE__, 'updateVersionSingle');
        Request::Build($server['ip'].':'.SOCKET_SC.'/'.SERVER_HANDLER_KEY.'/update/')
            ->setGET(['demons'=>$this->DEMON_LIST])
            ->requestGET(15);
    }*/


    function quick()
    {
        logs(__FILE__, __LINE__, 'quick');
        logs(__FILE__, __LINE__, $this->getRequestData()->post);
        $data = $this->getCastDataPost([
            ['id', INT, 0],
            ['table', STRING],
            ['server_action', STRING],
        ]);

        switch ($data['table']){
            case BaseDBMap::SERVER:
                //{"id":"0","server_action":"update","table":"server"}
                //"id":"1","server_action":"restart","table":"server"}
                /*if ($data['id']>0){
                    logs(__FILE__, __LINE__, '1');
                    $obj = Servers::Load($data['id']);
                    $this->serverHandler($obj->toArray(), $data['server_action']);
                } else {
                    logs(__FILE__, __LINE__, '2');
                    //$obj = Servers::Load($data['id']);
                    $this->serversHandler($data['server_action']);
                }*/
                logs(__FILE__, __LINE__, '1');
                $this->serversHandler($data['server_action']);
                break;
        }

        $this->getResponse()->json();
    }

//back
    protected function replyVersion($hash){
        $servers = $this->db->connection(DB_NAME_SETTING)->get(BaseDBMap::SERVER);
        foreach ($servers as $server){
            Request::Build($server['ip'].':'.SOCKET_SC.'/'.SERVER_HANDLER_KEY.'/reply/')->setGET(['demons'=>$this->DEMON_LIST, 'hash'=>$hash])->requestGET(10);
        }
    }


    protected function getBtnServer(){
        $res = [];
        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::SERVER_RESET), Icons::REFRESH, Button::COLOR_YELLOW)
            ->setServerAction(ServerHandler::SERVER_RESET);
        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::OFF), Icons::REFRESH, Button::COLOR_YELLOW)
            ->setServerAction(ServerHandler::OFF);
        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::ON), Icons::REFRESH, Button::COLOR_YELLOW)
            ->setServerAction(ServerHandler::ON);

        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::UPDATE), Icons::REFRESH, Button::COLOR_YELLOW)
            ->setServerAction(ServerHandler::UPDATE);
        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::UPDATE_SCRIPT), Icons::REFRESH, Button::COLOR_BLUE_LIGHT)
            ->setServerAction(ServerHandler::UPDATE_SCRIPT);

        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::UPDATE_BACK), Icons::REPLY, Button::COLOR_RED)
            ->setServerAction(ServerHandler::UPDATE_BACK)->setTable(BaseDBMap::SERVER);
        $res[] = Button::Create(Button::ACTION_QUICK, $this->getTextShortcut(ServerHandler::UPDATE_BACK_SCRIPT), Icons::REPLY, Button::COLOR_BLUE2)
            ->setServerAction(ServerHandler::UPDATE_BACK_SCRIPT)->setTable(BaseDBMap::SERVER);



        return $res;
    }




}