<?php
namespace App\Library\Scelet\ControllersBase\Base;



use App\Library\Scelet\ControllersBase\Exeptions\UserBlock;
use App\Library\Scelet\ControllersBase\Exeptions\UserRedirect;
use App\Library\Scelet\ControllersBase\Injection;
use App\Library\Scelet\ModelBase\BaseModelDB;
use App\Library\Scelet\ModelBase\TaskTicker;
use App\Library\Scelet\Query\SwooleFileProvider;
use App\Model\Base\FileHandler;
use App\Model\GlobalData;
use App\Model\Base\Util;


/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 04.05.2020
 * Time: 23:42
 */

class BaseUserController extends ABaseUserController
{

    public      $unit;
    public      $action;
    protected   $userStatus;
    protected   $callFnc=[];
    protected   $userProtection;

    public function __construct(Injection $injection, BaseUserProtection $userProtection, string $table, array $callFnc=[])
    {
        parent::__construct($injection, $table);
        //logs(__FILE__, __LINE__, 4);
        $this->unit = new Util();
        $this->action = $this->uri[1];
        $this->userProtection = $userProtection;
        $this->callFnc = $callFnc;
    }


    function redirToLogin(){
        logs(__FILE__, __LINE__, 'redirToLogin');
        $this->clearCookie();
    }

    function redirToMainPage(){}


    //API
    public function handle()
    {
        if (isset($this->callFnc['pre'])){
            $this->{$this->callFnc['pre']}();
        }
        //logs(__FILE__, __LINE__, $this->action);
        //logs(__FILE__, __LINE__, '');
        //logs(__FILE__, __LINE__, 1);
        //logs(__FILE__, __LINE__, $this->action);
        //logs(__FILE__, __LINE__, $freeMethods);
        //logs(__FILE__, __LINE__, in_array($this->action, $freeMethods));
        if (method_exists($this, $this->action)) {
            try {
                $this->userProtection->handle();
                if (!is_null($this->userProtection->getUser())){
                    $this->user = $this->userProtection->getUser();
                }

                $this->{$this->action}();

            } catch (UserBlock $e) {
                $this->redirToLogin();
                $this->getResponse()->unauth('');
                logs(__FILE__, __LINE__, '1');
            } catch (UserRedirect $e) {
                //logs(__FILE__, __LINE__, get_class($e));
                logs(__FILE__, __LINE__, '1');
                switch ($e->getCode()){
                    case UserRedirect::REDIRECT_LOGIN:
                        logs(__FILE__, __LINE__, '1');
                        $this->redirToLogin();
                        $this->getResponse()->unauth('');
                        break;
                    case UserRedirect::REDIRECT_PAGE:
                        logs(__FILE__, __LINE__, '1');
                        $this->redirToMainPage();
                        break;
                }
            }
            //logs(__FILE__, __LINE__, $protection->hasAccess);
        } else {
            //logs(__FILE__, __LINE__, '1');
            //logs(__FILE__, __LINE__, 'checkAccess-deprecated-'.$this->action);
            $this->getResponse()->badURI();
        }

        if (isset($callFnc['after'])){
            $this->{$callFnc['after']}();
        }
    }


    public function generateToken(int $timeLife, $add=[]) : string
    {
        //logs(__FILE__, __LINE__, 'generateCookie');
        $data = array_merge([
            'iat' => $this->timeNow,
            'exp' => $this->timeNow + $timeLife,
        ], $add);

        if ($this->user instanceof BaseModelDB){
            $data['uid'] = $this->user->id;

        }
        //logs(__FILE__, __LINE__, 'generateToken');
        return $this->accessGenerator->encode($data);
    }


    public function getTextShortcut(string $key) : string
    {
        if (isset($this->user->lang)){
            $lang = $this->user->lang;
        } else {
            $lang = $this->lang;
        }
        return GlobalData::getInstance()->getTextShortcut($key, $lang);
    }


    public function getTextShortcuts(array $key) : array
    {
        if (isset($this->user->lang)){
            $lang = $this->user->lang;
        } else {
            $lang = $this->lang;
        }

        return GlobalData::getInstance()->getTextShortcuts($key, $lang);
    }


    //$card->photo, 'photo'(field sender), 'photo'(field object), 'photo'(folder, folder files)
    /**
     * @param $oldNameImage
     * @param $keyFile (name file sender)
     * @param $keyColumn (field object)
     * @param $dir (first folder files)
     * @return string name new file
     * @throws \Exception
     */
    public function _fileHandler($oldNameImage, $keyFile, $keyColumn, $dir)
    {
        //logs(__FILE__, __LINE__, $key);
        //logs(__FILE__, __LINE__, $this->getRequestData()->files);
        $fileName = null;
        $fp = SwooleFileProvider::Create($this->getRequestData());
        $hasUplodFile =  !is_null($fp->existFileUpload($keyFile))
            && $this->getRequestData()->files[$keyFile]['error']==0
            && $this->getRequestData()->files[$keyFile]['size']>0;

        //удаляем файл
        if (
            //файл уже есть
            ($hasUplodFile && strlen($oldNameImage) > 0)

            //команда удаления для админки
            || (isset($this->getRequestData()->post['del_'.$keyColumn]) && $this->getRequestData()->post['del_'.$keyColumn] == 1)
        ){
            //удалять файл сразу если в тэмплэйте
            //logs(__FILE__, __LINE__, $oldNameImage);
            $path = FileHandler::getMPathFile($oldNameImage);
            //logs(__FILE__, __LINE__, $path);
            //logs(__FILE__, __LINE__, strpos($path, 'temp/'));
            //logs(__FILE__, __LINE__, strpos($path, 'temp/')===false);
            FileHandler::delFileTask($oldNameImage);
            //if (strpos($path, 'temp/') === false){
            //    FileHandler::delFileTask($dir, $oldNameImage);
            //} else {
            //    SwooleFileProvider::delFile2($path);
            //}
        }


        //сохраняем файл
        if ($hasUplodFile){
            //logs(__FILE__, __LINE__, 3);
            //$fileName = getRandomString(15).'_'.ID_SERVER.'_'.DIR_TEMPLATE.'.'.$fp->getExtension($keyFile);
            //$fp->upload($dirFile, $keyFile, $fileName);
            $fileName = FileHandler::saveFileTemplate($fp, $keyFile);
        }
        return $fileName;
    }



    protected function getIdServerFile($file){
        return FileHandler::getIdServerFile($file);
    }




}