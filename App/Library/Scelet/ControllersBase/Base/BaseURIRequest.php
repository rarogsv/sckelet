<?php
/**
 * Created by PhpStorm.
 * BaseUser: rarog
 * Date: 28.11.2019
 * Time: 18:14
 */

namespace App\Library\Scelet\ControllersBase\Base;


interface BaseURIRequest
{
    function login();
    function logout();
}