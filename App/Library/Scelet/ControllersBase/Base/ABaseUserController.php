<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 20.02.2020
 * Time: 15:18
 */

namespace App\Library\Scelet\ControllersBase\Base;

use App\Library\Scelet\ControllersBase\Injection;
use App\Library\Scelet\Query\SwooleHtppRequest;
use App\Library\Scelet\Query\SwooleHttpResponse;
use App\Library\Scelet\Util\AccessHandler;
use Swoole\Http\Request;

abstract class ABaseUserController extends BaseController
{


    public $tableUser;
    public $user;
    public $accessGenerator;
    public $lang = DEFAULT_LANGUAGE;

    public function __construct(Injection $injection, string $table)
    {
        parent::__construct($injection);
        $this->tableUser = $table;
        $this->accessGenerator = new AccessHandler($injection->getPublicKey());

    }

    public function clearCookie($nameCookie = null, $d=DOMAIN){
        //logs(__FILE__, __LINE__, $this->tableUser);
        if (is_null($nameCookie)){
            $nameCookie = $this->tableUser;
        }
        logs(__FILE__, __LINE__, 'clearCookie');
        logs(__FILE__, __LINE__, [$nameCookie, $d]);
        //logs(__FILE__, __LINE__, [$nameCookie, $d]);
        if (isset($this->getRequestData()->cookie[$nameCookie])){
            $this->getResponse()->setCookie($nameCookie, '', time()-100, $d);
        }
    }
    
    
    public function setCookie($nameCookie = null, $add=[], $d=DOMAIN, $tokenLife=0){
        if (is_null($nameCookie)){
            $nameCookie = $this->tableUser;
        }
        if ($tokenLife == 0){
            $tokenLife = TOKEN_LIFE;
        }
        $this->getResponse()->setCookie($nameCookie, $this->generateToken($tokenLife, $add), $this->timeNow+$tokenLife, $d);
    }

    /**
     * @return SwooleHttpResponse
     */
    public function &getResponse() : SwooleHttpResponse
    {
        //logs(__FILE__, __LINE__, $this->response);
        return $this->response;
    }

    /**
     * @return SwooleHtppRequest
     */
    public function &getRequest(): SwooleHtppRequest
    {
        return $this->request;
    }

    /**
     * @return Request
     */
    public function getRequestData(): Request
    {
        return $this->getRequest()->getRequest();
    }

    /**
     * @return array
     */
    public function getRequestPost(){
        return $this->getRequest()->getRequest()->post;
    }

    /**
     * @return array
     */
    public function getRequestGet($key){
        if (isset($this->getRequest()->getRequest()->get[$key])){
            return $this->getRequest()->getRequest()->get[$key];
        } else {
            return null;
        }
    }

    /**
     * @return array
     */
    public function getRequestJson(){
        $res = [];
        if ($this->getRequest()->getRequest()->rawContent() != null){
            $res = json_decode($this->getRequest()->getRequest()->rawContent(), true);
        }
        return $res;
    }

    /**
     * @param array $list
     * @return array
     * ничего не возращает если нету поля и занчение по умолчанию
     */
    public function getCastDataPost(array $list): array
    {
        return $this->getRequest()->getCastData($list, 'POST');
    }

    /**
     * @param array $list
     * @return array
     */
    public function getCastDataGet(array $list): array
    {
        return $this->getRequest()->getCastData($list, 'GET');
    }

    /**
     * @param array $list
     * @return array
     */
    public function getCastDataJson(array $list): array
    {
        return $this->getRequest()->getCastData($list, 'JSON');
    }


    /**
     * @param $field
     * @param $type
     * @param null $default
     * @return false|float|int|null|string
     */
    public function getCastFieldPost($field, $type, $default=null)
    {
        return $this->getRequest()->getCastField($field, $type, $default, 'POST');
    }

    /**
     * @param $field
     * @param $type
     * @param $default
     * @return false|float|int|null|string
     */
    public function getCastFieldGet($field, $type, $default=null)
    {
        return $this->getRequest()->getCastField($field, $type, $default, 'GET');
    }

    /**
     * @param $field
     * @param $type
     * @param $default
     * @return false|float|int|null|string
     */
    public function getCastFieldJson($field, $type, $default=null)
    {
        return $this->getRequest()->getCastField($field, $type, $default, 'JSON');
    }

    abstract function handle();
    
    abstract function generateToken(int $timeLife, $add=[]) : string;

    abstract function redirToLogin();
    abstract function redirToMainPage();


}