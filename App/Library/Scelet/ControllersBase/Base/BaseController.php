<?php

namespace App\Library\Scelet\ControllersBase\Base;


use App\Library\Scelet\ControllersBase\Injection;


/**
 * Class Controller
 * @package App\Controllers
 */
class BaseController
{

    protected $TAG;
    public $db;
    protected $uri = []; //Название метод для выполнения
    protected $response;
    protected $request;
    public $timeNow=0;
    public $server = null;

    /**
     * Controller constructor.
     * @param Injection $injection
     */
    public function __construct(Injection $injection){
        $this->timeNow = time();
        $this->uri = $injection->getUri();
        $this->db = $injection->getDb();
        $this->request = $injection->getRequest();
        $this->response = $injection->getResponse();
        $this->server = $injection->getServer();
    }

}