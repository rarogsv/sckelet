<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 05.03.2020
 * Time: 16:13
 */
namespace App\Library\Scelet\ControllersBase\Base;



interface BaseWebURIRequest extends BaseURIRequest
{
    function page();
    function getContentTable();
    function quick();
    function getEditor();
    function editRow();
    function delete();
}