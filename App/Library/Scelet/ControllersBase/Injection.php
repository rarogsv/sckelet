<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 25.01.2019
 * Time: 15:38
 */
namespace App\Library\Scelet\ControllersBase;



use App\Library\Scelet\DataBase\PDORequest;
use App\Library\Scelet\Query\SwooleHtppRequest;
use App\Library\Scelet\Query\SwooleHttpResponse;
use Swoole\Http\Server;

class Injection
{
    protected $uri          = [];
    protected $publicKey    = '';

    protected $response     = null;
    protected $request      = null;
    protected $server       = null;

    public function __construct(string $publicKey, array $uri, $request, $response, $server)
    {
        $this->server       = $server;
        $this->uri          = $uri;
        $this->publicKey    = $publicKey;
        $this->response     = $response;
        $this->request      = $request;
    }


    public function getServer()
    {
        return $this->server;
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    /**
     * @return array
     */
    public function getUri() : array
    {
        return $this->uri;
    }


    /**
     * @return PDORequest
     */
    public function getDb(): PDORequest
    {
        return PDORequest::Create();
    }


    public function getRequest()
    {
        return $this->request;
    }


    public function getResponse()
    {
        return $this->response;
    }

}