<?php

namespace App\Library\Scelet\ModelBase;


use App\Library\Scelet\ModelBase\Interfaces\Creator;


/**
 * Class Version
 * @package App\Model
 */
class Version extends BaseModelDB implements Creator
{

    public $comment     = '';
    public $hash        = '';
    public $current     = 0;


    function __construct($dbName = DB_NAME_SETTING)
    {
        parent::__construct(BaseDBMap::VERSION, $dbName);
    }


    /**
     * @param $id

     * @param string $dbName
     * @return Version
     * @throws \Exception
     */
    public static function Load($id, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Version($dbName);
        $obj->_load($id);
        return $obj;
    }

    /**
     * @param array $data

     * @param string $dbName
     * @return Version
     */
    public static function Create(array $data=[], string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Version($dbName);
        $obj->toObject($data);
        return $obj;
    }

    /**
     * @param array $data

     * @param string $dbName
     * @return Version
     * @throws \Exception
     */
    public static function Search(array $data, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Version($dbName);
        $obj->loadLike($data);
        return $obj;
    }


}
