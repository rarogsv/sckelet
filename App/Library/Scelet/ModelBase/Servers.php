<?php

namespace App\Library\Scelet\ModelBase;


use App\Library\Scelet\ModelBase\Interfaces\Creator;
use App\Library\Scelet\Util\SysInfo;
use App\Model\GlobalData;

/**
 * Class Servers
 * @package App\Model
 */
class Servers extends BaseModelDB implements Creator
{


    public $enable              = 1;
    public $comment             = '';
    public $ip                  = '';
    public $git_hash            = '';
    public $git_hash_beafore    = '';
    public $ram                 = 0;
    public $rom                 = 0;
    public $last_update         = 0;
    public $cpu                 = 0;
    public $data                = '{}';
    public $speed               = '{}';

    public $comment_git         = '';
    public $comment_composer    = '';


    function __construct($dbName = DB_NAME_SETTING)
    {
        parent::__construct(BaseDBMap::SERVER, $dbName);
    }


    /**
     * @param $id

     * @param string $dbName
     * @return Servers
     * @throws \Exception
     */
    public static function Load($id, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Servers($dbName);
        $obj->_load($id);
        return $obj;
    }

    /**
     * @param array $data
     * @param string $dbName
     * @return Servers
     */
    public static function Create(array $data=[], string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Servers($dbName);
        $obj->toObject($data);
        return $obj;
    }

    /**
     * @param array $data
     * @param string $dbName
     * @return Servers
     * @throws \Exception
     */
    public static function Search(array $data, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Servers($dbName);
        $obj->loadLike($data);
        return $obj;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return json_decode($this->data, true);
    }


    public function online($key)
    {
        //logs(__FILE__, __LINE__, $key);
        //logs(__FILE__, __LINE__, $this->id);
        if ($this->id > 0){
            $ram    = SysInfo::getRAM();
            $rom    = SysInfo::getROM();
            $cpu    = round(SysInfo::getCPU()[0]);

            $data = $this->getData();

            $data[$key] = time();
            //[time(), GlobalData::$pid];
            //logs(__FILE__, __LINE__,  $data[$key]);

            $this->update([
                'last_update' => time(),
                'ram'   =>$ram,
                'rom'   =>$rom,
                'cpu'   =>$cpu,
                'data' => json_encode($data),
            ]);
        }
    }


    public function setDataServer($speeds){
        //logs(__FILE__, __LINE__, $this->id > 0);
        if ($this->id > 0){
            $ram    = SysInfo::getRAM();
            $rom    = SysInfo::getROM();
            $cpu    = round(SysInfo::getCPU()[2]*100);

            $hoursNow=intval(date('H'));
            $data = $this->getData();
            $data['inf'][$hoursNow] = [$ram, $rom, $cpu];

            //logs(__FILE__, __LINE__, $data);
            //logs(__FILE__, __LINE__, $this->dbName);

            $this->update([
                'last_update' => time(),
                'data' => json_encode($data),
                'speed' => json_encode($speeds)
            ]);

        }
    }


}
