<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 02.03.2020
 * Time: 17:46
 */

namespace App\Library\Scelet\ModelBase;




use App\Library\Scelet\DataBase\PDORequest;
use Exception;

class Dictionary
{

    protected $data = [];


    /**
     * @param $id
     * @return Dictionary
     * @throws \Exception
     */
    public static function Load($id) : self
    {
        $obj = new Dictionary();
        $obj->data = PDORequest::Create()->where('id', $id)->getOne(BaseDBMap::DICTIONARY);
        return $obj;
    }

    /**
     * @param array $data
     * @return Dictionary
     */
    public static function Create(array $data) : self
    {
        $obj = new Dictionary();
        $obj->data = $data;
        return $obj;
    }

    public static function get(int $id, string $lang = DEFAULT_LANGUAGE)
    {

        $data = PDORequest::Create()->where('id', $id)->getOne(BaseDBMap::DICTIONARY, [$lang]);
        return $data[$lang];
    }

    public static function getArr(array $id, string $lang) : array
    {
        $data = PDORequest::Create()->where('id', $id, 'IN')->get(BaseDBMap::DICTIONARY, [$lang]);
        if (count($data) > 0){
            return array_column($data, $lang);
        } else {
            return [];
        }
    }


    /**
     * @param array $data
     * @return int
     * @throws Exception
     */
    public function save(array $data) : int
    {
        $id = PDORequest::Create()->insert(BaseDBMap::DICTIONARY, $data);

        if ($id === false){
            $id = 0;
        } else {
            $this->data['id'] = $id;
        }
        return $id;
    }


    /**
     * @return bool
     * @throws Exception
     */
    public function delete() : bool
    {
        return PDORequest::Create()->where('id',  $this->data['id'])->delete(BaseDBMap::DICTIONARY);

    }


    /**
     * @param array $data
     * @return bool
     */
    public function update(array $data=null) : bool
    {

        if (is_null($data)){
            $data = $this->data;
        }

        if (isset($data['id'])){
            unset($data['id']);
        }

        $res = PDORequest::Create()->where('id', $this->data['id'])->update(BaseDBMap::DICTIONARY, $data);

        foreach ($data as $k=>$d){
            $this->data[$k] = $d;
        }

        return $res;
    }


}