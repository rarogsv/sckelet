<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 03.08.2019
 * Time: 2:56
 */

namespace App\Library\Scelet\ModelBase;


use App\Library\Scelet\DataBase\PDORequest;
use Exception;
use PDO;
use Throwable;


class BaseGlobalData
{
    protected $db;
    protected $isTest = false;
    protected $loggerSpeed = [];
    protected $dictionary = [];

    //pid
    public static $pid=0;

    //global connects
    public static $connects = [];
    public static $lastRequest = '';


    /**
     * BaseGlobalData constructor.
     * @throws Exception
     */
    public function __construct()
    {

        //$this->loggerFast = new LoggerFast();
        $this->connectDB();
        $this->loadDictionary();
        self::$pid = posix_getpid();
    }

    public static function getPDO($dbName){
        return self::$connects[$dbName];
    }


    public function addConnection($dbName, $dbConnect){
        //$default['host'], $default['username'], $default['password'], $default['db']
        $connSet = 'mysql:host='.$dbConnect['host'].';';
        if (isset($dbConnect['socket'])){
            $connSet .= 'port='.$dbConnect['socket'].';';
        }
        $connSet .= 'dbname='.$dbConnect['db'].';charset='.$dbConnect['charset'];

        //logs(__FILE__, __LINE__, $connSet);
        //logs(__FILE__, __LINE__, $dbConnect['username']);
        //logs(__FILE__, __LINE__, $dbConnect['password']);
        try {
            self::$connects[$dbName] = new PDO(
                $connSet,
                $dbConnect['username'],
                $dbConnect['password'],
                [
                    PDO::ATTR_PERSISTENT => true,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
                ]);
        } catch (Exception $e) {
            logs(__FILE__, __LINE__, $e->getMessage());
        }

        //переводит строки в числовое значение
        self::$connects[$dbName]->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$connects[$dbName]->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        self::$connects[$dbName]->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
    }

    /**
     */
    public function connectDB()
    {
        //logs(__FILE__, __LINE__, 'connectDB');
        foreach (DB_LIST as $name => $item) {
            try {
                $this->addConnection($name, $item);
            } catch (Throwable $e) {
            }
        }
    }


    /**
     * @return array
     */
    public function getDictionary($lang=DEFAULT_LANGUAGE): array
    {
        return $this->dictionary;
    }

    public function getTextShortcut(string $shortcut, $lang=DEFAULT_LANGUAGE): string
    {
        // logs(__FILE__, __LINE__, count($this->dictionary));
        //$d = indexOfObj($shortcut, $this->dictionary, 'shortcut');
        if (isset($this->dictionary[$shortcut])){
            return $this->dictionary[$shortcut][strtoupper($lang)];
        } else {
            return $shortcut;
        }
    }

    public function getTextShortcuts(array $shortcuts, $lang): array
    {
        // logs(__FILE__, __LINE__, count($this->dictionary));
        $res = [];
        foreach ($shortcuts as $shortcut){
            $res[$shortcut] = $this->getTextShortcut($shortcut, $lang);
        }

        return $res;
    }


    /**
     * @throws Exception
     */
    function loadDictionary(){
         $dic= PDORequest::Create()->connection(DB_NAME_SETTING)->get(BaseDBMap::DICTIONARY);
         $this->dictionary = [];
         foreach ($dic as $d){
             $this->dictionary[$d['shortcut']] = $d;
         }

        /*foreach ($this->dictionary as $dic){
            logs(__FILE__, __LINE__, $dic['id']);
        }*/
    }


    /**
     * @return array
     */
    public function getLoggerSpeed(): array
    {
        return $this->loggerSpeed;
    }


    /**
     * @param string $action
     * @param int $start
     * @param $stop
     */
    public function setLoggerSpeed(string $action, int $start, int $stop)
    {
        $this->loggerSpeed[$action] = round($stop - $start);
    }

    public function ping()
    {
        foreach (DB_LIST as $key => $setting){
            try {
                PDORequest::Create()->connection($key)->ping();
            } catch (Throwable $e) {
                $this->connectDB();
                //\App\Library\Scelet\Util\ServerHandler::exception($e,  'ping');
                //logs(__FILE__, __LINE__, $e->getFile());
                //logs(__FILE__, __LINE__, $e->getLine());
                //logs(__FILE__, __LINE__, $e->getMessage());
            }
        }

    }

}