<?php
/**
 * Created by PhpStorm.
 * UserVerification: rarog
 * Date: 17.01.2019
 * Time: 12:26
 */
namespace App\Library\Scelet\ModelBase\Interfaces;


interface Creator
{
    public static function Load($id, string $dbName = DB_NAME_DEFAULT);
    public static function Create(array $data=[], string $dbName = DB_NAME_DEFAULT);
    public static function Search(array $data, string $dbName = DB_NAME_DEFAULT);
}