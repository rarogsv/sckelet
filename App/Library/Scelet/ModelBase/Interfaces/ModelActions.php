<?php
/**
 * Created by PhpStorm.
 * BaseUser: rarog
 * Date: 08.11.2019
 * Time: 10:34
 */

namespace App\Library\Scelet\ModelBase\Interfaces;


interface ModelActions
{

    /**
     * @return bool
     */
    public function update();

    /**
     * @return int
     */
    public function save();
    /**
     * @param int $id
     */
    public function _load(int $id);
    /**
     * @return bool
     */
    public function delete();

    /**
     * @param array $data
     */
    public function loadLike(array $data);

}