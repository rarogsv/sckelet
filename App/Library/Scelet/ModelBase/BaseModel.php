<?php

namespace App\Library\Scelet\ModelBase;




class BaseModel
{

    /**
     * ObjectBase constructor.
     */
    function __construct(){
    }

    /**
     * @param array $data
     */
    public function toObject(array $data)
    {
        foreach ($data as $key => $d){
            if (isset($this->{$key})){
                $this->{$key} = $d;
            }
        }
    }

    public function toArray(){
        return (array) $this;
        /*if ($columns == null){
            return (array) $this;
        } else {
            $d = [];
            logs(__FILE__, __LINE__, $columns);
            foreach ($columns as $key){
                //if (!isset($this->{$key})){throw new Exception($key.' not found');}
                $d[$key] = $this->{$key};
            }
            return $d;
        }*/
    }

    protected function toHash(string $data) : string
    {
        return password_hash($data, PASSWORD_DEFAULT);
    }

    protected function verifyHash(string $verified, string $hash) : bool
    {
        if ( password_verify($verified, $hash)) {
            return true;
        } else {
            return false;
        }
    }
}
