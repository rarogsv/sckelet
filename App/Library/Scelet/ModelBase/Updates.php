<?php

namespace App\Library\Scelet\ModelBase;


use App\Library\Scelet\ModelBase\Interfaces\Creator;

/**
 * Class Updates
 * @package App\Model
 */
class Updates extends BaseModelDB implements Creator
{

    public $current  = 1;
    public $hash = '';


    function __construct($dbName = DB_NAME_SETTING)
    {
        parent::__construct(BaseDBMap::SERVER, $dbName);
    }


    /**
     * @param $id

     * @param string $dbName
     * @return Updates
     * @throws \Exception
     */
    public static function Load($id, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Updates($dbName);
        $obj->_load($id);
        return $obj;
    }

    /**
     * @param array $data
     * @param string $dbName
     * @return Updates
     */
    public static function Create(array $data=[], string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Updates($dbName);
        $obj->toObject($data);
        return $obj;
    }

    /**
     * @param array $data
     * @param string $dbName
     * @return Updates
     * @throws \Exception
     */
    public static function Search(array $data, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new Updates($dbName);
        $obj->loadLike($data);
        return $obj;
    }


}
