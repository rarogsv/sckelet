<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 20.01.2019
 * Time: 20:58
 */
namespace App\Library\Scelet\ModelBase\Logger;


use App\Library\Scelet\DataBase\PDORequest;
use App\Library\Scelet\ModelBase\BaseDBMap;
use Exception;
use Throwable;

/**
 * Class LoggerBug
 * @package App\Library\Logger
 */
class LoggerBug extends BaseLogger
{
    //0-debug, 1-bug

    public $trace = '';
    public $db_last_query = '';
    public $request='{}';
    public $message='';
    public $count = 1;

    /**
     * LoggerBug constructor.
     * @param string $controller
     * @param string $action
     * @param array $request //get post cookie files addr
     * @param Throwable $exception
     */
    function __construct(string $controller, string $action, array $request, Throwable $exception)
    {
        $this->trace = $exception->getTraceAsString();
        if (strlen($this->trace) > 60000){
            $this->trace = substr($this->trace, 0, 60000);
        }
        $this->request = json_encode($request);
        if (strlen($this->request) > 60000){
            if (isset($request['post'])){
                $request['post'] = 'VERY BIG';
                $this->request = json_encode($request);
            }
        }


        if (strlen($exception->getMessage()) > 250){
            $this->message = substr($exception->getMessage(), 0, 250);
        } else {
            $this->message = $exception->getMessage();
        }


        parent::__construct($controller, $action);
    }


    /**
     * @param string $controller
     * @param string $action
     * @param array $request
     * @param Exception $exception
     * @return LoggerBug
     */
    public static function error(string $controller, string $action, array $request, Exception $exception){
        $obj = new LoggerBug($controller, $action, $request,$exception);
        return $obj;
    }


    public function save()
    {
        logs(__FILE__, __LINE__, $this->controller.$this->action);
        $db = PDORequest::Create();
        $duplicate = $db->connection(DB_NAME_SETTING)->where('controller', $this->controller)->where('action', $this->action)->getOne(BaseDBMap::LOG_BUG);

        logs(__FILE__, __LINE__, is_null($duplicate));
        if (is_null($duplicate)) {
            $this->created = time();
            $db->connection(DB_NAME_SETTING)->insert(BaseDBMap::LOG_BUG, (array)$this);
        } else {
            //logs(__FILE__, __LINE__, 2);
            $db->connection(DB_NAME_SETTING)->where('id', $duplicate['id'])->update(BaseDBMap::LOG_BUG, ['count' => $duplicate['count'] + 1]);
        }
    }


    /**
     * @param $minTime

     */
    public static function cleanDB($minTime){
        PDORequest::Create()->connection(DB_NAME_SETTING)->where('created', $minTime, '<')->delete(BaseDBMap::LOG_BUG);
    }


    public function setDBData($lastQuery){
        $this->db_last_query = $lastQuery;
    }
}