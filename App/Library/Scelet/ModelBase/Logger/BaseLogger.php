<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 10.03.2020
 * Time: 20:19
 */

namespace App\Library\Scelet\ModelBase\Logger;



class BaseLogger
{
    public $action='';
    public $controller='';
    public $created = 0;


    /**
     * BaseLogger constructor.
     * @param string $controller
     * @param string $action

     */
    function __construct( string $controller, string $action)
    {
        $this->action = $action;
        $this->controller = $controller;
    }


}