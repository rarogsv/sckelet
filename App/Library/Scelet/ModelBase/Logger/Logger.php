<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 03.08.2019
 * Time: 11:53
 */

namespace App\Library\Scelet\ModelBase\Logger;




/**
 * Interface Logger
 * @package App\Library\Logger
 */
interface Logger
{

    /**
     * @param $minTime

     * @return mixed
     */
    static function cleanOld($minTime);
}