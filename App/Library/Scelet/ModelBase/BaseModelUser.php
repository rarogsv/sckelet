<?php

namespace App\Library\Scelet\ModelBase;




class BaseModelUser extends BaseModelDB
{

    public $auth            = '';
    public $lang            = DEFAULT_LANGUAGE;
    public $block           = 0;
    public $name            = '';
    public $last_open       = 0;


    function __construct(string $table, string $dbName)
    {
        parent::__construct($table, $dbName);
    }

}
