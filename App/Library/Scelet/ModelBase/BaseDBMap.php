<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 10.03.2020
 * Time: 11:23
 */

namespace App\Library\Scelet\ModelBase;


class BaseDBMap
{
    //setting
    const SERVER            = 'server';
    const VERSION           = 'version';
    const UPDATES           = 'updates';
    const LOG_FAST          = 'log_fast';
    const LOG_BUG           = "log_bug";
    const LOG_REQUEST       = "log_request";


    //default
    const DICTIONARY        = 'dictionary';
    const SADMIN            = 'sadmin';
    const TASK_TICKER       = 'task_ticker';
    const HISTORY           = 'history';
}