<?php

namespace App\Library\Scelet\ModelBase;

use App\Library\Scelet\ModelBase\Interfaces\Creator;
use App\Model\Base\DBMap;

/**
 * Class Block
 * @package App\Model
 */
class Block extends BaseModelDB implements Creator
{


    public $type   = 0;
    public $user_id= 0;
    public $comment= '';
    public $user_db_name= '';


    function __construct($dbName = DB_NAME_DEFAULT)
    {
        parent::__construct(DBMap::BLOCK_USER, $dbName);
    }
    /**
     * @param $id
     * @param string $dbName
     * @return Block
     * @throws \Exception
     */
    public static function Load($id, string $dbName = DB_NAME_DEFAULT) : self
    {
        $obj = new Block();
        $obj->_load($id);
        return $obj;
    }

    /**
     * @param array $data
     * @param string $dbName
     * @return Block
     */
    public static function Create(array $data=[], string $dbName = DB_NAME_DEFAULT) : self
    {
        $obj = new Block();
        $obj->toObject($data);
        return $obj;
    }

    /**
     * @param array $data
     * @param string $dbName
     * @return Block
     * @throws \Exception
     */
    public static function Search(array $data, string $dbName = DB_NAME_DEFAULT) : self
    {
        $obj = new Block();
        $obj->loadLike($data);
        return $obj;
    }


}
