<?php

namespace App\Library\Scelet\ModelBase;


use App\Library\Scelet\ModelBase\Interfaces\Creator;

/**
 * Class TaskTicker
 * @package App\Model
 */
class TaskTicker extends BaseModelDB implements Creator
{
    const ACTION_DEEPLINK   = 1;
    const DEL_FILE          = 2;
    const TRANSFER          = 3;

    public $action      = 0;
    public $server_id   = 0;
    public $data        = '{}'; //данные



    function __construct($dbName = DB_NAME_SETTING)
    {
        parent::__construct(BaseDBMap::TASK_TICKER, $dbName);
    }

    /**
     * @param $id

     * @param string $dbName
     * @return TaskTicker
     * @throws \Exception
     */
    public static function Load($id, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new TaskTicker();
        $obj->_load($id);
        return $obj;
    }

    /**
     * @param array $data

     * @param string $dbName
     * @return TaskTicker
     */
    public static function Create(array $data=[], string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new TaskTicker();
        $obj->toObject($data);
        return $obj;
    }

    /**
     * @param array $data

     * @param string $dbName
     * @return TaskTicker
     * @throws \Exception
     */
    public static function Search(array $data, string $dbName = DB_NAME_SETTING) : self
    {
        $obj = new TaskTicker();
        $obj->loadLike($data);
        return $obj;
    }
}
