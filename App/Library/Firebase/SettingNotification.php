<?php
namespace App\Library\Firebase;

	use Exception;

    class SettingNotification  {
    //$aPayload see https://firebase.google.com/docs/cloud-messaging/http-server-ref#downstream-http-messages-json
    // $aOptions see https://firebase.google.com/docs/cloud-messaging/http-server-ref#downstream-http-messages-json
        /*
            collapse_key: String
            This parameter identifies a group of messages (e.g., with collapse_key: "Updates Available") that can be collapsed, so that only the last message gets sent when delivery can be resumed. This is intended to avoid sending too many of the same messages when the device comes back online or becomes active.

            Note that there is no guarantee of the order in which messages get sent.

            Note: A maximum of 4 different collapse keys is allowed at any given time. This means a FCM connection server can simultaneously store 4 different addField-to-sync messages per client app. If you exceed this number, there is no guarantee which 4 collapse keys the FCM connection server will keep.
         */
        public $collapse_key = null;

        /*
            priority: String
            Sets the priority of the message. Valid values are "normal" and "high." On iOS, these correspond to APNs priorities 5 and 10.

            By default, notification messages are sent with high priority, and data messages are sent with normal priority. Normal priority optimizes the client app's battery consumption and should be used unless immediate delivery is required. For messages with normal priority, the app may receive the message with unspecified delay.

            When a message is sent with high priority, it is sent immediately, and the app can wake a sleeping device and open a network connection to your server.

            more info: https://firebase.google.com/docs/cloud-messaging/concept-options#setting-the-priority-of-a-message
         */
        public $priority = null;
        /*
            content_available: Boolean
            On iOS, use this field to represent content-available in the APNs payload. When a notification or message is sent and this is set to true, an inactive client app is awoken. On Android, data messages wake the app by default. On Chrome, currently not supported.
         */
        public $content_available = null;
        /*
            mutable_content: Boolean
            Currently for iOS 10+ devices only. On iOS, use this field to represent mutable-content in the APNS payload. When a notification is sent and this is set to true, the content of the notification can be modified before it is displayed, using a Notification Service app extension. This parameter will be ignored for Android and web.
         */
        public $mutable_content = null;
        /*
            time_to_live: number
            This parameter specifies how long (in seconds) the message should be kept in FCM storage if the device is offline. The maximum time to live supported is 4 weeks, and the default value is 4 weeks. For more information, see https://firebase.google.com/docs/cloud-messaging/concept-options#ttl
         */
        public $time_to_live = null;
        /*
            restricted_package_name: string
            This parameter specifies the package name of the application where the registration tokens must match in order to receive the message.
         */
        public $restricted_package_name = null;
        /*
            dry_run: boolean
            This parameter, when set to true, allows developers to test a request without actually sending a message.
            The default value is false.
         */
        public $dry_run = null;


        /*anithing*/
        public $data = null;


        /*for web*/
        public $notification = [
            'title' => '',
            'body' => '',
        ];


        public static function Create()
        {
            return new SettingNotification();
        }



        public function setCollapseKey($collapse_key) : self
        {
            $this->collapse_key = $collapse_key;
            return $this;
        }


        public function setPriority(string $priority) : self
        {
            $this->priority = $priority;
            return $this;
        }


        public function setContentAvailable($content_available) : self
        {
            $this->content_available = $content_available;
            return $this;
        }


        public function setMutableContent($mutable_content) : self
        {
            $this->mutable_content = $mutable_content;
            return $this;
        }

        public function setTimeToLive(int $time_to_live) : self
        {
            $this->time_to_live = $time_to_live;
            return $this;
        }

        public function setRestrictedPackageName($restricted_package_name) : self
        {
            $this->restricted_package_name = $restricted_package_name;
            return $this;
        }

        public function setDryRun($dry_run) :self
        {
            $this->dry_run = $dry_run;
            return $this;
        }

        public function setNotification(string $title, string $body) : self
        {
            $this->notification['title'] = $title;
            $this->notification['body'] = $body;
            return $this;
        }


        public function setNotificationSound(string $urlSoundDevice=DB_NAME_DEFAULT) : self
        {
            $this->notification['sound'] = $urlSoundDevice;
            return $this;
        }

        public function setNotificationIcon(string $urlIcon) : self
        {
            $this->notification['icon'] = $urlIcon;
            return $this;
        }

        public function setNotificationClickAction(string $urlSite) : self
        {
            $this->notification['click_action'] = $urlSite;
            return $this;
        }




        public function setData($data) : self
        {
            $this->data = $data;
            return $this;
        }



        public function toArray() : array
        {
            if (!is_null($this->data) && is_array($this->data)){
                foreach($this->data as $key => $value) {
                    if ($key == 'from' || stripos('google', $key) || stripos('gcm', $key)) {
                        throw new Exception('Invalid Payload: "data" key "'.$key.'" is a reserved keyword');
                    } else if (in_array($key, $this->data)) {
                        throw new Exception('Invalid Payload: "data" key "'.$key.'" is a reserved keyword for Options');
                    }
                }
            }

            $dataFull = (array) $this;
            $res = [];
            foreach ($dataFull as $key => $value){
                if (!is_null($value)){
                    $res[$key] = $value;
                }
            }
            return $res;
        }

	}


