<?php
namespace App\Library\Firebase;

	use Exception;

    class FCMPushNotification  {
		//https://github.com/BarryDam/FCMPushNotification
		private $APIKey; // Firebase project Serverkey

		public function __construct($sAPIKey) {
			$this->APIKey = $sAPIKey;
		}



        /**
         * private addField method
         * @param  array $aData addField data
         * @return array    server response data
         * @throws Exception
         */
		private function _send($aData)
		{
			// Prepare headers
			$aHeaders = [
				'Authorization: key='.$this->APIKey,
				'Content-Type: application/json'
            ];
			// Curl request

            logs(__FILE__, __LINE__, $aHeaders);
            logs(__FILE__, __LINE__, $aData);
			$ch				= curl_init();
			curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');//old
			//curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/v1/projects/myproject-b5ae1/messages:send');
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $aHeaders);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($aData));
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			$result 		= curl_exec($ch);
            //logs(__FILE__, __LINE__, $result);
			$httpcode		= curl_getinfo($ch, CURLINFO_HTTP_CODE);
			curl_close($ch);
			// Process result
			if ($httpcode == "401") { // unauthorized
				throw new Exception("There was an error authenticating the sender account.", 401);
			} else if ($httpcode == "200") { // OK
				return json_decode($result, true);	
			} else { // Unkown
				throw new Exception($result, $httpcode);				
			}
		}

        /**
         * Condition based push notification
         * @param  string $sCondition topic condition
         * @param SettingNotification $settingNotification
         * @return array [type]             [description]
         * @throws Exception
         */
		public function sendToCondition($sCondition, SettingNotification $settingNotification)
		{
			if (! is_string($sCondition)) {
				throw new Exception("Invalid Condition");
			}
            $data = $settingNotification->toArray();
			$data['condition'] 	= $sCondition;
			return $this->_send($data);
		}

        /**
         * Send push notification to single device
         * @param  string $sRegistrationToken The registation token comes from the client FCM SDKs
         * @param SettingNotification $settingNotification
         * @return array    server response data
         * @throws Exception
         */
		public function sendToDevice($sRegistrationToken, SettingNotification $settingNotification)
		{
			if (! is_string($sRegistrationToken)) {
				throw new Exception("Invalid RegistrationToken");
			}
            $data = $settingNotification->toArray();
            $data['to'] 	= $sRegistrationToken;
			return $this->_send($data);
		}

        /**
         * Send push notification to multiple devices
         * @param  array $aRegistrationTokens array with devices RegistrationTokens
         * @param SettingNotification $settingNotification
         * @return array    server response data
         * @throws Exception
         */
		public function sendToDevices($aRegistrationTokens, SettingNotification $settingNotification)
		{
			if (! is_array($aRegistrationTokens) || count($aRegistrationTokens) === 0) {
				throw new Exception("Invalid RegistrationTokens");
			}
            $data = $settingNotification->toArray();
            $data['registration_ids'] 	= $aRegistrationTokens;
			return $this->_send($data);
		}

        /**
         * Send push notification to group
         * @param  string $sNotificationKey group id
         * @param SettingNotification $settingNotification
         * @return array    server response data
         * @throws Exception
         */
		public function sendToDeviceGroup($sNotificationKey, SettingNotification $settingNotification) {
			if (! is_string($sNotificationKey)) {
				throw new Exception("Invalid NotificationKey");
			}
            $data = $settingNotification->toArray();
            $data['to'] 	= $sNotificationKey;
			return $this->_send($data);
		}

        /**
         * addField to topic
         * @param  string $sTopic topic which devices can subscribe to
         * @param SettingNotification $settingNotification
         * @return array    server response data
         * @throws Exception
         */
		public function sendToTopic($sTopic, SettingNotification $settingNotification)
		{
			if (! is_string($sTopic)) {
				throw new Exception("Invalid Topic");
			}
            $data = $settingNotification->toArray();
            $data['to'] 	= '/topics/'.$sTopic;
			return $this->_send($data);
		}		
	}



