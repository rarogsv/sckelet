<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 14.05.2020
 * Time: 16:22
 */

namespace App\Library\WebAdmin\Constructor;


use App\Library\Scelet\ControllersBase\Base\BaseWebController;
use App\Library\Scelet\DataBase\PDORequest;
use App\Library\Scelet\ModelBase\BaseModelUser;
use App\Library\WebAdmin\Constructor\Window\Editor;
use App\Library\WebAdmin\Constructor\Field\Field;
use App\Library\WebAdmin\Constructor\Field\BaseInput;


class FormCreator
{

    public $db;
    public $controller;
    protected $editor;
    public $table;
    public $id=0;
    public $serverAction='';



    public $badData = false;
    public $comment = '';

    public function __construct(BaseWebController $controller)
    {
        $this->controller = $controller;

        $this->id = $this->controller->getCastFieldPost('id', INT, 0);
        $this->table = $this->controller->getCastFieldPost('table', STRING, null);
        logs(__FILE__, __LINE__, $this->table);
        $this->editor = $this->createEditor();
        $this->db = PDORequest::Create();
    }

    function getServerAction(){
        return $this->controller->getCastFieldPost('server_action', STRING, null);
    }

    function getTableParent(){
        return $this->controller->getCastFieldPost('tp', STRING, null);
    }

    function getTableParentID(){
        return $this->controller->getCastFieldPost('tp_id', INT, 0);
    }

    function getField(BaseInput $input, string $shortcut=null, $show=true): Field
    {
        if (is_null($shortcut)){
            return Field::Create('', $input, intval($show));
        } else {
            return Field::Create($this->controller->getTextShortcut($shortcut), $input, intval($show));
        }
    }


    protected function createEditor() : Editor
    {
        $editor = new Editor();
        if ($this->id > 0){
            $title = $this->controller->getTextShortcut('edit');
        } else {
            $title = $this->controller->getTextShortcut('create');
        }


        $editor->setTitle($title)
            ->addBtnAction(Button::Create()->setText($this->controller->getTextShortcut('save'))->setAction(Button::ACTION_SAVE))
            ->setId($this->id)
            ->setTable($this->table);
        return $editor;
    }

    /**
     * @return Editor
     */
    public function getEditor(): Editor
    {
        return $this->editor;
    }




}