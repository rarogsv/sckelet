<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 14.05.2020
 * Time: 16:22
 */

namespace App\Library\WebAdmin\Constructor;


use App\Library\Scelet\ControllersBase\Base\BaseSAdminController;
use App\Library\Scelet\ModelBase\BaseDBMap;
use App\Library\Scelet\ModelBase\Servers;


class BaseSAdminEdit extends FormEdit
{

    public function __construct(BaseSAdminController $controller)
    {
        parent::__construct($controller);

        switch ($this->table){
            case BaseDBMap::SERVER:
                $this->editServerLogic();
                break;
        }
    }

    protected function editServerLogic(){
        switch ($this->controller->getRequestData()->post['server_action']){
            case 'editor':
                $this->editServer($this->id);
                break;
            case 'reply':
                //{"id":"0","table":"server","server_action":"reply","git_hash":"werwe"}
                if (strlen($this->controller->getRequestPost()['git_hash']) > 0){
                    //logs(__FILE__, __LINE__, $this->getRequestData()->post['git_hash']);
                    $this->controller->replyVersion($this->controller->getRequestPost()['git_hash']);
                }
                break;
        }
    }



    //193.203.50.113
    protected function editServer(){
        //"files":{"avatar":{"name":"g10.jpg","type":"application\/octet-stream","tmp_name":"\/tmp\/swoole.upfile.enxSRa","error":0,"size":393406}},"post":{"lang":"RU"},"tmpfiles":["\/tmp\/swoole.upfile.enxSRa"]
        //logs(__FILE__, __LINE__, $this->getRequestData()->post);
        logs(__FILE__, __LINE__, $this->id);

        $data = $this->controller->getCastDataPost([
            ['ip', STRING],
            ['comment', STRING],
            ['enable', BOOL],
        ]);

        if ($this->id > 0){
            $obj= Servers::Load($this->id);
            $obj->update($data);
        } else {
            $obj = Servers::Create($data);
            $obj->save();
        }

        logs(__FILE__, __LINE__, $data);
    }


}