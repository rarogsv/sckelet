<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 14.05.2020
 * Time: 16:16
 */

namespace App\Library\WebAdmin\Constructor;


use App\Library\WebAdmin\Constructor\Field\BaseInput;

class Button extends BaseInput
{
    const COLOR_BLUE = 'btn-primary';
    const COLOR_BLUE_LIGHT = 'btn-info';
    const COLOR_BLUE2 = 'btn-primary';
    const COLOR_GREEN = 'btn-success';
    const COLOR_RED = 'btn-danger';
    const COLOR_YELLOW = 'btn-warning';

    const ACTION_COPY   = 'copy';
    const ACTION_TABLE  = 'table';
    const ACTION_EDIT   = 'edit';
    const ACTION_LINK   = 'link';
    const ACTION_QUICK  = 'quick';
    const ACTION_SAVE   = 'save';
    const ACTION_DELETE = 'delete';

    const COLOR_BY_ACTION=[
        self::ACTION_TABLE=>self::COLOR_BLUE_LIGHT,
        self::ACTION_EDIT=>self::COLOR_GREEN,
        self::ACTION_LINK=>self::COLOR_YELLOW,
        self::ACTION_DELETE=>self::COLOR_RED,
        self::ACTION_SAVE=>self::COLOR_BLUE,
        ];

    public $icon        = '';
    public $color       = Button::COLOR_BLUE;
    public $text        = '';
    public $action      = '';
    public $table       = '';
    public $table_parent= '';
    public $msg         = '';
    public $server_action= '';
    public $href        = '';
    public $_blanc      = 0;
    public $url_copy    = '';
    public $clas        = 'btn-xs';
    public $onclick     = '';

    public static function Create($action=null, $text=null, $iconBtn=null, $colorBtn=null): Button
    {
        $btn = new Button();
        if (!is_null($action)){
            $btn->setAction($action);
        }
        if (!is_null($text)){
            $btn->setText($text);
        }
        if (!is_null($iconBtn)){
            $btn->setIcon($iconBtn);
        }
        if (!is_null($colorBtn)){
            $btn->setColor($colorBtn);
        } else {
            if (!is_null($action)){
                $btn->setColor(self::COLOR_BY_ACTION[$action]);
            }
        }
        return $btn;
    }

    public static function CreateStandartButton(string $action, string $type, string $text=''){
        $colorBtn = self::COLOR_BLUE;
        $iconBtn = Icons::SHOW;
        switch ($type){
            case 'show':
                $colorBtn = self::COLOR_BLUE;
                $iconBtn = Icons::SHOW;
                break;
            case 'edit':
                $colorBtn = self::COLOR_GREEN;
                $iconBtn = Icons::EDIT;
                break;
            case 'send':
                $colorBtn = self::COLOR_BLUE_LIGHT;
                $iconBtn = Icons::SEND;
                break;
            case 'user':
                $colorBtn = self::COLOR_YELLOW;
                $iconBtn = Icons::USER;
                break;
            case 'delete':
                $colorBtn = self::COLOR_RED;
                $iconBtn = Icons::DELETE;
                break;
            case 'close':
                $colorBtn = self::COLOR_YELLOW;
                $iconBtn = Icons::CLOSE;
                break;
            case 'ban':
                $colorBtn = self::COLOR_RED;
                $iconBtn = Icons::BAN;
                break;
            case 'money':
                $colorBtn = self::COLOR_BLUE_LIGHT;
                $iconBtn = Icons::MONEY;
                break;
            case 'discount':
                $colorBtn = self::COLOR_YELLOW;
                $iconBtn = Icons::PERCENT;
                break;
        }


        return Button::Create()->setAction($action)->setText($text)->setIcon($iconBtn)->setColor($colorBtn);
    }



    /**
     * @param string $href
     * @return Button
     * Таблица в БД
     */
    public function setHref(string $href)
    {
        $this->href = $href;
        return $this;
    }

    /**
     * @param int $blanc
     * @return Button
     */
    public function newTabOpen(int $blanc)
    {
        $this->_blanc = $blanc;
        return $this;
    }

    /**
     * @param string $icon
     * @return Button
     */
    public function setIcon(string $icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @param string $color
     * @return Button
     */
    public function setColor(string $color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @param string $text
     * @return Button
     */
    public function setText(string $text)
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return Button
     */
    public function isHeader()
    {
        $this->clas = 'to_modal_form_btn';
        return $this;
    }

    /**
     * @param string $action
     * @return Button
     */
    public function setAction(string $action)
    {
        $this->action = $action;
        return $this;
    }



    /**
     * @param string $table
     * @return Button
     * Таблица в БД
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }


    /**
     * для быыстрых кнопок
     * @param string $msg
     * @return Button
     */
    public function setMsg(string $msg)
    {
        $this->msg = $msg;
        return $this;
    }

    /**
     * @param string $table_parent
     * @return Button
     */
    public function setTableParent(string $table_parent): Button
    {
        $this->table_parent = $table_parent;
        return $this;
    }

    /**
     * @param string $server_action
     * @return Button
     */
    public function setServerAction(string $server_action)
    {
        $this->server_action = $server_action;
        return $this;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function onClick(string $text){
        $this->onclick = $text;
        return $this;
    }

    /**
     * @param string $url_copy
     * @return Button
     */
    public function setDataCopy(string $url_copy)
    {
        $this->url_copy = base64_encode($url_copy);
        return $this;
    }

    function toArray(){
        return (array) $this;
    }
}