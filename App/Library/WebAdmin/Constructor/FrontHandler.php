<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 11.07.2020
 * Time: 15:46
 */

namespace App\Library\WebAdmin\Constructor;


class FrontHandler
{

    public $function='';
    public $data=null;

    public function __construct(string $function, $data=[])
    {
        $this->function = $function;
        $this->data = $data;
    }

    function toArray(){
        return (array) $this;
    }
}