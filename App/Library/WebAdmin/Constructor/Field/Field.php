<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:36
 */

namespace App\Library\WebAdmin\Constructor\Field;



use App\Library\WebAdmin\Constructor\Button;

class Field
{
    public $input_type='';
    public $name='';

    public $help='';//&#013; перенос строки
    public $input;
    public $show = 1;

    public static function Create(string $name, BaseInput $input, int $show=1) : self
    {
        $obj = new Field();
        $obj->name = $name;
        $obj->input = $input;
        $obj->show = $show;
        //print_log(__FILE__, __LINE__, $name);
        //print_log(__FILE__, __LINE__, $input instanceof GeoAddress);
        //input_type

        if ($input instanceof Button){
            $obj->input_type = 'button';
        } else if ($input instanceof Textarea){
            $obj->input_type = 'textarea';
        } else if ($input instanceof Select){
            $obj->input_type = 'select';
        } else if ($input instanceof InputFile){
            $obj->input_type = 'file';
        } else if ($input instanceof GeoAddress){
            $obj->input_type = 'geoaddress';
        } else if ($input instanceof InputColor){
            $obj->input_type = 'color';
        } else if ($input instanceof InputIntervalTime){
            $obj->input_type = 'interval_time';
        } else if ($input instanceof InputIntervalDate){
            $obj->input_type = 'interval_date';
        } else  if ($input instanceof InputHTML){
            $obj->input_type = 'html';
        } else{
            $obj->input_type = 'input';
        }

        return $obj;
    }


    public static function AutoCreateFields(array $defaultValues) : array
    {
        $fields = [];
        foreach ($defaultValues as $field=>$value){
            if(is_string($value)){
                $input = InputText::Create();
            } else if($value==0 || $value==1) {
                $input = InputCheckbox::Create();
            } else {
                $input = InputNumber::Create();
            }
            $input->setField($field);
            $fields[] = Field::Create($field, $input->setValue($value));
        }
        return $fields;
    }


    //пробел &#013; - перенос строки
    public function setHelp(string $help)
    {
        $this->help = $help;
        return $this;
    }
}