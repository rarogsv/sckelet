<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 17:19
 */

namespace App\Library\WebAdmin\Constructor\Field;


class Select extends BaseInput
{
    public $multiple=false;
    public $size=1;
    public $options=[];
    public $addble = 0;
    public $selects = [];


    /**
     * @param $name
     * @param $field
     * @param null $inputType
     * @param null $inputValue
     * @return $this
     */
    public function addSelect($name, $field, $delete=true, $inputField = null, $inputValue=null)
    {
        $this->addble = 1;
        $select = [
            'name' => $name,
            'field' => $field,
            'delete'=> intval($delete)
        ];
        if ($inputValue!=null && $inputField!=null){
            $select['input'] = [
                'field' => $inputField,
                'value' => $inputValue,
            ];
        }
        $this->selects[] = $select;
        return $this;
    }

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::SELECT;
    }


    public static function Create($field='', $value='') : self
    {
        return new Select($field, $value);
    }


    /**
     * @param $value
     * @param string $text
     * @param null $add //single, multiple
     * @return $this
     */
    public function addOption($value, string $text, $add=null)
    {
        $o = [
            'value' => $value,
            'text' => $text
        ];
        if (!is_null($add)){
            $o['add'] = $add;
        }
        $this->options[]= $o;
        return $this;
    }


    /**
     * @param bool $multiple
     * @return Select
     */
    public function setMultiple(bool $multiple)
    {
        $this->multiple = $multiple;
        return $this;
    }

    /**
     * @param int $size
     * @return Select
     */
    public function setSize(int $size)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @param array $options
     * @return Select
     */
    public function setOptions(array $options)
    {
        $this->options = $options;
        return $this;
    }



}