<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class InputIntervalDate extends BaseInput
{
    //const PATTERN_EMAIL = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$';

    public $value2 = '';

    public function __construct($field, $value, $value2)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->value2 = $value2;
        $this->type = self::DATE_INTERVAL;
    }
    public static function Create($field = '', $value, $value2) :self
    {
        return new InputIntervalDate($field, $value, $value2);

    }



}