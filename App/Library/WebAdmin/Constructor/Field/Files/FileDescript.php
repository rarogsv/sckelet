<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 09.07.2020
 * Time: 12:48
 */

namespace App\Library\WebAdmin\Constructor\Field\Files;

class FileDescript
{
    //name in DB
    public $name='';

    public $url_icon='';

    public $clas='';

    public function __construct(string  $n, string $u, string $c='scan')
    {
        $this->name = $n;
        $this->url_icon = $u;
        $this->clas = $c;
    }

    public static function Create(string  $n, string $u, string $c='scan'){
        return new FileDescript($n, $u, $c);
    }



}