<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 05.06.2020
 * Time: 14:22
 */

namespace App\Library\WebAdmin\Constructor\Field;


class GraphicData
{
    const COLOR_RED = 'rgba(255, 99, 132, 1)';
    const COLOR_BLUE = 'rgba(54, 162, 235, 1)';
    const COLOR_GREEN = 'rgba(100, 100, 100, 1)';
    const COLOR_GREEN2 = 'rgba(255, 206, 86, 1)';
    const COLOR_GREEN3 = 'rgba(75, 192, 192, 1)';
    const COLOR_GREEN4 = 'rgba(255, 159, 64, 1)';



    public $label = '';
    public $data=[];
    public $borderWidth = 1;
    public $fill = false;

    public $borderColor=[];
    public $backgroundColor=[];


    public static function Create() : self
    {
        return new GraphicData();
    }


    public function setLabel(string $label)
    {
        $this->label = $label;
        return $this;
    }


    public function setFill(int $fill)
    {
        $this->fill = $fill;
        return $this;
    }

    public function setBorderWidth(int $borderWidth)
    {
        $this->borderWidth = $borderWidth;
        return $this;
    }

    public function setData($data, $backgroundColor, $borderColor)
    {
        $this->data[] = $data;
        $this->backgroundColor[] = $backgroundColor;
        $this->borderColor[] = $borderColor;
        return $this;
    }

}