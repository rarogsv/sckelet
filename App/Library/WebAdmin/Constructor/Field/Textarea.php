<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class Textarea extends BaseInput
{

    public $count_rows = 5;
    public $max = 0;

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = BaseInput::TEXTAREA;
    }


    public static function Create($field='', $value='') :self
    {
        return new Textarea($field, $value);
    }

    /**
     * @param int $max
     * @return Textarea
     */
    public function setMax(int $max)
    {
        $this->max = $max;
        return $this;
    }


    /**
     * @param int $count_rows
     */
    public function setCountRows(int $count_rows)
    {
        $this->count_rows = $count_rows;
        return $this;
    }

}