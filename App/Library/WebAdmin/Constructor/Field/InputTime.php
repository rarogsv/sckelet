<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class InputTime extends BaseInput
{
    //const PATTERN_EMAIL = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$';

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::TIME;
    }
    public static function Create($field = '', $value) :self
    {
        return new InputTime($field, $value);

    }



}