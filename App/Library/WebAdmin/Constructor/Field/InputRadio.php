<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;




class InputRadio extends BaseInput
{

    public $data = [];

    public function __construct($field, $value, $data)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;

        $this->type = self::RADIO;
        $this->data = $data;
    }

    public static function Create($field, $value, $data):self
    {
        return new InputRadio($field, $value, $data);
    }


}