<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:36
 */

namespace App\Library\WebAdmin\Constructor\Field;


class BaseInput
{

    const TEXTAREA  = 'textarea';
    const TEXT      = 'text';
    const PHONE     = 'tel';
    const NUMBER    = 'number';
    const FILE      = 'file';
    const DATE      = 'date';
    const COLOR     = 'color';
    const TIME      = 'time';
    const DATE_INTERVAL = 'interval_date';
    const TIME_INTERVAL = 'interval_time';
    const CHECK     = 'checkbox';
    const RADIO     = 'radio';
    const SELECT    = 'select';
    const GEO       = 'geoaddress';

    public $value='';
    public $field='';
    public $onchange='';
    public $oninput = '';

    public $required= true;
    public $disable = false;
    public $hidden  = false;
    public $autocomplete  = false;
    public $rest_autocomplete  = [];

    public $type    = '';
    public $id      = '';

    public function __construct(){
        if ($this->type == self::SELECT || $this->type == self::CHECK){
            $this->required = false;
        }
    }

    /**
     * @param string $fieldId - название ID поля
     * @param array $fieldFill - заполнить дополнительно поле
     * @param array $addParams - параметр который отправлять при запросе
     * @return $this
     */
    public function setRestAutocomplete(string $fieldId, string $fieldFill='', $addParams='')
    {
        $this->rest_autocomplete = [
            'fill_id'   => $fieldId,
            'fill'      => $fieldFill,
            'add_params'=> $addParams
        ];
        return $this;
    }


    public function setOninput(string $oninput)
    {
        $this->oninput = $oninput;
        return $this;
    }

    public function setHidden(bool $hidden)
    {
        $this->hidden = $hidden;
        return $this;
    }

    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }


    public function setRequired(bool $required)
    {
        $this->required = $required;
        return $this;
    }

    /**
     * name column in db
     */
    public function setField(string $field)
    {
        $this->field = $field;
        return $this;
    }


    public function setDisable(bool $disable) : self
    {
        $this->disable = $disable;
        return $this;
    }

    public function setOnchange(string $onchange)
    {
        $this->onchange = $onchange;
        return $this;
    }


    public function setId(string $id) : self
    {
        $this->id = $id;
        return $this;
    }


    public function setAutocomplete(bool $autocomplete)
    {
        $this->autocomplete = $autocomplete;
        return $this;
    }
}