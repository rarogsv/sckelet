<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 05.06.2020
 * Time: 14:47
 */

namespace App\Library\WebAdmin\Constructor;


use App\Library\WebAdmin\Constructor\Field\Graphic;

class GraphicPage
{
    const TYPE_LINE = 'line';

    public $graphics= [];
    public static function Create() : self
    {
        return new GraphicPage();
    }


    public function setGraphics(Graphic $graphic, string $title, string  $type=self::TYPE_LINE)
    {
        $this->graphics[] = [
            'data' => json_encode($graphic),
            'title' => $title,
            'type'=>$type
        ];
        return $this;
    }

    public function toArray()
    {
        //$this->graphics = json_encode($this->graphics);
        return (array) $this;
    }
}