<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;

class InputColor extends BaseInput
{

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::COLOR;
    }

    public static function Create($field = '', $value = '') :self
    {
        return new InputColor($field, $value);
    }

}