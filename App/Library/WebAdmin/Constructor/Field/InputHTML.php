<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class InputHTML extends BaseInput
{
    public $html='';

    public function __construct($field, $value)
    {
        parent::__construct();
    }

    public static function Create($field='', $value=''):self
    {
        return new InputHTML($field, $value);
    }

    /**
     * @param string $html
     */
    public function setHtml(string $html)
    {
        $this->html = $html;
        return $this;
    }

}