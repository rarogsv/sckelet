<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class InputNumber extends BaseInput
{
    public $pattern_input   = '';
    public $pattern         = '';
    public $min             = -1;
    public $max             = -1;
    public $step            = 0.01;
    public $placeholder     = '';


    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::NUMBER;
    }


    public static function Create($field='', $value=''):self
    {
        return new InputNumber($field, $value);
    }


    /**
     * @param string $pattern
     * @return InputNumber
     */
    public function setPattern(string $pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    /**
     * @param int $min
     * @return InputNumber
     */
    public function setMin($min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @param $max
     * @return InputNumber
     */
    public function setMax($max)
    {
        $this->max = $max;
        return $this;
    }

    public function setPatternInput(string $pattern_input)
    {
        $this->pattern_input = $pattern_input;
        return $this;
    }

    public function setPlaceholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

    public function setStep($step)
    {
        $this->step = $step;
        return $this;
    }
}