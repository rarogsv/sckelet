<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 09.07.2020
 * Time: 12:08
 */

namespace App\Library\WebAdmin\Constructor\Field;
use App\Library\WebAdmin\Constructor\Field\Files\FileDescript;

class InputFile extends BaseInput
{
    const TYPE_IMAGE = '.png, .jpg, .jpeg';
    const TYPE_DOC = 'doc';

    /**
     * _Files = {$field=>{file}} - multiple=false
     * _Files = {$field_0|1...=>{file}} - multiple=true
     *
     */

    public $accept = InputFile::TYPE_IMAGE;
    public $url = '';
    public $min_size = 5;
    public $max_size = 1000;
    public $size = '';
    public $file=null;
    public $multiple=0;

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::FILE;
    }

    public static function Create($field='', $value=''):self
    {
        return new InputFile($field, $value);
    }


    public function setAccept(string $accept): self
    {
        $this->accept = $accept;
        return $this;
    }




    public function setFile(FileDescript $file): self
    {
        $this->file = $file;
        return $this;
    }



    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }


    public function setMinSize(int $min_size): self
    {
        $this->min_size = $min_size;
        return $this;
    }


    /**
     * $max_size in kbyte
     * @param int $max_size
     * @return $this
     */
    public function setMaxSize(int $max_size): self
    {
        $this->max_size = $max_size;
        return $this;
    }

    /**
     * @param string $size
     */
    public function setSize($w, $h): self
    {
        $this->size = json_encode([$w, $h]);
        return $this;
    }

    /**
     * @param int $multiple
     */
    public function setMultiple(int $multiple): self
    {
        $this->multiple = $multiple;
        return $this;
    }

}