<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class InputDate extends BaseInput
{
    //const PATTERN_EMAIL = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$';

    public $min = '';
    public $max = '';

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::DATE;
    }
    public static function Create($field = '', $value = '') :self
    {
        return new InputDate($field, $value);
    }

    public static function Create2($field = '', $timeStamp = 0) :self
    {
        if ($timeStamp > 0){
            return new InputDate($field, date('Y-m-d', $timeStamp));
        } else {
            return new InputDate($field, '');
        }
    }

    /**
     * @param string $min
     * @return InputDate
     */
    public function setMin(string $min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @param string $max
     * @return InputDate
     */
    public function setMax(string $max)
    {
        $this->max = $max;
        return $this;
    }

}