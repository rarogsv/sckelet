<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class InputText extends BaseInput
{
    //const PATTERN_EMAIL = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$';
    const PATTERN_EMAIL = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$';
    public $pattern_input = '';
    public $pattern = '';
    public $min = -1;
    public $max = -1;
    public $placeholder = '';

    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = self::TEXT;
    }


    public static function Create($field='', $value='') :self
    {
        return new InputText($field, $value);
    }





    /**
     * @param int $min
     * @return InputText
     */
    public function setMin(int $min)
    {
        $this->min = $min;
        return $this;
    }

    /**
     * @param int $max
     * @return InputText
     */
    public function setMax(int $max)
    {
        $this->max = $max;
        return $this;
    }

    public function setPatternInput(string $pattern_input)
    {
        $this->pattern_input = $pattern_input;
        return $this;
    }
    /**
     * @param string $pattern
     * @return InputText
     */
    public function setPattern(string $pattern)
    {
        $this->pattern = $pattern;
        return $this;
    }

    public function setPlaceholder(string $placeholder)
    {
        $this->placeholder = $placeholder;
        return $this;
    }

}