<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 15.05.2020
 * Time: 14:56
 */

namespace App\Library\WebAdmin\Constructor\Field;


class GeoAddress extends BaseInput
{
    public $lat = 48.45466;
    public $lng = 30.5238;
    public $isDynamic = 0;//не используется


    public function __construct($field, $value)
    {
        parent::__construct();
        $this->field = $field;
        $this->value = $value;
        $this->type = 'geoaddress';
    }

    public static function Create($field='', $value='') : self
    {
        return new GeoAddress($field, $value);
    }


    public function setLat(float $lat): self
    {
        $this->lat = $lat;
        return $this;
    }


    public function setLng(float $lng): self
    {
        $this->lng = $lng;
        return $this;
    }


    public function isDynamic(bool $isDynamic): self
    {
        $this->isDynamic = $isDynamic;
        return $this;
    }



}