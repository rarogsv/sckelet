<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 05.06.2020
 * Time: 14:37
 */

namespace App\Library\WebAdmin\Constructor\Field;


class Graphic
{
    public $datasets = [];
    public $labels = [];

    public static function Create() : self
    {
        return new Graphic();
    }

    public function setLabels(array $labels)
    {
        $this->labels = $labels;
        return $this;
    }
    public function addLabels(string $label)
    {
        $this->labels[] = $label;
        return $this;
    }


    public function addDatasets(GraphicData $dataset)
    {
        $this->datasets[] = $dataset;
        return $this;
    }
}