<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.05.2020
 * Time: 22:50
 */

namespace App\Library\WebAdmin\Constructor\Window;


use App\Library\WebAdmin\Constructor\Button;
use App\Library\WebAdmin\Constructor\Field\Select;


class Table
{
    public $title = '';
    public $actionTheadName = '';
    public $headerBtn = [];
    public $headers = [];
    public $select = null;
    public $words = [];
    public $update = 0;
    public $base_url = '';
    public $hideControl = [];
    public $hideSearch = 0;
    public $hideCountPage = 0;
    public $style = '';
    //public $search = '';
    //public $offset = 1;
    //public $previous = '';
    //public $next = '';
    //public $count = 25;
    /**
     * @param string $styleTable
     */
    public function setStyleTable(string $styleTable)
    {
        $this->style = $styleTable;
    }

    public function setWords(array $words)
    {
        $this->words = $words;
        return $this;
    }

    public static function Create() : self
    {
        return new Table();
    }


    public function addBtnHeader(Button $button) : self
    {

        $this->headerBtn[] = $button;
        return $this;
    }



    public function addHeader(TableHeader $tableHeader) : self
    {
        $this->headers[] = $tableHeader;
        return $this;
    }


    public function toArray(): array
    {
        //logs(__FILE__, __LINE__, $activity);

        return ['table' => (array) $this];
    }

    /**
     * @param string $actionTheadName
     * @return Table
     */
    public function setActionTheadName(string $actionTheadName) : self
    {
        $this->actionTheadName = $actionTheadName;
        return $this;
    }

    /**
     * @param array $headerBtn
     * @return Table
     */
    public function setHeaderBtn(array $headerBtn) : self
    {
        $this->headerBtn = $headerBtn;
        return $this;
    }

    /**
     * @param string $title
     * @return Table
     */
    public function setTitle(string $title) : self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @param string $title
     * @return Table
     */
    public function setUpdate(int $update) : self
    {
        $this->update = $update;
        return $this;
    }

    /**
     * @param $select
     * @return Table
     */
    public function setSelect(Select $select) : self
    {
        $this->select = $select;
        return $this;
    }

}