<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 11.05.2020
 * Time: 19:41
 */

namespace App\Library\WebAdmin\Constructor\Window;


class BaseWebConstructor
{
    public $words = [];


    public function setWords(array $words)
    {
        $this->words = $words;
        return $this;
    }
}