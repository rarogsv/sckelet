<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 08.05.2020
 * Time: 0:19
 */

namespace App\Library\WebAdmin\Constructor\Window;


use App\Library\WebAdmin\Constructor\Button;
use App\Library\WebAdmin\Constructor\Field\Field;

class Editor
{
    public $title='';
    public $words=[];
    public $columns=[];
    public $id = 0;
    public $table = -1;
    public $actionsBtn = [];
    public $onload = '';

    public function &getActionBtn($def=0) : Button
    {
        return $this->actionsBtn[$def];
    }

    /**
     * @param array $words
     */
    public function setWords(array $words): Editor
    {
        $this->words = $words;
        return $this;
    }
    /**
     * @param string $table
     * @return Editor
     */
    public function setTable(string $table)
    {
        $this->table = $table;
        return $this;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }
    public function addBtnAction(Button $btn){
        $this->actionsBtn[] = $btn;
        return $this;
    }



    /**
     * @param Field $field
     * @return Editor
     */
    public function addColumn(Field $field)
    {
        $this->columns[] = $field;
        return $this;
    }

    /**
     * @param array $columns
     * @return Editor
     */
    public function setColumns(array $columns)
    {
        foreach ($columns as $column){
            $this->columns[] = $column;
        }
        return $this;
    }

    public function toArray(){
        return ['editor'=>(array) $this];
    }

    /**
     * @param string $onload
     */
    public function setOnload(string $onload)
    {
        $this->onload = $onload;
        return $this;
    }

}