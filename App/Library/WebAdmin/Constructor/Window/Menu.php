<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 18.05.2020
 * Time: 16:37
 */

namespace App\Library\WebAdmin\Constructor\Window;


class Menu
{


    public $table = '';
    public $name = '';
    public $href = '';

    public $icon = '';
    public $subMenu=[];


    public static function Create(string $name, string $table, string $icon, string $href){
        return new Menu($name, $table, $icon, $href);
    }

    public function __construct(string $name, string $table, string $icon, string $href)
    {
        $this->name = $name;
        $this->table=$table;
        $this->icon=$icon;
        $this->href=$href;
    }


    public function addSubMenu(string $name, string $table, string $href)
    {
        $this->subMenu[] = ['name'=>$name, 'table'=>$table, 'href'=>$href];
        return $this;
    }


    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }



    public function setIcon(string $icon)
    {
        $this->icon = $icon;
        return $this;
    }

    /**
     * @param string $href
     * @return Menu
     */
    public function setHref(string $href)
    {
        $this->href = $href;
        return $this;
    }
}