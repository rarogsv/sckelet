<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.05.2020
 * Time: 22:50
 */

namespace App\Library\WebAdmin\Constructor\Window;


use App\Library\WebAdmin\Constructor\Button;
use http\Header;

class TableContent
{
    const AS_IT_IS      = 0;
    const DATE          = 1;
    const BOOLEAN       = 2;
    const MONEY         = 3;
    const LINK          = 4;
    const BUTTON        = 5;
    const IMG           = 6;
    const COUNT         = 7;
    const CALLBACK      = 8;
    const RATING        = 9;
    const EMAIL         = 10;
    const TEL           = 11;
    const AS_IT_SHORT   = 12;


    public $actionsBtn = [];
    public $rows = [];


    public function addBtnAction(Button $btn){
        $this->actionsBtn[] = $btn;
        return $this;
    }


    public function addRow(array $columns, int $id, $color='', array $actionsBtn = [])
    {
        $this->rows[] = [
            'id' => $id,
            'columns' => $columns,
            'color' => $color,
            'actionsBtn'=>$actionsBtn
        ];
        return $this;
    }

    /**
     * @param array $rows
     * @param $header
     * @return $this
     * если совпадает название в
     */
    public function addRows(array $rows)
    {
        $this->rows = $rows;
        return $this;
    }


    public function toArray()
    {
        //print_log(__FILE__, __LINE__, $activity);

        return ['table' => (array) $this];
    }

}