<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 24.05.2020
 * Time: 18:24
 */

namespace App\Library\WebAdmin\Constructor\Window;


class TableHeader
{

    public $shortcut = '';
    public $sort = 1;
    public $colspan = 1;
    public $column = '';
    public $vertical = 0;


    public static function Create(){
        return new TableHeader();
    }

    /**
     * @param string $shortcut
     * @return TableHeader
     */
    public function setShortcut(string $shortcut) : self
    {
        $this->shortcut = $shortcut;
        return $this;
    }


    public function setSort($sort) : self
    {
        $this->sort = (int)$sort;
        return $this;
    }

    /**
     * @param string $column
     * @return TableHeader
     */
    public function setColumn(string $column) : self
    {
        $this->column = $column;
        return $this;
    }

    /**
     * @param int $colspan
     * @return TableHeader
     */
    public function setColspan(int $colspan) : self
    {
        $this->colspan = $colspan;
        return $this;
    }

    /**
     * @param int $vertical
     * @return TableHeader
     */
    public function setVertical(int $vertical) : self
    {
        $this->vertical = $vertical;
        return $this;
    }

}