<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 14.05.2020
 * Time: 16:22
 */

namespace App\Library\WebAdmin\Constructor;


use App\Library\Scelet\ControllersBase\Base\BaseWebController;


class FormEdit
{
    public $controller;
    protected $editor;
    public $table;//назв таблица
    public $id=0;//id строки
    public $db;
    public $badData = false;
    public $comment = '';


    public function __construct(BaseWebController $controller)
    {
        //logs(__FILE__, __LINE__, 0);
        $this->controller = $controller;
        $this->table = $this->controller->getRequestPost()['table'];
        $this->id = $this->controller->getCastFieldPost('id', INT, 0);
        $this->db = $controller->db;
        logs(__FILE__, __LINE__, $this->table);
    }


    protected function end(){
        //logs(__FILE__, __LINE__, 'end');
        if ($this->badData){
            $this->controller->getResponse()->badData(
                $this->controller->getTextShortcut($this->comment)
            );
        } else {
            $this->controller->getResponse()->json();
        }
    }

    protected function getMultiUploadList($baseKey){
        $post = $this->controller->getRequestPost();
        $res= [];
        $selectKey = $baseKey.'_select_';
        $countKey = $baseKey.'_count_';

        foreach ($post as $key=>$val){
            if (strpos($key, $selectKey) !== false){
                $itemAdd=[$val];
                if (isset($post[$countKey.$val])){
                    if ($post[$countKey.$val] > 0){
                        $itemAdd[]=$post[$countKey.$val];
                    }
                }
                $res[]=$itemAdd;
            }
        }
        return $res;
    }

}