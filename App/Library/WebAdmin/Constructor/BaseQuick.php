<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 19.12.2022
 * Time: 14:04
 */

namespace App\Library\WebAdmin\Constructor;

use App\Library\Scelet\ControllersBase\Base\BaseWebController;
use App\Library\Scelet\DataBase\PDORequest;

class BaseQuick
{
    public $db;

    public $id=0;
    public $table='';
    public $serverAction='';

    public $badData = false;
    public $comment = '';

    public function __construct(BaseWebController $controller)
    {
        /*$data = $controller->getCastDataPost([
            ['id',      INT, 0],
            ['table',   STRING],
            ['server_action', STRING],
        ]);*/

        $this->db = PDORequest::Create();
        $this->id = $controller->getCastFieldPost('id', INT, 0);
        $this->table = $controller->getCastFieldPost('table', STRING, '');
        $this->serverAction = $controller->getCastFieldPost('server_action', STRING, '');
    }

}