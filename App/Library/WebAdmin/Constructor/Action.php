<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 14.05.2020
 * Time: 16:22
 */

namespace App\Library\WebAdmin\Constructor;


class Action
{
    public $variable='';
    public $function='';
    public $params=[];

    public static function Create() : self
    {
        return new Action();
    }

    /**
     * @param string $variable
     * @return Action
     */
    public function setVariable(string $variable)
    {
        $this->variable = $variable;
        return $this;
    }

    /**
     * @param string $function
     * @return Action
     */
    public function setFunction(string $function)
    {
        $this->function = $function;
        return $this;
    }

    /**
     * @param array $params
     * @return Action
     */
    public function setParams(array $params)
    {
        $this->params = $params;
        return $this;
    }



}