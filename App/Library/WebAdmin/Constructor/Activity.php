<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 06.05.2020
 * Time: 12:16
 */

namespace App\Library\WebAdmin\Constructor;

use App\Library\WebAdmin\Constructor\Window\Editor;
use App\Library\WebAdmin\Constructor\Window\Menu;
use App\Library\WebAdmin\Constructor\Window\Table;

class Activity
{
    const DEFAULT_LOGO = '/disp/img/logo.ico';

    const LOAD_TABLE    =   'table';
    const LOAD_EDITOR   =   'editor';
    const LOAD_GRAPH    =   'graph';

    public $url;
    public $api='';
    public $page = [
        'name'=>'web',
        'icon'=> ''
    ];


    public $user    = ['name' => '', 'avatar' => '/disp/img/avatar.png', 'post' => ''];
    public $keys    = [];
    public $menu    = [];
    public $info    = [];
    public $word    = [];
    public $lib     = [];
    public $data    = [];

    //public $words_string = '{}';
    public $hasPushNotification = 0;
    public $table;
    public $editor;
    public $graph;


    public $language    = DEFAULT_LANGUAGE;
    public $languages   = LANGUAGES;
    public $classBody   = Activity::LOAD_TABLE;

    public function __construct()
    {
        //$this->languages = json_encode(LANGUAGES);
    }

    /**
     * @param string $name
     * @param string $icon
     * @return Activity
     */
    public function setPage(string $name, string $icon=self::DEFAULT_LOGO): Activity
    {
        $this->page = [
            'name'=>$name,
            'icon'=> $icon
        ];
        return $this;
    }

    /**
     * @param array $keys  gmap/
     */
    public function setKeys(array $keys)
    {
        $this->keys = $keys;
    }

    public function setUser(string $name, string $avatar, string $post): Activity
    {
        $this->user = [
            'name' => $name,
            'avatar' => $avatar,
            'post' => $post,
        ];
        return $this;
    }

    /**
     * @param string $api
     * @return Activity
     */
    public function setApi(string $api): Activity
    {
        $this->api = $api;
        return $this;
    }

    /**
     * @param array $info
     */
    public function setInfo(array $info)
    {
        $this->info = $info;
    }


    public function setWords(array $word) : self
    {
        $this->word = $word;
        return $this;
    }

    public function addWords(array $word) : self
    {
        $this->word = array_merge($word, $this->word);
        return $this;
    }

    public function toArray()
    {
        //print_log(__FILE__, __LINE__, $activity);
        return ['activity' => (array) $this];
    }


    public function addMenuItem(Menu $menu) : self
    {
        //print_log(__FILE__, __LINE__, $this->translate);
        //print_log(__FILE__, __LINE__, $menu->table);
        //print_log(__FILE__, __LINE__, $this->translate[$menu->table]);
        //$menu->setName($this->translate[$menu->table]);
        $this->menu[] = $menu;
        return $this;
    }



    public function setHasPushNotification(bool $hasPushNotification): self
    {
        $this->hasPushNotification = (int)$hasPushNotification;
        return $this;
    }


    /**
     * @param mixed $table
     * @return Activity
     */
    public function setTable(Table $table)
    {
        $this->table = $table;
        return $this;
    }

    /**
     * @param mixed $editor
     * @return Activity
     */
    public function setEditor(Editor $editor)
    {
        $this->editor = $editor;
        $this->classBody = Activity::LOAD_EDITOR;
        return $this;
    }

    public function setClassBody(string $classLoad)
    {
        $this->classBody = $classLoad;
        return $this;
    }


    public function setLib(string $lib)
    {
        $this->lib[] = $lib;
        return $this;
    }

    public function setData($key, $val)
    {
        $this->data[$key] = $val;
        return $this;
    }

}