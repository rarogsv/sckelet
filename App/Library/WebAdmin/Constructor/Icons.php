<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 18.05.2020
 * Time: 16:47
 */

namespace App\Library\WebAdmin\Constructor;


class Icons
{

    const START     = 'fa fa-flag-o';
    const MAIN      = 'fa fa-bookmark';
    const USER      = 'fa-user';
    const UPDATE    = 'fa-cloud-upload';
    const USERS     = 'fa-users';
    const CODE      = 'fa-code';
    const PLACE     = 'fa-university';
    const GRAPHICS  = 'fa-bar-chart';
    const REFRESH   = 'fa-refresh';
    const REPLY     = 'fa-reply-all';
    const SETTING   = 'fa-cog';
    const SETTING_CODE = 'fa-code';
    const ORDER     = 'fa-briefcase';
    const SERVICES  = 'fa-database';
    const SERVER    = 'fa-server';
    const BANK      = 'fa-bank';
    const COMPLAIN      = 'fa-flag';


    const CREATE = 'fa-file';



    const SHOW = 'fa-list-alt';
    const EDIT = 'fa-edit';
    const SEND = 'fa-send';
    const DELETE = 'fa-trash';
    const CLOSE = 'fa-close';
    const BAN = 'fa-ban';
    const MONEY = 'fa-money';
    const PERCENT = 'fa-percent';
    const BACK = 'fa-arrow-left';
    const FIRE = 'fa-fire';
    const LINK = 'fa-external-link';
    const CALENDAR = 'fa fa-calendar';
    const MOBILE = 'fa fa-mobile';
    const HISTORY = 'fa fa-history';
    const COPY = 'fa fa-clone';
    const CHECK = 'fa fa-check';
    const COMMENT = 'fa fa-comment-o';
    const SHOPING = 'fa fa-shopping-cart';

}