<?php
/**
 * Created by PhpStorm.
 * User: rarog
 * Date: 14.05.2020
 * Time: 16:22
 */

namespace App\Library\WebAdmin\Constructor;


use App\Library\Scelet\ControllersBase\Base\BaseWebController;
use App\Library\Scelet\ModelBase\BaseDBMap;
use App\Library\WebAdmin\Constructor\Field\InputText;
use App\Library\WebAdmin\Constructor\Field\Textarea;
use App\Library\WebAdmin\Constructor\Field\InputCheckbox;
use App\Library\WebAdmin\Constructor\Field\Field;
use App\Library\Scelet\ModelBase\Servers;


class BaseSAdminFormCreator extends FormCreator
{

    public function __construct(BaseWebController $controller)
    {
        parent::__construct($controller);

        switch ($this->table){
            case BaseDBMap::SERVER:
                $this->getEditorServer();
                break;
        }
    }

    protected function getEditorServer(){
        if (isset($this->controller->getRequestPost()['server_action']) && $this->controller->getRequestPost()['server_action'] == 'reply'){
            $this->editorVersion();
        } else {
            $this->editorServer();
        }
    }

    protected function editorVersion()
    {
        $this->editor->addColumn(
            $this->getField(InputText::Create('server_action', 'reply')->setHidden(true),'')
        );

        $this->editor->addColumn(Field::Create('hash', InputText::Create('git_hash', '')));
    }

    protected function editorServer(){

        if ($this->id > 0){
            $obj= Servers::Load($this->id);
        } else {
            $obj = new Servers();
        }

        $this->editor->addColumn(
            $this->getField(InputText::Create('server_action', 'editor')->setHidden(true),'')
        );

        $this->editor->addColumn(
            $this->getField(InputText::Create('ip', $obj->ip),'ip')
        );

        $this->editor->addColumn(
            $this->getField(Textarea::Create('comment', $obj->comment)->setMax(250)->setRequired(false),'comment')
        );
        $this->editor->addColumn(
            $this->getField(InputCheckbox::Create('enable', $obj->enable),'enable')
        );
    }



}