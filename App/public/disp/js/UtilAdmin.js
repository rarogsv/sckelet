/*
 window.onerror = function(error, url, line) {
    console.log(error)
     //alert(error+'----'+url+'----'+line);

     //controller.sendLog({acc:'error', data:'ERR:'+error+' URL:'+url+' L:'+line});
 };
*/
var TIME_ZONE = (new Date()).getTimezoneOffset()*60;
var USER_LANG = window.navigator.language.split('-')[1];


if (langSupport.indexOf(USER_LANG) == -1){
    USER_LANG = DEFAULT_LNG;
}

var DomHandler = {
    showGroup:function(isShow, hide, show){
        //console.log(isShow);
        //console.log(hide);
        //console.log(show);

        if (isShow){
            for (var i = 0; i < hide.length; i++) {
                var shGrQ = '[name='+hide[i]+']';
                //console.log(shGrQ);
                //console.log(document.querySelector(shGrQ) == null);
                //console.log(document.querySelector(shGrQ).closest('div.form-group') == null);
                var el =  document.querySelector(shGrQ).closest('div.form-group');
                if (el.style.display == 'none')
                    el.style.removeProperty('display');
            }
            for (var i = 0; i < show.length; i++) {
                var hiGrQ = '[name='+show[i]+']';
                document.querySelector(hiGrQ).closest('div.form-group').style.display = 'none';
            }
        } else {
            for (var i = 0; i < show.length; i++) {
                var hiGrQ = '[name='+show[i]+']';
                var el =  document.querySelector(hiGrQ).closest('div.form-group');
                //console.log(hiGrQ);
                //console.log(el == null);
                //console.log(el.style.display);
                //console.log(el.style.display == 'none');
                if (el.style.display == 'none')
                    el.style.removeProperty('display');
            }

            for (var i = 0; i < hide.length; i++) {
                var shGrQ = '[name='+hide[i]+']';
                document.querySelector(shGrQ).closest('div.form-group').style.display = 'none';
            }
        }
    },

    targetClick : function(){
        return  window.event.target;
    },

    classTransition : function(el, clas, child, classChild, func) {
        //console.log('1-'+el==null);
        //console.log('2-'+el.classList.contains(clas));

        if (Array.isArray(clas)) {
            if (el.classList.contains(clas[0])) {
                //console.log('31-'+clas);
                el.classList.remove(clas[0]);
                el.classList.add(clas[1]);
            } else {
                //console.log('32-'+clas);
                el.classList.add(clas[0]);
                el.classList.remove(clas[1]);
            }
        } else {
            if (el.classList.contains(clas)) {
                //console.log('31-'+clas);
                el.classList.remove(clas);
            } else {
                //console.log('32-'+clas);
                el.classList.add(clas);
            }
        }

        if (child !== null && !isUndefined(child)) {
            //console.log('4-child');
            DomHandler.classTransition(child, classChild);
        }

        if (!isUndefined(func)){
            func();
        }
    },

    hiden : function(el){
        //console.log(el.style.display)
        if (el.style.display == 'none'){
            el.style.display = 'block';
        } else {
            el.style.display = 'none';
        }

    }
};


var UtilAdmin = {

    getTimezoneOffset:function(ts){
        return (new Date(ts*1000)).getTimezoneOffset()*60;
    },

    arrayColumn:function (arr, key) {
        return arr.map(function (a) {
            return a[key];
        });
    },

    getArrayColumn:function(arr, key){
        var res = [];
        for (var i = 0; i < arr.length; i++) {
            res.push(arr[i][key]);
        }
        return res;
    },

    sortObj:function(array, key, to){
        if (to == 'ASC'){
            return array.sort(function(a, b) {
                var x = a[key];
                var y = b[key];
                return ((x < y) ? -1 : ((x > y) ? 1 : 0));
            });
        } else {
            return array.sort(function(a, b) {
                var x = a[key];
                var y = b[key];
                return ((x > y) ? -1 : ((x < y) ? 1 : 0));
            });
        }
    },

    getGeo:function(funcOk, funcErr) {
        var geolocation = new Promise(() => {
            navigator.geolocation.getCurrentPosition(
                position => { funcOk(position.coords)},
                error => { funcErr(error) }
            )
        }).catch(error => funcErr(error))
    },

    getLangUser:function () {
      return navigator.language.substr(0, 2).toUpperCase();
    },
    
    countdownTimer : function(stop, step, eventDuration, eventStop){
        stop = Math.round(stop);

        if (stop <= 0){
            eventStop();
            return;
        }

        var timer = setInterval(function(){
            stop--;
            eventDuration(stop);

            if (stop == 0){
                eventStop();
                clearInterval(timer);
            }
        }, duration*1000);

    },

    createUrl:function(url, data){
        if (!isUndefined(data) && data !== null){
            var str = '?';
            for (var key in data) {
                if (Array.isArray(data[key])){
                    data[key] = JSON.stringify(data[key]);
                }
                str += key + "=" + data[key]+'&';
            }
            url += str;
        }
        return url;
    },



    getParamsURL:function(key){
        //var url = new URL(window.location.href);
        //return url.searchParams.get(key);
        //console.log(key);
        key = key.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        var regex = new RegExp('[\\?&]' + key + '=([^&#]*)');
        var results = regex.exec(location.search);
        var res = results === null ? null : decodeURIComponent(results[1].replace(/\+/g, ' '));
        //console.log(res);
        return res;
    },

    getParamsURLObj:function(){
        return Object.fromEntries(new URLSearchParams(location.search));
    },

    requestWakeLock:function () {
        try {
            var wakeLock = navigator.wakeLock.request('screen');

        } catch (err) {
            // The wake lock request fails - usually system-related, such as low battery.
            //console.log(`${err.name}, ${err.message}`);
        }
    },

    getObject:function(obj, key){
        var data = {};
        for (var i = 0; i < key.length; i++) {
            data[key[i]] = obj[key[i]];
        }
        return data;
    },

    formatNumber:function(num, length) {
        var r = "" + num;
        while (r.length < length) {
            r = "0" + r;
        }
        return r;
    },

    checkAdBlock:function(func) {
        var testAd = document.createElement('div');
        testAd.innerHTML = '&nbsp;';
        testAd.className = 'adsbox';
        document.body.appendChild(testAd);
        window.setTimeout(function() {
            //console.log(1);
            if (testAd.offsetHeight === 0) {
                //console.log(2);
                func();
            }
            testAd.remove();
        }, 100);
    },

    copyToClipboard:function(text) {
        if (window.clipboardData && window.clipboardData.setData) {
            // IE specific code path to prevent textarea being shown while dialog is visible.
            return clipboardData.setData("Text", text);

        } else if (document.queryCommandSupported && document.queryCommandSupported("copy")) {
            var textarea = document.createElement("textarea");
            textarea.textContent = text;
            textarea.style.position = "fixed";  // Prevent scrolling to bottom of page in MS Edge.
            document.body.appendChild(textarea);
            textarea.select();
            try {
                return document.execCommand("copy");  // Security exception may be thrown by some browsers.
            } catch (ex) {
                console.warn("Copy to clipboard failed.", ex);
                return false;
            } finally {
                document.body.removeChild(textarea);
                Notifications.toast('Copy')
            }
        }
    },

    /**
     * @param arr array
     * @param search obj key/value
     * @returns array
     */
    subArray:function(arr, search){
        var res = [];
        for (var i = 0; i < arr.length; i++) {
            var isFind = true;
            for (var key in search){
                isFind = isFind && search[key] == arr[i][key];
            }
            if (isFind){
                res.push(arr[i]);
            }
        }
        return res;
    },

    indexOf:function(arr, search){
        for (var i = 0; i < arr.length; i++) {
            var isFind = true;
            for (var key in search){
                isFind = isFind && search[key] == arr[i][key];
            }
            if (isFind){
                return i;
            }
        }
        return -1;
    },

    search:function(arr, search){
        for (var i = 0; i < arr.length; i++) {
            var isFind = true;
            for (var key in search){
                isFind = isFind && search[key] == arr[i][key];
            }
            if (isFind){
                return arr[i];
            }
        }
        return null;
    },

    resize:function (imgFile, MAX_WIDTH, MAX_HEIGHT, func) {
    //var MAX_WIDTH = 800;
    //var MAX_HEIGHT = 600;
    var reader = new FileReader();
    reader.onload = function(e) {
        var canvas = document.createElement('canvas');
        //var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        var img = document.createElement("img");
        //var img = document.getElementById("img");
        img.src = e.target.result;
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
            var width = img.width;
            var height = img.height;
            // keep portration
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT
                }
            }
            canvas.width = width;
            canvas.height = height;
            ctx.drawImage(img, 0, 0, width, height);


            // ctx.save();
            // // move to the center of the canvas
            // ctx.translate(canvas.width/2,canvas.height/2);
            // // rotate the canvas to the specified degrees
            // ctx.rotate(90*Math.PI/180);
            // // draw the image
            // // since the context is rotated, the image will be rotated also
            // ctx.drawImage(img,-img.width/2,-img.width/2);
            // // we’re done with the rotating so restore the unrotated context
            // ctx.restore();


            /*if (os == 'android'){
                console.log('rotate');
                ctx.rotate(90 * Math.PI / 180);
            }*/

            canvas.toBlob(function (blob) {
                func(blob)
            });
        }
    };
    reader.readAsDataURL(imgFile);


},
};

var Convert = {
    boolToInt:function(data){
        if (data){
            return 1;
        } else {
            return 0;
        }
    },

    tsToTime:function (ts){
        if (ts < 1) {
            return '-';
        }
        //ts = ts - TIME_ZONE;
        var d = new Date(ts*1000);
        var times = d.toISOString().split('T')[1].split('.')[0].split(':');
        //console.log(ts);
        //console.log(d.toISOString());
        return times[0]+':'+times[1];
    },


    tsToDate:function (ts){
        if (ts < 1) {
            return '-';
        }
        var d = new Date((ts - TIME_ZONE)*1000);
        var dateArr = d.toISOString().split('T')[0].split('-');

        return dateArr[2]+'.'+dateArr[1]+'.'+dateArr[0];
        //return d.toISOString().split('T')[0];
    },

    tsToDateTime:function (ts){
        if (ts < 1) {
            return '-';
        }
        var d = new Date((ts - TIME_ZONE)*1000);
        return d.toISOString().split('T')[0]+' '+d.toISOString().split('T')[1].split('.')[0];
    },

    dateToTS:function (date){
        if (date.length == 0){
            return -1;
        }
        var d = new Date(date);
        return d.getTime()/1000;
    },

    tsTo:function (ts, TIME_ZONE, pattern) {
        var d = new Date((ts-TIME_ZONE)*1000);
        //console.log(d.toISOString());
        //2022-01-17T08:00:00.000Z
        var arr = d.toISOString().split('T');
        var arrDate = arr[0].split('-');
        var arrTime = arr[1].split(':');
        //console.log(d.getHours());

        var keys = ['y', 'm', 'd', 'h', 'i', 's'];
        var dataSet = '';
        // console.log(data);
        for (var i = 0; i < pattern.length; i++) {
            var letter = pattern.charAt(i);
            var add = '';
            switch (letter){
                case 'y':
                    add = arrDate[0].substr(-2);
                    break;
                case 'Y':
                    add = arrDate[0];
                    break;
                case 'm':
                    add = arrDate[1];

                    break;
                case 'd':
                    add = arrDate[2];
                    break;
                case 'h':
                    add = arrTime[0];
                    break;
                case 'i':
                    add = arrTime[1];
                    break;
                case 's':
                    add = d.getSeconds().toString();
                    break;
            }
            //console.log(add);
            //console.log(add.length);
            if (add.length === 0){
                dataSet += letter;
            } else if (add.length == 1){
                dataSet += '0'+add;
            } else {
                dataSet += add;
            }
        }

        return dataSet;
    },

    seconsTo: function(){
        var date = new Date(null);
        date.setSeconds(data); // specify value for SECONDS here
        //var result = date.toISOString().substr(11, 8); //H:i:s
        var result = date.toISOString().substr(14, 5); //i:s
        return result;
        /*return (new Date).clearTime()
            .addSeconds(data)
            .toString('mm:ss');*/
    },


    resize:function(imgFile, MAX_WIDTH, MAX_HEIGHT, func) {
    //var MAX_WIDTH = 800;
    //var MAX_HEIGHT = 600;
    var reader = new FileReader();
    reader.onload = function(e) {
        var canvas = document.createElement('canvas');
        //var canvas = document.getElementById('canvas');
        var ctx = canvas.getContext('2d');
        var img = document.createElement("img");
        //var img = document.getElementById("img");
        img.src = e.target.result;
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
            var width = img.width;
            var height = img.height;
            // keep portration
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH
                }
            } else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT
                }
            }
            canvas.width = width;
            canvas.height = height;
            ctx.drawImage(img, 0, 0, width, height);


            // ctx.save();
            // // move to the center of the canvas
            // ctx.translate(canvas.width/2,canvas.height/2);
            // // rotate the canvas to the specified degrees
            // ctx.rotate(90*Math.PI/180);
            // // draw the image
            // // since the context is rotated, the image will be rotated also
            // ctx.drawImage(img,-img.width/2,-img.width/2);
            // // we’re done with the rotating so restore the unrotated context
            // ctx.restore();


            /*if (os == 'android'){
                console.log('rotate');
                ctx.rotate(90 * Math.PI / 180);
            }*/

            canvas.toBlob(function (blob) {
                func(blob)
            });
        }
    };
    reader.readAsDataURL(imgFile);


}

};

var Storage = {

    hasKey : function(key){
        try {
            if (window.localStorage && window.localStorage.getItem(key) != null && window.localStorage.getItem(key) !== "undefined") {
                return true;
            } else {
                return false;
            }
        } catch (e) {
            return false;
        }
    },

    loadObj:function(key){
        try {
            if (window.localStorage && window.localStorage.getItem(key) != null && window.localStorage.getItem(key) !== "undefined") {
                //console.log(window.localStorage.getItem(key));
                //console.log(!isUndefined(window.localStorage.getItem(key)));
                //console.log(window.localStorage.getItem(key) === "undefined");
                //console.log(isUndefined(window.localStorage.getItem(key)));
                return JSON.parse(window.localStorage.getItem(key));
            } else {
                return null;
            }
        } catch (e) {
            return null;
        }
    },

    saveObj:function(key, obj){
        this.save(key, JSON.stringify(obj));
    },


    load:function(key, deff) {
        if (isUndefined(deff)){
            deff = null;
        }
        try {
            if (window.localStorage && window.localStorage.getItem(key) !== null && !isUndefined(window.localStorage.getItem(key))) {
                //console.log(1);
                return window.localStorage.getItem(key);
            }
        } catch (e) {
        }
        //console.log(2);
        return deff;
    },

    save:function(key, value) {
        if (window.localStorage){
            window.localStorage.setItem(key,value);
        }
    },

    clear:function(key){
        if (window.localStorage){
            window.localStorage.removeItem(key);
        }
    }
};

var Notifications = {
    toast:function(showText){
        var message = createDOM("div", {
            "id":"toast",
            "class": "show_toast"
        });

        document.body.appendChild(message);
        message.innerHTML = showText;
        setTimeout(function () {
            document.body.removeChild(message);
        }, 3000);
    },
};

function isUndefined (value){
    return String(typeof(value)) === "undefined"
}

function createDOM(dom, attribute, val){
    var el = document.createElement(dom);
    for (var key in attribute){
        el.setAttribute(key, attribute[key]);
    }

    if (!isUndefined(val)){
        el.innerHTML = val;
    }
    return el;
}

var Validation = {
    image : function(value){
        if (value == null || value.length < 4){
            return 1;
        }

        var re = value.split('.');
        if (re.length != 2){
            return 2;
        }
        if (re[1] == 'jpg' || re[1] == 'png'){
            return true;
        } else {
            return false;
        }
    },

    phone : function(value){
        value.trim();
        if (value.length>11){
            value = value.slice(0,12);
        }
        return value.replace(/[^0-9]/g, "");
    },

    price  : function(value){
        value = String(value);
        if (value.length == 0){value = 0}
        value = parseFloat(value);
        value=Math.abs(value);
        return value;
    },

    int : function(value){
        value = String(value);
        if (value.length == 0){value='0';}

        value = value.replace(/[^0-9]/g, "");

        if (value.length == 0){value='0';}
        return parseInt(value);
    },

    float  : function(value){
        var numb = "";
        value = String(value);
        if (value.length == 0){value=0;}

        for(var i=0; i<value.length; i++){
            if(value[i]!="-"){
                numb +=value[i];
            }
        }

        if (numb.length == 0){numb=0;}
        return parseFloat(numb);
    },

    coefficient  : function(value){
        value = String(value);
        if (value.length === 0){value=0;}
        value = parseFloat(value);
        value = Math.abs(value);
        if (value > 5){ value = 5; }

        return value;
    },

    name  : function(value){
        value.trim();
        return value.replace(/[^a-zA-Zа-яА-ЯёЁ]/g, "");
    },

    litteralNumb  : function(value){
        value.trim();
        return value.replace(/[^a-zA-Zа-яА-ЯёЁ0-9 ]/g, "");
    },

    text  : function(value){
        value.trim();
        return value.replace(/[^a-zA-Zа-яА-ЯёЁ0-9,.!? ]/g, "");
    },

    percent  : function(value){
        value = String(value);
        if (value.length == 0){value='0';}

        value = value.replace(/[^0-9]/g, "");

        if (value.length == 0){value='0';}
        value = Math.round(parseFloat(value));

        if(value<0){value=-1*value;}
        if(value>100){value=100;}

        return value;
    },

    email  : function(email) {
        var regex = /\S+@\S+\.\S+/;
        if (regex.test(email)){
            return true;
        }
        return false;
    }
};

var Formater  = {
    convert :function(data, format){
        var keys = ['i', 's'];
        var dataSet = '';
       // console.log(data);
        var j=0;
        for (var i = 0; i < data.length; i++) {
            if (i>=format.length){
                continue;
            }

            var letter = data.charAt(i);
            var ind = keys.indexOf(format.charAt(j));

            switch (ind){
                case 0:
                    var number = parseInt(data.charAt(i));
                    if (!isNaN(number))
                        dataSet += number;
                    break;

                case 1:
                    dataSet += data.charAt(i);
                    break;

                default:
                    if (letter != format.charAt(j)){
                        dataSet += format.charAt(j)+letter;
                        j++;
                    } else {
                        dataSet += letter;
                    }
            }

            j++;
            //console.log(format.charAt(i));
            //console.log(data.charAt(i));
            //console.log(ind);
            //console.log(dataSet);
        }

        return dataSet;
    },

    format : function(dom, format){
        var data = dom.value;
        dom.value = this.convert(data, format);
    },
};

function mRrequest(action, data, funcOk, funcErr){

    var url = URL_API+action+'/'+'?tz='+TIME_ZONE;

    //try {
    //loading from server
    console.log(url);
    //console.log(action);
    if (data == null){
        data = {};
    }
    var form_data = null;
    if (data != null){
        //console.log(this.dataRequest instanceof FormData);
        if (data instanceof FormData) {
            form_data = data;
        } else {
            form_data = new FormData();
            for (var key in data ) {
                if (!(data[key] instanceof File)&&(typeof data[key] == 'object' || Array.isArray(data[key]))){
                    form_data.append(key, JSON.stringify(data[key]));
                } else {
                    form_data.append(key, data[key]);
                }
            }
        }
    }

    var http = new XMLHttpRequest();

    http.open('POST', url, true);

    var eventErr = function(data){if (typeof(funcErr) === "function"){funcErr(data);}};
    var eventOk = function(data){if (typeof(funcOk) === "function"){funcOk(data);}};

    //Ответ
    http.onreadystatechange = function() {//Call a function when the state changes.
        http.onerror = function(){
            console.log('err1');
            eventErr(http.status);
        };

        if(http.readyState == 4 && http.status == 200) {
            //console.log("res-"+http.responseText);
            try {
                var response = JSON.parse(http.responseText);
            } catch (e) {
                //console.log(e);
                //console.log(http.responseText);
                eventOk(http.responseText);
                //console.log(0);
                return;
            }

            if (response.hasOwnProperty('callback')){
                if (typeof window[response.callback.func] == 'function'){
                    window[response.callback.func](response.callback.data);
                }
            }
            if (response.status == 200){
                eventOk(response);
            } else {
                if (response.hasOwnProperty('comment')) {
                    Notifications.toast(response.comment);
                }
                eventErr(response);
            }
        }
    };
    http.send(form_data);
}

function redir(url) {
    //console.log(url);
    document.location.href = url;
}

function reload(){
    document.location.reload();
}

function back() {
    history.back();
    document.body.style.display='none';
    history.back();
    setInterval(function (){history.back();}, 500)
}


var  EvPageShow = {
     main:function () {
     },
     add1:null,
     add2:null,
 };

window.onpageshow = function () {
     EvPageShow.main();

     if (EvPageShow.add1 != null){
         EvPageShow.add1();
     }
     if (EvPageShow.add2 != null){
         EvPageShow.add2();
     }
 };
/*
var autocompleteHandler = {

    currentFocus: null,
    inputField  : null,
    intId   : null,
    arr     : [],
    select  : null,
    evSelect:null,

    closeAllLists:function(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != this.inputField) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    },

    removeActive:function(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    },

    input:function(dom){
        console.log('input');
        this.inputField = dom;
        //var arr = [[1,"Italy"], [3,"Spain"], [5,"Portugal"]];
        var self = this;

        if (this.intId != null){clearTimeout(this.intId)}

        this.intId = setTimeout(function () {
            this.intId = null;
            var data = {
                value   : dom.value,
                id      : document.querySelector('input[name="id"]').value,
                field   : dom.getAttribute('name'),
                table   : document.querySelector('input[name="table"]').value
            };
            mRrequest('autocomplete', data,
                function (response) {
                //console.log(response);
                self.createList(response.arr,  dom);
            });
        }, 1000);
    },

    createList:function (arr, dom) {

        this.arr = arr;
        console.log(this.arr);
        this.closeAllLists();

        //var arr = ["Italy", "Spain", "Portugal", "Spurt"];
        var a, b, i, val = dom.value;
        if (!val) { return false;}
        this.currentFocus = -1;
        a = createDOM('div', {id:dom.id + "autocomplete-list", class:"autocomplete-items"});
        //a = document.createElement("DIV");
        //a.setAttribute("id", dom.id + "autocomplete-list");
        //a.setAttribute("class", "autocomplete-items");
        dom.parentNode.appendChild(a);
        var self = this;
        console.log(arr);

        for (i = 0; i < arr.length; i++) {
            //console.log(arr[i][1]);
            //console.log(arr[i][1].toUpperCase().indexOf(val.toUpperCase()));

            var index = arr[i][1].toUpperCase().indexOf(val.toUpperCase());
            //if (arr[i][1].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
            //if (arr[i][1].toUpperCase().includes(val.toUpperCase())) {
            if (index > -1) {
                //console.log('create');
                b = document.createElement("DIV");

                b.innerHTML = arr[i][1].substr(0,index);
                b.innerHTML += "<strong>" + arr[i][1].substr(index, val.length) + "</strong>";
                b.innerHTML += arr[i][1].substr(index+val.length, arr[i][1].length);

                b.innerHTML += "<input type='hidden' value='" + arr[i][1] + "'>";
                b.innerHTML += "<input type='hidden' value='" + arr[i][0] + "'>";
                b.addEventListener("click", function(e) {
                    dom.value = this.getElementsByTagName("input")[0].value;
                    //input type="hidden" value="" name="{{column.input.field}}"
                    //dom.value = this.getElementsByTagName("input")[0].value;


                    var inputs = document.querySelector('input[name="autocomplete_id"]');

                    console.log(inputs.length);
                    inputs.value = this.getElementsByTagName("input")[1].value;
                    
                    self.closeAllLists();


                    var idSel = parseInt(inputs.value);
                    //console.log(self.arr);
                    for (var j = 0; j < self.arr.length; j++) {
                        if (idSel == self.arr[j][0]){
                            self.select = self.arr[j];
                            try {
                                var selector = 'input[name="'+dom.getAttribute('name')+'"]';
                                document.querySelector(selector).onchange();
                            } catch (e) {
                            }
                            return;
                        }
                    }
                });

                a.appendChild(b);
            }
        }
    }
};
*/





if (!Array.from) {
    Array.from = (function() {
        var toStr = Object.prototype.toString;
        var isCallable = function(fn) {
            return typeof fn === 'function' || toStr.call(fn) === '[object Function]';
        };
        var toInteger = function (value) {
            var number = Number(value);
            if (isNaN(number)) { return 0; }
            if (number === 0 || !isFinite(number)) { return number; }
            return (number > 0 ? 1 : -1) * Math.floor(Math.abs(number));
        };
        var maxSafeInteger = Math.pow(2, 53) - 1;
        var toLength = function (value) {
            var len = toInteger(value);
            return Math.min(Math.max(len, 0), maxSafeInteger);
        };

        // Свойство length метода from равно 1.
        return function from(arrayLike/*, mapFn, thisArg */) {
            // 1. Положим C равным значению this.
            var C = this;

            // 2. Положим items равным ToObject(arrayLike).
            var items = Object(arrayLike);

            // 3. ReturnIfAbrupt(items).
            if (arrayLike == null) {
                throw new TypeError('Array.from requires an array-like object - not null or undefined');
            }

            // 4. Если mapfn равен undefined, положим mapping равным false.
            var mapFn = arguments.length > 1 ? arguments[1] : void undefined;
            var T;
            if (typeof mapFn !== 'undefined') {
                // 5. иначе
                // 5. a. Если вызов IsCallable(mapfn) равен false, выкидываем исключение TypeError.
                if (!isCallable(mapFn)) {
                    throw new TypeError('Array.from: when provided, the second argument must be a function');
                }

                // 5. b. Если thisArg присутствует, положим T равным thisArg; иначе положим T равным undefined.
                if (arguments.length > 2) {
                    T = arguments[2];
                }
            }

            // 10. Положим lenValue равным Get(items, "length").
            // 11. Положим len равным ToLength(lenValue).
            var len = toLength(items.length);

            // 13. Если IsConstructor(C) равен true, то
            // 13. a. Положим A равным результату вызова внутреннего метода [[Construct]]
            //     объекта C со списком аргументов, содержащим единственный элемент len.
            // 14. a. Иначе, положим A равным ArrayCreate(len).
            var A = isCallable(C) ? Object(new C(len)) : new Array(len);

            // 16. Положим k равным 0.
            var k = 0;
            // 17. Пока k < len, будем повторять... (шаги с a по h)
            var kValue;
            while (k < len) {
                kValue = items[k];
                if (mapFn) {
                    A[k] = typeof T === 'undefined' ? mapFn(kValue, k) : mapFn.call(T, kValue, k);
                } else {
                    A[k] = kValue;
                }
                k += 1;
            }
            // 18. Положим putStatus равным Put(A, "length", len, true).
            A.length = len;
            // 20. Вернём A.
            return A;
        };
    }());
}

//[5,3,4].diff([1,2,3,4,5,6])=> [], и это означает полное вхождение
Array.prototype.diff = function(a) {
    return this.filter(function(i) {return !(a.indexOf(i) > -1);});
};
