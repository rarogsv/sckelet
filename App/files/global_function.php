<?php

const INNER         = 'inner';
const STRING        = 'str';
const INT           = 'int';
const FLOAT         = 'float';
const TIME_STAMP    = 'time_stamp';
const MONEY         = 'money';
const BOOL          = 'bool';
const EMAIL         = 'email';
const DECODE        = 'decode';


//loadin classes
spl_autoload_register(
    function ($class) {
        $class =  str_replace('\\', '/', $class) . '.php';
        //print_r($class);
        //echo $class;
        //logs(__FILE__, __LINE__, DIR .$class);
        //logs(__FILE__, __LINE__, file_exists(DIR .$class));
        //logs(__FILE__, __LINE__, DIR_CLASS .$class);
        //logs(__FILE__, __LINE__, file_exists(DIR_CLASS .$class));
        //logs(__FILE__, __LINE__, DIR_SKELETON . $class);
        //logs(__FILE__, __LINE__, file_exists(DIR_SKELETON . $class));

        if (file_exists(DIR_CLASS .$class)) {
            require_once DIR_CLASS . $class;
        } else if (file_exists(DIR_SKELETON . $class)) {
            require_once DIR_SKELETON . $class;
        } else {
            logs(__FILE__, __LINE__, DIR_SKELETON .$class);
            logs(__FILE__, __LINE__, DIR .$class);
        }
    }
);

function getItemArr($data, $key, $deff){
    if (isset($data[$key])){
        return $data[$key];
    } else {
        return $deff;
    }
}

function getNumberWeek($ts){
    $day = date('w', $ts); //0-sunday
    logs(__FILE__, __LINE__, $day);
    $day--;
    if ($day == -1){
        $day=6;
    }
    logs(__FILE__, __LINE__, $day);
    return $day;
}

function array_column_object(array $arr, $key){
    $res = [];
    foreach ($arr as $item){
        $res[] = $item[$key];
    }
    return array_unique($res, SORT_REGULAR);
}

function array_sub($data, $keys){
    $res = [];
    foreach ($keys as $key){
        if (isset($data[$key])){
            $res[$key] = $data[$key];
        }
    }
    return $res;
}

function array_merge_coincidence($arr1, $keyCoin1, $arr2, $keyCoin2){
    $arrRes = [];
    foreach ($arr1 as $item1){
        $item2 = indexOfObj($arr2, [$keyCoin2 => $item1[$keyCoin1]]);
        if ($item2 != null){
            unset($item2[$keyCoin2]);
            $arrRes[] = array_merge($item1, $item2);
        }
    }
    return $arrRes;
}

function array_sub2(array $arr, $search){
    $res = [];
    foreach ($arr as $ind => $o){
        $find = true;
        foreach ($search as $key => $val){
            $find = $find && $o[$key] === $val;
        }
        if ($find){
            $res[] = $o;
        }
    }
    return $res;
}

function array_del_item(array &$arr, $search){
    //logs(__FILE__, __LINE__, $arr);
    //logs(__FILE__, __LINE__, $search);
    $ind = array_search($search, $arr);
    if ($ind !== false){
        array_splice($arr, $ind, 1);
    }
}

function logs($file, $line, $data){
    if (SHOW_LOG){return;}
    if (is_array($data) || is_object($data) || is_bool($data)){
        $data = json_encode($data);
    }
    print_r("\n");
    print_r(str_replace(DIR, '', $file).' - '.$line.' : '.$data);
}

function indexOf(array $arr, $search) : int
{
    foreach ($arr as $ind => $o){
        $find = true;
        foreach ($search as $key => $val){
            $find = $find && $o[$key] === $val;
        }
        if ($find){
            return $ind;
        }
    }
    return -1;
}

function indexOfObj(array $arr, $search)
{
    foreach ($arr as $ind => $o){
        $find = true;
        foreach ($search as $key => $val){
            $find = $find && $o[$key] == $val;
        }
        if ($find){
            return $o;
        }
    }
    return null;
}

function array_sort_by_key(&$array, $key, $asc = true, $string = false){
    if($string){
        usort($array,function ($a, $b) use(&$key,&$asc)
        {
            if($asc) {
                return strcmp(strtolower($a[$key]), strtolower($b[$key]));
            } else {
                return strcmp(strtolower($b[$key]), strtolower($a[$key]));
            }
        });
    }else{
        usort($array,function ($a, $b) use(&$key,&$asc)
        {
            if($a[$key] == $b[$key]){
                return 0;
            }

            if($asc) {
                return ($a[$key] < $b[$key]) ? -1 : 1;
            } else {
                return ($a[$key] > $b[$key]) ? -1 : 1;
            }

        });
    }
}

function array_swap(&$array,$swap_a,$swap_b){
    list($array[$swap_a],$array[$swap_b]) = array($array[$swap_b],$array[$swap_a]);
}

function mexplode(string $data, string $separartor=',') : array
{
    if (strlen($data) == 0){
        return [];
    }

    return explode($separartor, $data);
}

function mimplode(array $data, string $separartor=','):string
{
    if (count($data) == 0){
        return '';
    }
    return implode($separartor, $data);
}

function addItemToArrStr(string $data, $item, $orderBy='ASC') : string
{
    $d = json_decode($data, true);

    if ($orderBy == 'ASC'){
        if (is_numeric($item)){
            if (!in_array($item, $d) && $item !== 0){
                $d[] = $item;
            }
        } else {
            $d[] = $item;
        }
    } else {
        array_unshift($d, $item);
    }

    return json_encode($d);
}

function delItemToArrStr(string $data, $item) : string
{
    $dataArr = json_decode($data, true);
    array_del_item($dataArr, $item);

    //$ind = array_search($item, $dataArr);
    //if ($ind !== false){
    //    array_splice($dataArr, $ind, 1);
    //}

    return json_encode($dataArr);
}

function getRandomString($length = 8) {
    $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $string = '';

    for ($i = 0; $i < $length; $i++) {
        $string .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    return $string;
}

function getRateFormat($rate){
    return number_format($rate/100, 2, '.', '');
}

function getMoneyFormat($money){
    return number_format($money/100, 2, '.', '');
}

function generatePassword($length = 20){
    $chars =  'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    $str = '';
    $max = strlen($chars) - 1;

    for ($i=0; $i < $length; $i++){
        $str .= $chars[random_int(0, $max)];
    }

    return $str;
}

//best AES-256-CBC
function encrypt(array $data, string $secret_key, $cipher = "AES-128-CBC")
{
    $plaintext = json_encode($data);
    $key = openssl_digest($secret_key, 'sha256', true);
    $ivlen = openssl_cipher_iv_length($cipher);
    $iv = openssl_random_pseudo_bytes($ivlen);
    // binary cipher
    $ciphertext_raw = openssl_encrypt($plaintext, $cipher, $key, OPENSSL_RAW_DATA, $iv);
    // or replace OPENSSL_RAW_DATA & $iv with 0 & bin2hex($iv) for hex cipher (eg. for transmission over internet)

    // or increase security with hashed cipher; (hex or base64 printable eg. for transmission over internet)
    $hmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
    return base64_encode($iv . $hmac . $ciphertext_raw);
}
//best AES-256-CBC
function decrypt(string $ciphertext, string $secret_key, $cipher = "AES-128-CBC")
{

    $c = base64_decode($ciphertext);

    $key = openssl_digest($secret_key, 'sha256', true);

    $ivlen = openssl_cipher_iv_length($cipher);

    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len = 32);
    $ciphertext_raw = substr($c, $ivlen + $sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, OPENSSL_RAW_DATA, $iv);

    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, true);
    if (hash_equals($hmac, $calcmac)){
        return json_decode($original_plaintext, true);
    } else {
        return false;
    }
}

const LANGUAGES_WORLD = [
    'ab' => 'Abkhazian',
    'aa' => 'Afar',
    'af' => 'Afrikaans',
    'ak' => 'Akan',
    'sq' => 'Albanian',
    'am' => 'Amharic',
    'ar' => 'Arabic',
    'an' => 'Aragonese',
    'hy' => 'Armenian',
    'as' => 'Assamese',
    'av' => 'Avaric',
    'ae' => 'Avestan',
    'ay' => 'Aymara',
    'az' => 'Azerbaijani',
    'bm' => 'Bambara',
    'ba' => 'Bashkir',
    'eu' => 'Basque',
    'be' => 'Belarusian',
    'bn' => 'Bengali',
    'bh' => 'Bihari languages',
    'bi' => 'Bislama',
    'bs' => 'Bosnian',
    'br' => 'Breton',
    'bg' => 'Bulgarian',
    'my' => 'Burmese',
    'ca' => 'Catalan, Valencian',
    'km' => 'Central Khmer',
    'ch' => 'Chamorro',
    'ce' => 'Chechen',
    'ny' => 'Chichewa, Chewa, Nyanja',
    'zh' => 'Chinese',
    'cu' => 'Church Slavonic, Old Bulgarian, Old Church Slavonic',
    'cv' => 'Chuvash',
    'kw' => 'Cornish',
    'co' => 'Corsican',
    'cr' => 'Cree',
    'hr' => 'Croatian',
    'cs' => 'Czech',
    'da' => 'Danish',
    'dv' => 'Divehi, Dhivehi, Maldivian',
    'nl' => 'Dutch, Flemish',
    'dz' => 'Dzongkha',
    'en' => 'English',
    'eo' => 'Esperanto',
    'et' => 'Estonian',
    'ee' => 'Ewe',
    'fo' => 'Faroese',
    'fj' => 'Fijian',
    'fi' => 'Finnish',
    'fr' => 'French',
    'ff' => 'Fulah',
    'gd' => 'Gaelic, Scottish Gaelic',
    'gl' => 'Galician',
    'lg' => 'Ganda',
    'ka' => 'Georgian',
    'de' => 'German',
    'ki' => 'Gikuyu, Kikuyu',
    'el' => 'Greek (Modern)',
    'kl' => 'Greenlandic, Kalaallisut',
    'gn' => 'Guarani',
    'gu' => 'Gujarati',
    'ht' => 'Haitian, Haitian Creole',
    'ha' => 'Hausa',
    'he' => 'Hebrew',
    'hz' => 'Herero',
    'hi' => 'Hindi',
    'ho' => 'Hiri Motu',
    'hu' => 'Hungarian',
    'is' => 'Icelandic',
    'io' => 'Ido',
    'ig' => 'Igbo',
    'id' => 'Indonesian',
    'ia' => 'Interlingua (International Auxiliary Language Association)',
    'ie' => 'Interlingue',
    'iu' => 'Inuktitut',
    'ik' => 'Inupiaq',
    'ga' => 'Irish',
    'it' => 'Italian',
    'ja' => 'Japanese',
    'jv' => 'Javanese',
    'kn' => 'Kannada',
    'kr' => 'Kanuri',
    'ks' => 'Kashmiri',
    'kk' => 'Kazakh',
    'rw' => 'Kinyarwanda',
    'kv' => 'Komi',
    'kg' => 'Kongo',
    'ko' => 'Korean',
    'kj' => 'Kwanyama, Kuanyama',
    'ku' => 'Kurdish',
    'ky' => 'Kyrgyz',
    'lo' => 'Lao',
    'la' => 'Latin',
    'lv' => 'Latvian',
    'lb' => 'Letzeburgesch, Luxembourgish',
    'li' => 'Limburgish, Limburgan, Limburger',
    'ln' => 'Lingala',
    'lt' => 'Lithuanian',
    'lu' => 'Luba-Katanga',
    'mk' => 'Macedonian',
    'mg' => 'Malagasy',
    'ms' => 'Malay',
    'ml' => 'Malayalam',
    'mt' => 'Maltese',
    'gv' => 'Manx',
    'mi' => 'Maori',
    'mr' => 'Marathi',
    'mh' => 'Marshallese',
    'ro' => 'Moldovan, Moldavian, Romanian',
    'mn' => 'Mongolian',
    'na' => 'Nauru',
    'nv' => 'Navajo, Navaho',
    'nd' => 'Northern Ndebele',
    'ng' => 'Ndonga',
    'ne' => 'Nepali',
    'se' => 'Northern Sami',
    'no' => 'Norwegian',
    'nb' => 'Norwegian Bokmål',
    'nn' => 'Norwegian Nynorsk',
    'ii' => 'Nuosu, Sichuan Yi',
    'oc' => 'Occitan (post 1500)',
    'oj' => 'Ojibwa',
    'or' => 'Oriya',
    'om' => 'Oromo',
    'os' => 'Ossetian, Ossetic',
    'pi' => 'Pali',
    'pa' => 'Panjabi, Punjabi',
    'ps' => 'Pashto, Pushto',
    'fa' => 'Persian',
    'pl' => 'Polish',
    'pt' => 'Portuguese',
    'qu' => 'Quechua',
    'rm' => 'Romansh',
    'rn' => 'Rundi',
    'ru' => 'Russian',
    'sm' => 'Samoan',
    'sg' => 'Sango',
    'sa' => 'Sanskrit',
    'sc' => 'Sardinian',
    'sr' => 'Serbian',
    'sn' => 'Shona',
    'sd' => 'Sindhi',
    'si' => 'Sinhala, Sinhalese',
    'sk' => 'Slovak',
    'sl' => 'Slovenian',
    'so' => 'Somali',
    'st' => 'Sotho, Southern',
    'nr' => 'South Ndebele',
    'es' => 'Spanish, Castilian',
    'su' => 'Sundanese',
    'sw' => 'Swahili',
    'ss' => 'Swati',
    'sv' => 'Swedish',
    'tl' => 'Tagalog',
    'ty' => 'Tahitian',
    'tg' => 'Tajik',
    'ta' => 'Tamil',
    'tt' => 'Tatar',
    'te' => 'Telugu',
    'th' => 'Thai',
    'bo' => 'Tibetan',
    'ti' => 'Tigrinya',
    'to' => 'Tonga (Tonga Islands)',
    'ts' => 'Tsonga',
    'tn' => 'Tswana',
    'tr' => 'Turkish',
    'tk' => 'Turkmen',
    'tw' => 'Twi',
    'ug' => 'Uighur, Uyghur',
    'uk' => 'Ukrainian',
    'ur' => 'Urdu',
    'uz' => 'Uzbek',
    've' => 'Venda',
    'vi' => 'Vietnamese',
    'vo' => 'Volap_k',
    'wa' => 'Walloon',
    'cy' => 'Welsh',
    'fy' => 'Western Frisian',
    'wo' => 'Wolof',
    'xh' => 'Xhosa',
    'yi' => 'Yiddish',
    'yo' => 'Yoruba',
    'za' => 'Zhuang, Chuang',
    'zu' => 'Zulu'
];

const COUNTRIES =
    [
        "AF" => "Afghanistan",
        "AL" => "Albania",
        "DZ" => "Algeria",
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua and Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AU" => "Australia",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BD" => "Bangladesh",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia and Herzegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "BN" => "Brunei Darussalam",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "CV" => "Cape Verde",
        "KY" => "Cayman Islands",
        "CF" => "Central African Republic",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China",
        "CX" => "Christmas Island",
        "CC" => "Cocos (Keeling) Islands",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CG" => "Congo",
        "CD" => "Congo, the Democratic Republic of the",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Cote D'Ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands (Malvinas)",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French Southern Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GN" => "Guinea",
        "GW" => "Guinea-Bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard Island and Mcdonald Islands",
        "VA" => "Holy See (Vatican City State)",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran, Islamic Republic of",
        "IQ" => "Iraq",
        "IE" => "Ireland",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "KP" => "Korea, Democratic People's Republic of",
        "KR" => "Korea, Republic of",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Lao People's Democratic Republic",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libyan Arab Jamahiriya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macao",
        "MK" => "Macedonia, the Former Yugoslav Republic of",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia, Federated States of",
        "MD" => "Moldova, Republic of",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "AN" => "Netherlands Antilles",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Northern Mariana Islands",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau",
        "PS" => "Palestinian Territory, Occupied",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn",
        "PL" => "Poland",
        "PT" => "Portugal",
        "QA" => "Qatar",
        "RE" => "Reunion",
        "RO" => "Romania",
        "RU" => "Russian Federation",
        "RW" => "Rwanda",
        "SH" => "Saint Helena",
        "KN" => "Saint Kitts and Nevis",
        "LC" => "Saint Lucia",
        "PM" => "Saint Pierre and Miquelon",
        "VC" => "Saint Vincent and the Grenadines",
        "WS" => "Samoa",
        "SM" => "San Marino",
        "ST" => "Sao Tome and Principe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "CS" => "Serbia and Montenegro",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia and the South Sandwich Islands",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syrian Arab Republic",
        "TW" => "Taiwan, Province of China",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania, United Republic of",
        "TH" => "Thailand",
        "TL" => "Timor-Leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad and Tobago",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks and Caicos Islands",
        "TV" => "Tuvalu",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "US" => "United States",
        "UM" => "United States Minor Outlying Islands",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VE" => "Venezuela",
        "VN" => "Viet Nam",
        "VG" => "Virgin Islands, British",
        "VI" => "Virgin Islands, U.s.",
        "WF" => "Wallis and Futuna",
        "EH" => "Western Sahara",
        "YE" => "Yemen",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe"
    ];

const CODE_PHONE = ["BD"=> "880", "BE"=> "32", "BF"=> "226", "BG"=> "359", "BA"=> "387", "BB"=> "1246", "WF"=> "681", 
    "BL"=> "590", "BM"=> "1441", "BN"=> "673", "BO"=> "591", "BH"=> "973", "BI"=> "257", "BJ"=> "229", "BT"=> "975", 
    "JM"=> "1876", "BW"=> "267", "WS"=> "685", "BQ"=> "599", "BR"=> "55", "BS"=> "1242", "JE"=> "441534",
    "BY"=> "375", "BZ"=> "501", "RU"=> "7", "RW"=> "250", "RS"=> "381", "TL"=> "670", "RE"=> "262", "TM"=> "993", 
    "TJ"=> "992", "RO"=> "40", "TK"=> "690", "GW"=> "245", "GU"=> "1671", "GT"=> "502", "GS"=> "", "GR"=> "30", 
    "GQ"=> "240", "GP"=> "590", "JP"=> "81", "GY"=> "592", "GG"=> "441481", "GF"=> "594", "GE"=> "995", 
    "GD"=> "1473", "GB"=> "44", "GA"=> "241", "SV"=> "503", "GN"=> "224", "GM"=> "220", "GL"=> "299", "GI"=> "350", 
    "GH"=> "233", "OM"=> "968", "TN"=> "216", "JO"=> "962", "HR"=> "385", "HT"=> "509", "HU"=> "36", "HK"=> "852", 
    "HN"=> "504", "VE"=> "58", "PS"=> "970", "PW"=> "680", "PT"=> "351", "SJ"=> "47", "PY"=> "595", "IQ"=> "964",
    "PA"=> "507", "PF"=> "689", "PG"=> "675", "PE"=> "51", "PK"=> "92", "PH"=> "63", "PN"=> "870", "PL"=> "48", "PM"=> "508",
    "ZM"=> "260", "EH"=> "212", "EE"=> "372", "EG"=> "20", "ZA"=> "27", "EC"=> "593", "IT"=> "39", "VN"=> "84", "SB"=> "677",
    "ET"=> "251", "SO"=> "252", "ZW"=> "263", "SA"=> "966", "ES"=> "34", "ER"=> "291", "ME"=> "382", "MD"=> "373", "MG"=> "261",
    "MF"=> "590", "MA"=> "212", "MC"=> "377", "UZ"=> "998", "MM"=> "95", "ML"=> "223", "MO"=> "853", "MN"=> "976", "MH"=> "692",
    "MK"=> "389", "MU"=> "230", "MT"=> "356", "MW"=> "265", "MV"=> "960", "MQ"=> "596", "MP"=> "1670", "MS"=> "1664", "MR"=> "222",
    "IM"=> "441624", "UG"=> "256", "TZ"=> "255", "MY"=> "60", "MX"=> "52", "IL"=> "972", "FR"=> "33", "IO"=> "246", "SH"=> "290",
    "FI"=> "358", "FJ"=> "679", "FK"=> "500", "FM"=> "691", "FO"=> "298", "NI"=> "505", "NL"=> "31", "NO"=> "47", "NA"=> "264",
    "VU"=> "678", "NC"=> "687", "NE"=> "227", "NF"=> "672", "NG"=> "234", "NZ"=> "64", "NP"=> "977", "NR"=> "674", "NU"=> "683",
    "CK"=> "682", "CI"=> "225", "CH"=> "41", "CO"=> "57", "CN"=> "86", "CM"=> "237", "CL"=> "56", "CC"=> "61", "CA"=> "1",
    "CG"=> "242", "CF"=> "236", "CD"=> "243", "CZ"=> "420", "CY"=> "357",
    "CX"=> "61", "CR"=> "506", "CW"=> "599", "CV"=> "238", "CU"=> "53", "SZ"=> "268", "SY"=> "963", "SX"=> "599", "KG"=> "996", 
    "KE"=> "254", "SS"=> "211", "SR"=> "597", "KI"=> "686", "KH"=> "855", "KN"=> "1869", "KM"=> "269", "ST"=> "239", 
    "SK"=> "421", "KR"=> "82", "SI"=> "386", "KP"=> "850", "KW"=> "965", "SN"=> "221", "SM"=> "378", "SL"=> "232", "SC"=> "248", 
    "KZ"=> "7", "KY"=> "1345", "SG"=> "65", "SE"=> "46", "SD"=> "249", "DO"=> "1809 and 1829", "DM"=> "1767", "DJ"=> "253", 
    "DK"=> "45", "VG"=> "1284", "DE"=> "49", "YE"=> "967", "DZ"=> "213", "US"=> "1", "UY"=> "598", "YT"=> "262", "UM"=> "1", 
    "LB"=> "961", "LC"=> "1758", "LA"=> "856", "TV"=> "688", "TW"=> "886", "TT"=> "1868", "TR"=> "90", "LK"=> "94", 
    "LI"=> "423", "LV"=> "371", "TO"=> "676", "LT"=> "370", "LU"=> "352", "LR"=> "231", "LS"=> "266", "TH"=> "66",
    "TG"=> "228", "TD"=> "235", "TC"=> "1649", "LY"=> "218", "VA"=> "379", "VC"=> "1784", "AE"=> "971", "AD"=> "376", 
    "AG"=> "1268", "AF"=> "93", "AI"=> "1264", "VI"=> "1340", "IS"=> "354", "IR"=> "98", "AM"=> "374", "AL"=> "355", 
    "AO"=> "244", "AS"=> "1684", "AR"=> "54", "AU"=> "61", "AT"=> "43", "AW"=> "297", "IN"=> "91", "AX"=> "35818",
    "AZ"=> "994", "IE"=> "353", "ID"=> "62", "UA"=> "380", "QA"=> "974", "MZ"=> "258"];

